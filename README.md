# Metadata Transformer Service

Harvesting service with script based transforming.

Notes:

 * As of now this tutorial is for Linux only and has been tested with Debian 8 and Arch Linux.
 * Large amounts of this tutorial have been taken from [www.mastertheboss.com](http://www.mastertheboss.com/). For further reading take a look at their thorough tutorial found [here](http://www.mastertheboss.com/jboss-server/jboss-datasource/configuring-a-datasource-with-postgresql-and-jboss-wildfly).

## Installation

### Prerequisite

Install all of the following software using either your OS package manager (Linux) or by manually downloading and extracting the packages to a destination of your choice:

 * [Oracle / OpenJDK JDK](http://openjdk.java.net/) >= 1.8
 * [Apache Maven](https://maven.apache.org/) >= 3.2
 * [PostgreSQL](http://www.postgresql.org/) >= 9.4
 * [JBoss WildFly (Java EE7 Full & Web Distribution)](http://wildfly.org/) >= 10.x
 * [GIT](https://git-scm.com/) >= 1.9.4 
 
*The [Arch User Repository](https://aur.archlinux.org) (AUR) offers a WildFly 10 package.*

### Setup project structure and clone the sources

#### PostgreSQL

*Debian*  
Start PostgreSQL and set a password for user postgres:
```
$> service postgresql start
$> passwd postgres
```

*Arch*  
Create a user, initialize the database cluster and start `postgresql.service`. Finally, edit the `postgresql.conf` file and uncomment the line `listen_addresses = 'localhost'`.
```
$> passwd postgres
$> su - postgres
[postgres]$ initdb --locale en_US.UTF-8 -E UTF8 -D '/var/lib/postgres/data'
$> systemctl start service postgresql start
$> vim /var/lib/postgresql/data/postgresql.conf
```

*General*  
Log in as user postgres and set the password for the database user postgres:

```
$> su - postgres
[postgres]$ psql -c "ALTER USER postgres WITH PASSWORD 'postgres'" -d template1
```

Finally, change the allowed socket connection interfaces which are set in the `pg_hba.conf` file. For Debian, this file is found in `/etc/postgresql/your_version/main/` and in Arch at `/var/lib/postgres/data/`. Make sure to run this command with the proper permissions. Adjust the IPv4 settings, changing the `METHOD` to md5 and replacing `samehost` with your localhost IP. In my case this was `127.0.0.1/32` :
```
# TYPE  DATABASE  USER  ADDRESS  METHOD

# "local" is for Unix domain socket connections only
local  all  all  md5
 
# IPv4 local connections:
host  all     postgres  samehost  md5
host  all     nuxeo     samehost  md5
host  cspace  cspace    samehost  md5
```

Disable the IPv6 setting by commenting the following line:
```
# IPv6 local connections:
#host all all ::1/128 md5 
```


#### JBoss WildFly

Now it's time to configure WildFly. First, set up a new user:

```
$> cd /wildfly_root_dir/bin/
$> ./add-user.sh
```

Create a new Management User by selecting the appropiate option. Enter your username and set a password. In the next steps, ignore WildFly prompting you for possible groups by leaving everything blank and when asked about `AS` connections type `no`. You should now have successfully added a user. 

Now, download the postgres JDBC driver. Information on what version you should be getting can be found [here](https://jdbc.postgresql.org/download.html). Again, a JDBC package is offered at the Arch AUR.

Assuming you are still in the `bin` directory, start up your WildFly server and run the config script:
```
$> ./standalone.sh
$> ./jboss-cli.sh
```

Once connected, issue the following commands, replacing paths and credentials with your own:

```
module add --name=org.postgres --resources=/path/to/your/jdbc/driver.jar --dependencies=javax.api,javax.transaction.api
/subsystem=datasources/jdbc-driver=postgres:add(driver-name="postgres",driver-module-name="org.postgres",driver-class-name=org.postgresql.Driver)
data-source add --jndi-name=java:/PostgresDS --name=PostgrePool --connection-url=jdbc:postgresql://localhost/postgres --driver-name=postgres --user-name=postgres --password=postgres
```


#### Clone repository

Last but not least clone the repository:
```
$> git clone https://gitlab.com/EU_ODP/metadatatransformerservice.git
```

### Get it running

Now run the following Maven command from the metadatatransformerservice root directory:

```
$> cd /transformerservice_root_dir/
$> mvn package -Dmaven.test.skip=true -DschemaGeneration=create wildfly:deploy
```

Please note that the option `DschemaGeneration=create` is only required for the first deployment. Should the DB schema be altered later an update script will be provided which can then be run by using the `DschemaGeneration=drop` option.

### See it running

Open your preferred web browser and go to: 

```
http://localhost:8080/MetadataTransformerService
```

