-- Miscellaneous DB table updates --
ALTER TABLE run DROP COLUMN IF EXISTS taskid;

ALTER TABLE run ADD COLUMN numberskipped integer DEFAULT 0;
ALTER TABLE run ADD COLUMN numberdeleted integer DEFAULT 0;
ALTER TABLE run ADD COLUMN incremental boolean DEFAULT FALSE;
ALTER TABLE run RENAME COLUMN runstatus TO runstate;

-- Filter enhancement --
ALTER TABLE filter ADD COLUMN isnegation BOOLEAN DEFAULT FALSE;
ALTER TABLE filter ADD COLUMN isfilterenabled BOOLEAN DEFAULT TRUE;
ALTER TABLE filter ADD COLUMN index SERIAL;
ALTER TABLE filter DROP CONSTRAINT IF EXISTS unique_index;

-- Enum conversion from int to String --
ALTER TABLE harvester ALTER COLUMN frequency TYPE CHARACTER VARYING(127);
UPDATE harvester SET frequency='every_five_minutes' WHERE frequency='0';
UPDATE harvester SET frequency='hourly' WHERE frequency='1';
UPDATE harvester SET frequency='daily' WHERE frequency='2';
UPDATE harvester SET frequency='weekly' WHERE frequency='3';
UPDATE harvester SET frequency='monthly' WHERE frequency='4';
UPDATE harvester SET frequency='yearly' WHERE frequency='5';
UPDATE harvester SET frequency='manually' WHERE frequency='6';

ALTER TABLE harvester ALTER COLUMN harveststrategy TYPE CHARACTER VARYING(127);
UPDATE harvester SET harveststrategy='create' WHERE harveststrategy='0' OR harveststrategy='create_first';
UPDATE harvester SET harveststrategy='update' WHERE harveststrategy='1' OR harveststrategy='update_first';

ALTER TABLE harvester ALTER COLUMN visibility TYPE CHARACTER VARYING(127);
UPDATE harvester SET visibility='private_visibility' WHERE visibility='0';
UPDATE harvester SET visibility='public_visibility' WHERE visibility='1';

UPDATE repository SET visibility='private_visibility' WHERE visibility='PRIVATE';
UPDATE repository SET visibility='public_visibility' WHERE visibility='PUBLIC';
UPDATE repository SET repositorytype='dcatap' WHERE repositorytype='dcat';
UPDATE repository SET repositorytype='rdf_ckan' WHERE repositorytype='ckan_rdf';
UPDATE repository SET repositorytype='rdf' WHERE repositorytype='jena_dump';
UPDATE repository SET repositorytype='rdf' WHERE repositorytype='rdf_dump';
UPDATE repository SET repositorytype='rdf' WHERE repositorytype='rdf_dump_jena';
UPDATE repository SET repositorytype='rss' WHERE repositorytype='rss_dados_gov_pt';
UPDATE repository SET language='PORTUGUESE' WHERE language='POTUGUESE';


UPDATE log SET servicestage='init_stage' WHERE servicestage='Initialize';
UPDATE log SET servicestage='export_stage' WHERE servicestage='Export';
UPDATE log SET servicestage='transformation_stage' WHERE servicestage='Transform';
UPDATE log SET servicestage='import_stage' WHERE servicestage='Import';
UPDATE log SET servicestage='filter_stage' WHERE servicestage='Filter';
UPDATE log SET servicestage='mapping_stage' WHERE servicestage='Mapping';
UPDATE log SET servicestage='delete_stage' WHERE servicestage='Delete';

-- introduce spatial in repository
CREATE TABLE IF NOT EXISTS repository_spatial(repository_id bigint NOT NULL, spatial character varying(255));

-- Logger
ALTER TABLE log RENAME COLUMN loglevel TO severity;
ALTER TABLE log ADD COLUMN category CHARACTER VARYING(255) DEFAULT 'unknown';

-- Git
ALTER TABLE harvester ADD COLUMN transformationscriptsource CHARACTER VARYING(255) DEFAULT 'custom';
ALTER TABLE harvester ADD COLUMN gitrepourl CHARACTER VARYING(255);
ALTER TABLE harvester ADD COLUMN gitbranch CHARACTER VARYING (255);
ALTER TABLE harvester ADD COLUMN gitfilename CHARACTER VARYING(255);
ALTER TABLE harvester ADD COLUMN gitreporequirescredentials BOOLEAN DEFAULT FALSE;
ALTER TABLE harvester ADD COLUMN gitreposshauthentication BOOLEAN DEFAULT FALSE;
ALTER TABLE harvester ADD COLUMN gitusername CHARACTER VARYING(255);
ALTER TABLE harvester ADD COLUMN gitpassword CHARACTER VARYING(255);
ALTER TABLE harvester ADD COLUMN forceupdate BOOLEAN DEFAULT FALSE;
ALTER TABLE harvester DROP COLUMN IF EXISTS gitrepository_id;

DROP TABLE IF EXISTS gitrepository CASCADE;

-- Dataset redesigned
CREATE TABLE IF NOT EXISTS dataset(id bigserial NOT NULL, name character varying(255) NOT NULL, content text, originalcontent text, previouscontent text, error character varying(255), catalog integer NOT NULL, created timestamp without time zone, CONSTRAINT dataset_pkey PRIMARY KEY (id));
