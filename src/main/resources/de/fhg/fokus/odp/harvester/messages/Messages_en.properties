## Main
titleMain=Metadatentransformator
titleLogin=Anmeldung
titleDashboard=\u00DCbersicht
titleHelp=Hilfe



infoDescriptionMain=Der Metadatentransformator konvertiert mit Hilfe eines Transformationsskripts automatisiert Metadaten von einem Quell- zu einem Zielportal.
infoDescriptionHelpIntroduction=Der Metadatentransformator wurde mit dem Ziel entwickelt, das Indizieren von Datens\u00E4tzen verschiedenster Anbieter zu automatisieren. \
Dabei werden die Metadaten der angeschlossenen Portale in regelm\u00e4\u00DFigen Abst\u00e4nden abgefragt und in ein (\u00FCblicherweise) einheitliches Format \u00FCberf\u00FChrt. \
Dieser Konvertierungsprozess kann in Echtzeit mitverfolgt werden. Die erstellten Mitschnitte werden au\u00DFerdem archiviert, sodass der Ablauf auch im Nachinein nachvollzogen und analysiert werden kann. \
Die Verarbeitungskette besteht haupts\u00e4chlich aus drei Komponenten, zwei sogenannten Repositories und einem Harvester. \
Eine \u00DCbersichtsseite der abgeschlossenen, laufenden und geplanten Transformationen ist nach Anmeldung \u00FCber die Schaltfl\u00e4che \u201E\u00DCbersicht\u201C innerhalb der Navigationsleiste am oberen Rand aufrufbar.
infoDescriptionHelpRepository=Repositories definieren Quellen oder Ziele, die durch einen Harvester verkn\u00FCpft werden k\u00F6nnen. \
\u00DCblicherweise wird ein Portal \u00FCber ein solches Repository abgebildet, da hier\u00FCber Daten wie etwa Internetadresse oder Herausgeber gespeichert werden. \
Manche Portale verlangen die \u00DCbergabe von Parametern, die der Authentifizierung dienen. \
Diese, h\u00e4ufig in Form von sogenannten <a href="https://de.wikipedia.org/wiki/Programmierschnittstelle">API</a>-Keys vorkommenden Informationen, k\u00F6nnen bei Erstellung eines Harvesters optional mit angegeben werden. \
Eine \u00DCbersichtsseite der aktuell vorhandenen Repositories ist \u00FCber die gleichnamige Schaltfl\u00e4che innerhalb der Navigationsleiste am oberen Rand aufrufbar.
infoDescriptionHelpHarvester=Ein Harvester \u00FCberf\u00FChrt mithilfe eines speziellen Skripts Metadaten von einem Quell- in ein Ziel-Repository. \
Das Skript muss an die Datenformate der gew\u00e4hlten Repositories angepasst werden, um die gew\u00FCnschten Daten zuverl\u00e4ssig extrahieren und anschlie\u00DFend wieder speichern zu k\u00F6nnen. \
F\u00FCr Ausgangsdaten im <a href="https://de.wikipedia.org/wiki/Extensible_Markup_Language">XML-Format</a> werden daf\u00FCr <a href="https://de.wikipedia.org/wiki/XSL_Transformation">XSL Transformationen</a> verwendet, bei <a href="https://de.wikipedia.org/wiki/JavaScript_Object_Notation">JSON Objekten</a> entsprechend <a href="https://de.wikipedia.org/wiki/JavaScript">JavaScript</a>. \
Die Transformation der Metadaten kann au\u00DFerdem mit einstellbaren Filtern weiter angepasst werden. \
Da sich die zu verarbeitenden Daten (und somit deren Metadaten) im Laufe der Zeit ver\u00e4ndern k\u00F6nnen, kann ein Harvester nach voreingestelltem Zeitplan automatisiert ausgef\u00FChrt werden. \
Eine \u00DCbersichtsseite der aktuell vorhandenen Harvester ist \u00FCber die gleichnamige Schaltfl\u00e4che innerhalb der Navigationsleiste am oberen Rand aufrufbar.


## Misc
labelMiscGeneral=Allgemein
labelMiscDeleted=Gel\u00F6scht
labelMiscMore=Mehr
labelMiscSelect=W\u00E4hlen
labelMiscIntroduction=Einleitung
labelMiscNotAvailable=n. a.
inputMiscSelectionNone=Keine Auswahl
inputMiscSelectAll=Alle ausw\u00e4hlen
inputMiscSelectionNumber=ausgew\u00E4hlt
inputMiscName=Name
inputMiscPassword=Passwort
inputMiscConfirmPassword=Passwort best\u00E4tigen
inputMiscDescription=Beschreibung
inputMiscUrl=URL
inputMiscHomepage=Homepage
inputMiscDefaultLanguage=Voreingestellte Sprache
infoMiscPanelHeaderInformation=Informationen
infoMiscPanelHeaderStatistics=Statistiken
linkMiscVisualizeStats=Statistik visualisieren
linkMiscInfoMore=Weitere Informationen
buttonMiscStop=Stop
buttonMiscApply=\u00DCbernehmen
buttonMiscDelete=Entfernen
buttonMiscDeleteAll=Alle entfernen
buttonMiscNew=Neu
buttonMiscAdd=Hinzuf\u00FCgen
buttonMiscCreate=Erstellen
buttonMiscMoreInfos=Mehr
buttonMiscGo=Los


## Units
unitMinuteSingular=Minute
unitMinutePlural=Minuten


## Images
altMainLogo=Logo of the MetadataTransformerService
titleHelpImage=Schematische Darstellung
altHelpSchema=Schematische Darstellung des Zusammenspiels der Komponenten des Metadatentransformators
altProfilePicture=Profile picture


## Messages
msgErrGeneral=Ein unbekannter Fehler ist aufgetreten
msgErrNoFileSpecified=Keine Datei angegeben
msgInfoHarvesterCreated=Harvester erfolgreich erstellt
msgInfoHarvesterUpdated=Harvester erfolgreich aktualisiert
msgErrHarvesterDownloadFailed=Harvester konnte nicht zum Download vorbereitet werden
msgErrNameRequired=Bitte geben Sie einen Namen an
msgErrSourceRepoRequired=Bitte spezifizieren Sie ein Quell-Repository
msgErrTargetRepoRequired=Bitte spezifizieren Sie ein Ziel-Repository
msgErrScheduleRequired=Bitte geben Sie einen Zeitplan an
msgErrScriptSourceRequired=Bitte geben Sie die Quelle des Transformationsskriptes an
msgErrHarvesterMissingSourceTarget=Harvester kann mangels Quell- und/oder Zielrepository nicht angezeigt werden
msgInfoRepoCreated=Repository erfolgreich erstellt
msgInfoRepoUpdated=Repository erfolgreich aktualisiert
msgErrRepoInUse=Repository wird verwendet
msgErrUserPwMismatch=Passw\u00F6rter stimmen nicht \u00FCberein
msgErrUserAuthFail=Authentifizierung fehlgeschlagen
msgInfoUserAlreadyLoggedIn=Sie sind bereits angemeldet mit dem Konto
msgInfoUserUpdated=Profil erfolgreich aktualisiert
msgErrNameExists=Name existiert bereits
msgErrInvalidGitCredentials=Ung\u00fcltige Git Anmeldeinformationen
msgErrUrlRequired=Bitte geben Sie eine g\u00fcltige Git URL an
msgErrBranchRequired=Bitte geben Sie einen g\u00fcltigen Branch an
msgErrGitGeneral=Bei der Verarbeitung des Git Repositories ist ein Fehler aufgetreten
msgErrMergeFailed=Merge fehlgeschlagen
msgErrFailedToReadDir=Git Verzeichnis konnte nicht gelesen werden
msgErrFailedToReadScriptFile=Skript Datei konnte nicht gelesen werden
msgErrInvalidGitRemote=Ung\u00fcltiges Git Repository angegeben
msgInfoRepoDeleted=Repository erfolgreich entfernt
msgWarnFilesNotCleaned=Einige Dateien konnten nicht entfernt werden
msgErrCouldNotDeleteRepoInUse=Repository konnte nicht entfernt werden. Folgende Harvester verwenden noch dieses Repository:
msgInfoGitRepoFetched=Git Repository erfolgreich abgerufen
msgErrGitRepoFetchFailed=Git Repository konnte nicht abgerufen werden
msgRequiredMiscName=Bitte geben Sie einen Namen an
msgRequiredMiscUserName=Bitte geben Sie einen Benutzernamen an
msgRequiredMiscPassword=Bitte geben Sie ein Passwort an
msgRequiredMiscPasswordConfirm=Bitte geben Sie Ihr Passwort erneut ein
msgRequiredRegistrationEmail=Bitte geben Sie eine eMail-Adresse an
msgRequiredRepositoryUrl=Bitte geben Sie eine URL an
msgRequiredRepositoryPublisher=Bitte geben Sie einen Herausgeber an
msgRequiredRepositoryLanguage=Bitte w\u00e4hlen Sie eine Sprache aus
msgInfoFilterNotSupported=Das eingestellte Quell-Repository unterst\u00fctzt keine Filter
msgErrFilterInvalid=Ein oder mehrere Filter sind unvollst\u00e4ndig

## Harvester
titleHarvesterSingular=Harvester
titleHarvesterPlural=Harvesters
titleHarvesterEdit=Harvester bearbeiten
titleHarvesterShow=Harvester anzeigen
buttonHarvesterNew=New harvester
buttonHarvesterUpload=Import harvester
buttonHarvesterShowScript=Transformation script
labelHarvesterSourceRepo=Quelle
labelHarvesterTargetRepo=Ziel
labelHarvesterSchedule=Zeitplan
labelHarvesterFilter=Filter
labelHarvesterTransformation=Transformation
labelHarvesterRecentlyFinished=K\u00FCrzlich beendete Harvester
labelHarvesterCurrentlyRunning=Derzeit laufende Harvester
labelHarvesterScheduled=Eingereihte Harvester
labelHarvesterOwned=Verwaltete Harvester
inputHarvesterDate=Erste Ausf\u00FChrung
inputHarvesterErrorsOnly=Nur nach Harvestern mit Fehlern suchen
inputHarvesterEmailNotification=EMail Benachrichtigungen
inputHarvesterForceUpdate=Aktualisierung erzwingen
inputHarvesterSelectRepository=Repository
infoHintHarvesterExampleScript=Ein Beispielskript wird bereitgestellt, wenn Quelle und Ziel Repositories konfiguriert werden und das Feld leer gelassen wird.
infoPanelHarvester=<p>Wenn Sie den Harvester als \u201Egeteilt\u201C klassifizieren, k\u00F6nnen andere Benutzer diesen Harvester sehen, aber nicht bearbeiten.</p><p>Die Angabe eines Namens sowie eines Quell- und Zielrepositories ist zwingend erforderlich. Au\u00DFerdem ben\u00F6tigt werden Informationen zur Umwandlung der Daten.</p><p>Bitte beachten Sie, dass API Keys sowie Git-Zugangsdaten derzeit noch im Klartext gespeichert werden. Sie sollten den f\u00FCr Ihren Git Anbieter gew\u00e4hlten Nutzernamen und Ihr Passwort daher nicht f\u00FCr weitere Dienste verwenden und auf Ihrem Git-Konto keine sensiblen Daten speichern.


## Repositories
titleRepositorySingular=Repository
titleRepositoryPlural=Repositories
titleRepositoryEdit=Repository bearbeiten
buttonRepositoryNew=Neues Repository
inputRepositoryApiKey=API Key
inputRepositoryType=Typ
inputRepositoryTypeAll=Alle Typen
inputRepositorySpatial=Raumbezug
inputRepositoryIncremental=Inkrementell
inputRepositoryPublisher=Herausgeber
inputRepositoryPublisherEmail=EMail-Adresse des Herausgebers
menuRepositoryExportInstances=Export
menuRepositoryImportInstances=Import
labelRepositoryOwned=Verwaltete Repositories
labelRepositoryMostUsed=Beliebtestes Repository
infoPanelRepository=<p>Ein Repository kann sowohl ein Quell- als auch ein Zielportal abbilden. Wie bei Harvestern auch kann ein Repository als \u201Egeteilt\u201C klassifiziert werden, sodass andere Benutzer dieses einsehen k\u00F6nnen.</p><p>Wichtig ist, dass Sie den richtigen Typ, einen Namen sowie den Herausgeber eintragen. Auch die Adresse (URL) wird zwingend ben\u00F6tigt. Hier ist zu beachten, dass diese nicht mit einem Schr\u00e4gstrich abschlie\u00DFt</p>


## Runs
titleRunAll=L\u00e4ufe
titleRunShow=Lauf anzeigen
linkRunAll=Alle L\u00e4ufe
infoPanelRunRunning=L\u00e4uft
nextRun=N\u00E4chste
labelRunDuration=Laufzeit
labelRunTimeStart=Start
labelRunTimeEnd=Ende
labelRunTimeAVG=Durchschn. Laufzeit
labelRunTotal=Fertiggestellte L\u00e4ufe
labelRunTotalErrors=Fehlerhafte L\u00e4ufe
labelRunTotalNoErrors=Fehlerfreie L\u00e4ufe
labelRunMostCommonError=H\u00e4ufigste Fehlerkategorie
labelRunProcessing=Verarbeitet
labelRunAdded=Hinzugef\u00FCgt
labelRunUpdated=Aktualisiert
labelRunRejected=Abgelehnt
labelRunSkipped=\u00DCbersprungen
buttonRunGroupByStatus=Nach Status gruppieren


## Logs
titleLogAll=Logeintr\u00e4ge
titleLogShow=Logeintrag anzeigen
buttonLogMore=Mehr Logdateien
labelLogRecent=Neueste Logs
labelLogSeverityInfoPlural=Infos
labelLogSeverityWarningPlural=Warnungen
labelLogSeverityErrorPlural=Fehler


## Git
inputGitBranch=Branch
inputGitRequiresCredentials=Ben\u00F6tigt Benutzerdaten
inputGitSshAuth=Verwendet SSH
inputGitRepoFile=Skript
buttonGitFetch=Pull
infoDescriptionGit=<p>ANMELDEINFORMATIONEN WERDEN DERZEIT IM KLARTEXT GESPEICHERT</p><p>Der Name wird anderen Benutzern helfen dieses Repository wiederzufinden, vorausgesetzt, dass es auf geteilt gesetzt ist.</p>Bitte beachten Sie, dass die Authentifizierung via SSH momentan nicht unterst\u00fctzt wird..


## User profile
titleRegistration=Benutzerregistrierung
titleProfile=Profil
linkProfileLogin=Anmelden
linkProfileLogout=Abmelden
inputProfileUsername=Benutzername
inputProfileFirstname=Vorname
inputProfileLastname=Familienname
inputProfileOrganisation=Organisation
inputProfileEmail=EMail-Adresse
buttonProfilePasswordChange=Passwort \u00E4ndern
buttonProfileRegistration=Registrieren
buttonProfileRemoveAccount=Benutzer l\u00F6schen
infoDescriptionProfileLogin=<p>Melden Sie sich mit Ihrem Benutzernamen und Passwort bei dem Metadatentransformator an, um Zugriff auf Ihre Repositories, Harvester sowie Profilfunktionen zu erhalten.
infoDescriptionProfileRegistration=<p>Ein Konto erlaubt Ihnen selber sowohl Repositories als auch Harvester anzulegen</p>Andere Nutzer werden Ihren Benutzername, Ihre Profilbeschreibung sowie Ihre e-Mail Adresse einsehen k\u00F6nnen.
linkProfileRegistrationAccountExists=Sie besitzen bereits ein Konto? Melden Sie sich an!
linkProfileLoginAccountNoExists=Sie besitzen noch kein Konto? Registrieren Sie sich!

## Placeholders
placeholderPassword=Passwort
placeholderAccount=Benutzername
placeholderWIP=Im Aufbau
placeholderSearchFor=Suche nach...


## Enumerations
visibility=Visibility
visibilityPrivate=privat
visibilityPublic=geteilt
severity=Severity
severityInfo=Info
severityWarning=Warnung
severityError=Fehler
category=Category
categorySystem=System
categoryConnection=Verbindung
categoryTransformation=Transformation
categoryDataset=Datensatz
categoryUnknown=Unbekannt

harvestFrequency=Harvesting Frequenz
harvestFrequencyEvery_five_minutes=Alle f\u00fcnf Minuten
harvestFrequencyHourly=St\u00fcndlich
harvestFrequencyDaily=T\u00e4glich
harvestFrequencyWeekly=W\u00f6chentlich
harvestFrequencyMonthly=Monatlich
harvestFrequencyYearly=J\u00e4hrlich
harvestFrequencyManually=Manuell
runState=Run state
runStatusScheduled=Eingereiht
runStatusNotScheduled=Nicht eingereiht
runStatusRunning=L\u00E4uft
runStatusError=Fehler
runStatusInterrupted=Unterbrochen
runStatusFinished=Fertig

serviceStage=Abarbeitungsschritt
serviceStageInitialize=Initialisierung
serviceStageExport=Export
serviceStageTransform=Transformierung
serviceStageImport=Import
serviceStageFilter=Filter
serviceStageMapping=Mapping
serviceStageDelete=L\u00f6schen

orderBy=Sortieren nach
orderByDate=Datum
orderByRunStatus=Status
orderByAscending=Aufsteigend
orderByDescending=Absteigend

transformationScriptSource=Skriptquelle
transformationScriptSourceCustom=Serverupload
transformationScriptSourceGit=Git Repository
labelRunDeleted=Deleted
schedulingsTitle=Scheduling
titleUserManagement=User Management
msgErrLastAdmin=You cannot delete the one and only administrator!