package de.fhg.fokus.odp.harvester.sections.importer;

import de.fhg.fokus.odp.harvester.persistence.enums.LogEnums;
import de.fhg.fokus.odp.harvester.persistence.enums.ServiceStage;
import de.fhg.fokus.odp.harvester.sections.JobSection;
import de.fhg.fokus.odp.harvester.service.RunHandler;
import de.fhg.fokus.odp.harvester.util.JenaUtils;
import org.apache.commons.io.IOUtils;
import org.apache.jena.query.*;
import org.apache.jena.rdf.model.*;
import org.apache.jena.rdfconnection.RDFConnection;
import org.apache.jena.rdfconnection.RDFConnectionFactory;
import org.apache.jena.riot.RDFDataMgr;
import org.apache.jena.riot.RDFFormat;
import org.apache.jena.shared.JenaException;
import org.apache.jena.sparql.engine.http.QueryExceptionHTTP;
import org.apache.jena.vocabulary.DCAT;
import org.apache.jena.vocabulary.DCTerms;
import org.apache.jena.vocabulary.RDF;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.Namespace;
import org.jdom2.input.SAXBuilder;

import javax.enterprise.context.Dependent;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

@Dependent
public class SPARQLImportingSection extends ImportingSection<Document> {

	private static final Document POISON = new Document();

	private ModelExtract extractor = new ModelExtract(new StatementBoundaryBase() {
		@Override
		public boolean stopAt(Statement s) {
			return false;
		}
	});

	private Set<String> identifiers = new HashSet<>();

	private AtomicInteger read = new AtomicInteger();

	public SPARQLImportingSection() {
		super(Document.class);
	}

	@Override
	protected Document poisonObject() {
		return POISON;
	}

	@Override
	public Void call() {
		log.info("Starting import section for {}.", run.getHarvester().getName());

		QueryExecution exec = QueryExecutionFactory.sparqlService(run.getHarvester().getSource().getFormattedUrl(), "SELECT (COUNT(?s) AS ?count) WHERE { ?s a <" + DCAT.Dataset.getURI() + "> }");
		ResultSet result = exec.execSelect();
		if (result.hasNext()) {
			RDFNode count = result.next().get("count");
			if (count.isLiteral()) {
				run.setNumberToProcess(count.asLiteral().getInt());
			}
		}

		int offset = 29700;

		Query query = QueryFactory.create("SELECT ?graph WHERE { { SELECT DISTINCT ?graph WHERE { graph ?graph { ?s a <" + DCAT.Dataset.getURI() + "> } } ORDER BY ?graph } }");
//		Query query = QueryFactory.create("select distinct ?graph WHERE { graph ?graph { ?s a <" + JenaUtils.NS_DCAT + "Dataset> } } order by ?graph");
		query.setLimit(100);
		query.setQuerySelectType();

		List<Document> datasets;
		try {
			do {
				datasets = nextPage(query, offset);

				for (Document dataset : datasets) {
					put(dataset);
				}
				offset += 100;

			} while (offset < run.getRun().getNumberToProcess() && !datasets.isEmpty());

			if (read.get() < run.getRun().getNumberToProcess()) {
				log.warn("Import section could not read all announced datasets");
				run.addLogWarning("Import section could not read all announced datasets.", ServiceStage.import_stage, LogEnums.Category.system);
			}

			finish();

			log.info("Finished import section for {}. {} datasets read, {} datasets imported.", run.getHarvester().getName(), read.get(), putCounter());
			run.addLogInfo("Finished import section. " + read.get() + " datasets read, " + putCounter() + " datasets imported.", ServiceStage.import_stage, LogEnums.Category.system);

		} catch (InterruptedException e) {
			log.warn("Import thread interrupted", e);
			run.addLogWarning("Import section interrupted.", ServiceStage.import_stage, LogEnums.Category.system);
		}
		return null;
	}

	protected List<Document> nextPage(Query query, int offset) throws InterruptedException {
		List<Document> page = new ArrayList<>();

		query.setOffset(offset);

		try (RDFConnection connection = RDFConnectionFactory.connect(run.getHarvester().getSource().getFormattedUrl())) {
			connection.querySelect(query, qs -> {
				RDFNode graph = qs.get("graph");
				if (graph.isURIResource()) {
					String graphUri = graph.asResource().getURI();

					Query graphQuery = QueryFactory.create("CONSTRUCT { ?s ?p ?o } WHERE { GRAPH <" + graphUri + "> { ?s ?p ?o } }");
					try {
						Model dataset = connection.queryConstruct(graphQuery);

//						Model dataset = connection.fetch(graphUri);

						page.addAll(parseResult(dataset));
						dataset.close();
					} catch (JenaException e) {
						log.error("query graph " + graphUri, e);
						run.addLogError("query graph " + graphUri, ServiceStage.import_stage, LogEnums.Category.dataset, null);
						run.incrementRejected();
					}
				}
			});
		} catch (QueryExceptionHTTP e) {
			failed("query remote", e);
		} catch (JenaException e) {
			failed("query remote", e);
		}
		return page;
	}

	private List<Document> parseResult(Model model) {
		List<Document> datasets = new ArrayList<>();

		ResIterator it = model.listResourcesWithProperty(RDF.type, DCAT.Dataset);
		if (!it.hasNext()) {
			it = model.listResourcesWithProperty(RDF.type, model.createResource("http://rdfs.org/ns/void#Dataset"));
		}
		while (it.hasNext()) {
			Resource resource = it.nextResource();
			read.incrementAndGet();

			if (resource.isURIResource()) {
				identifiers.add(resource.getURI());
			} else {
				Statement id = resource.getProperty(DCTerms.identifier);
				if (id != null) {
					identifiers.add(id.getLiteral().getString());
				} else {
					read.decrementAndGet();
				}
			}

			Model d;
			try {
				d = extractor.extract(resource, model);
				d.setNsPrefixes(JenaUtils.DCATAP_PREFIXES);
			} catch (Exception e) {
				log.error("model extraction", e);
				run.incrementRejected();
				run.addLogError(e.getMessage(), ServiceStage.import_stage, LogEnums.Category.dataset, null);
				continue;
			}

			// we should remove in general all other rdf:types except dcat:Dataset
			d.remove(resource, RDF.type, d.createResource("https://data.gov.cz/slovník/nkod/typ-datové-sady-dle-zdroje/LKOD"));

 			ByteArrayOutputStream output = new ByteArrayOutputStream();
			try {
				RDFDataMgr.write(output, d, RDFFormat.RDFXML_ABBREV);
				SAXBuilder builder = new SAXBuilder();
				Document doc = builder.build(IOUtils.toInputStream(output.toString("UTF-8"), Charset.forName("UTF-8")));
				Element elem = doc.getRootElement().getChild("Dataset", Namespace.getNamespace("http://www.w3.org/ns/dcat#"));
				if (elem != null) {
					datasets.add(new Document(elem.detach()));
				} else {
					log.error("extracting dataset: {}", output.toString("UTF-8"));
					run.incrementRejected();
					run.addLogError("Extracting dataset from model.", ServiceStage.import_stage, LogEnums.Category.dataset, output.toString("UTF-8"));
				}
			} catch (JDOMException | IOException e) {
				log.error("building document from model", e);
				run.incrementRejected();
				run.addLogError(e.getMessage(), ServiceStage.import_stage, LogEnums.Category.dataset, output.toString());
			} catch (Exception e) {
				log.error("write model", e);
				run.incrementRejected();
				run.addLogError(e.getMessage(), ServiceStage.import_stage, LogEnums.Category.dataset, null);
			}

			d.close();
		}

		return datasets;
	}
    
	@Override
	public Set<String> listIdentifiers() {
		return read.get() < run.getRun().getNumberToProcess() ? Collections.emptySet() : identifiers;
	}

}
