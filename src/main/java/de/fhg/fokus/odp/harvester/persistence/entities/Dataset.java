package de.fhg.fokus.odp.harvester.persistence.entities;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by sim on 10.01.2017.
 */
@Entity
public class Dataset {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Integer id;

    private String name;

    @Lob
    private String content;

    @Lob
    private String originalContent;

    @Lob
    private String previousContent;

    private String error;

    private Integer catalog;

    @Temporal(TemporalType.TIMESTAMP)
    private Date created;

    @PrePersist
    public void onCreate() {
        created = new Date();
    }

    public Date getCreated() {
        return created;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getOriginalContent() {
        return originalContent;
    }

    public void setOriginalContent(String originalContent) {
        this.originalContent = originalContent;
    }

    public String getPreviousContent() {
        return previousContent;
    }

    public void setPreviousContent(String previousContent) {
        this.previousContent = previousContent;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public Integer getCatalog() {
        return catalog;
    }

    public void setCatalog(Integer catalog) {
        this.catalog = catalog;
    }

}
