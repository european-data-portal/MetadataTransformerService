package de.fhg.fokus.odp.harvester.sections.importer;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

import javax.enterprise.context.Dependent;
import javax.transaction.Transactional;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import de.fhg.fokus.odp.harvester.exceptions.GeneralHttpException;
import de.fhg.fokus.odp.harvester.exceptions.ProtocolException;
import de.fhg.fokus.odp.harvester.http.ckan.CkanError;
import de.fhg.fokus.odp.harvester.http.ckan.CkanResponse;
import de.fhg.fokus.odp.harvester.http.ckan.CkanResult;
import de.fhg.fokus.odp.harvester.persistence.entities.Filter;
import de.fhg.fokus.odp.harvester.persistence.entities.Run;
import de.fhg.fokus.odp.harvester.persistence.enums.LogEnums;
import de.fhg.fokus.odp.harvester.persistence.enums.RunState;
import de.fhg.fokus.odp.harvester.persistence.enums.ServiceStage;
import de.fhg.fokus.odp.harvester.sections.JobSection;
import de.fhg.fokus.odp.harvester.service.RunHandler;

@Dependent
//@Typed(CkanImportingSection.class)
public class CkanImportingSection extends ImportingSection<ObjectNode> {

	protected static final int NUMBER_OF_BATCH_ROWS = 300;

	private static final ObjectNode POISON = new ObjectMapper().createObjectNode();
	
	protected String filterString;

	protected AtomicInteger read = new AtomicInteger();

	public CkanImportingSection() {
		super(ObjectNode.class);
	}

	@Override
	protected ObjectNode poisonObject() {
		return POISON;
	}

	public boolean init(RunHandler run, JobSection section, int capacity) {
		if (!super.init(run, section, capacity)) {
			return false;
		}
		prepareFilter();
		return true;
	}

	public Void call() {
		log.info("Starting import section for {}.", run.getHarvester().getName());

		try {
			int offset = 0;
			List<ObjectNode> page;
			do {
				page = nextPage(offset);
				for (ObjectNode dataset : page) {
					put(dataset);
				}
				offset += page.size();

			} while (offset < run.getRun().getNumberToProcess() && !page.isEmpty());

			if (page.isEmpty() && read.get() < run.getRun().getNumberToProcess()) {
				run.addLogWarning("Finished import section due to empty page, but didn't read expected number: " + read.get() + " datasets read, " + run.getRun().getNumberToProcess() + " datasets expected.", ServiceStage.import_stage, LogEnums.Category.system);
			}

			finish();

		} catch (GeneralHttpException e) {
			failed("(" + e.getStatusCode() + " " + e.getMessage() + ") " + e.getBody(), e);
		} catch (ProtocolException | IOException e) {
			failed(e.getMessage(), e);
		} catch (InterruptedException e) {
			log.warn("Import thread interrupted", e);
			run.addLogWarning("Import section interrupted.", ServiceStage.import_stage, LogEnums.Category.system);
			return null;
		}

		log.info("Finished import section for {}.", run.getHarvester().getName());
		run.addLogInfo("Finished import section. " + read.get() + " datasets read, " + putCounter() + " datasets imported.", ServiceStage.import_stage, LogEnums.Category.system);

		return null;
	}
	
    protected List<ObjectNode> nextPage(int start) throws IOException, ProtocolException {
    	String request = buildRequest(start);
    	JsonNode response = httpClient.get(request);
    	CkanResponse ckanResponse = new CkanResponse(response);
    	if (ckanResponse.isError()) {
    		CkanError ckanError = ckanResponse.getError();
    		throw ckanError.asException();
    	} else if (ckanResponse.isSuccess()) {
    		CkanResult ckanResult = ckanResponse.getResult();
    		return parseResult(ckanResult.getContent());
    	} else {
    		throw new ProtocolException("unknown response format");
    	}
	}

	protected String buildRequest(int start) {
		return "/api/action/package_search?rows=" + NUMBER_OF_BATCH_ROWS + "&start=" + start + "&" + filterString;		
	}
	
    protected List<ObjectNode> parseResult(JsonNode result) {
		run.setNumberToProcess(result.path("count").asInt());
		ArrayList<ObjectNode> list = new ArrayList<>();
		String name = result.path("results").isMissingNode() ? "result" : "results";
		ArrayNode results = (ArrayNode) result.path(name);
		if (!results.isMissingNode()) {
			read.addAndGet(results.size());
			list.ensureCapacity(results.size());
			for (JsonNode element : results) {
				list.add((ObjectNode) element);
			}
		}
		if (list.isEmpty()) {
			try {
				run.addLogError("Received empty page", ServiceStage.import_stage, LogEnums.Category.system, new ObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(result));
			} catch (JsonProcessingException e) {
				log.error("attach result to log", e);
			}
		}
		return list;    	
    }

	protected void prepareFilter() {
		log.debug("preparing filter");

		StringBuilder fsb = new StringBuilder();

		if (run.getHarvester().getSource().isIncremental()) {
			Run latest = run.getLatestRun();
			if (latest != null) {
				fsb.append("q=");
				DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
				fsb.append("metadata_modified:[").append(dateFormat.format(latest.getStartTime())).append("%20TO%20*]");
			}			
		}

		if (fsb.length() > 0) {
			fsb.append("&");
		}
		fsb.append("fq=!(type:harvest)");

		List<Filter> filters = run.getHarvester().getEnabledAndSortedFilters();

		//Build filter String
		if (!filters.isEmpty()) {
			run.addLogInfo("Number of filters: " + filters.size(), ServiceStage.filter_stage, LogEnums.Category.system);
			for (Filter filter : filters) {
				try {
					fsb.append(filter.getAndOp() ? "%20AND%20" : "%20OR%20");
					if (filter.isNegation()) {
						fsb.append("NOT%20"); 	//Preceding space is taken care of by sb append above
					}
					fsb.append(URLEncoder.encode(filter.getAttribute() + ":" + filter.getValue(), "utf-8"));
				} catch (UnsupportedEncodingException e) {
					log.error("url encoding filter", e);
				}
			}
		}

		filterString = fsb.toString();

		if (filterString.trim().isEmpty()) {
			run.addLogInfo("No filter to use.", ServiceStage.filter_stage, LogEnums.Category.system);
		} else {
			run.addLogInfo("Final filter to use: " + filterString, ServiceStage.filter_stage, LogEnums.Category.system);
		}
		log.debug("filter prepared");
	}

	@Override
	public String getDatasetUrl(ObjectNode dataset) {
		return run.getHarvester().getSource().getFormattedUrl() + "/dataset/" + dataset.path("name").textValue();
	}

	@Override
	public Set<String> listIdentifiers() throws IOException, ProtocolException {
		String request = "/api/3/action/package_list?offset=0"; // offset just set cause some portals have problems when params are missing
		JsonNode result = httpClient.get(request);

		CkanResponse ckanResponse = new CkanResponse(result);
		if (ckanResponse.isError()) {
			CkanError ckanError = ckanResponse.getError();
			throw ckanError.asException();
		} else if (ckanResponse.isSuccess()) {
			JsonNode resultContent = ckanResponse.getResult().getContent();
			if (resultContent.isArray()) {
				Set<String> ids = new HashSet<>();
				resultContent.forEach(n -> ids.add(n.textValue()));
				return ids;
			} else {
				return Collections.emptySet();
			}
		} else {
			throw new ProtocolException("unknown response format:\n" + new ObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(result));
		}
	}

}
