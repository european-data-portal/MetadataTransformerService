package de.fhg.fokus.odp.harvester.actions;

import de.fhg.fokus.odp.harvester.persistence.LogsManager;
import de.fhg.fokus.odp.harvester.persistence.entities.Log;
import de.fhg.fokus.odp.harvester.persistence.enums.RepositoryType;

import javax.enterprise.inject.Produces;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.transaction.Transactional;

import java.io.Serializable;

@Named
@ViewScoped
public class LogAction implements Serializable {

	private static final long serialVersionUID = 1077368343033780994L;

	@Inject
	private LogsManager logsManager;

	private Log log;

	private String format;

	private Integer id;

	@Produces
	@Named
	public Log getLog() {
		return log;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

	@Transactional
	public void load() {
		log = logsManager.find(id);

		switch (log.getServiceStage()) {
			case export_stage:
				format = "javascript";
				break;
			case import_stage:
			case transformation_stage:
				RepositoryType repoType = log.getRun().getHarvester().getSource().getRepositoryType();
				switch (repoType) {
					case ckan:
					case rdf_ckan:
					case udata:
						format = "javascript";
						break;
					case dcatap:
					case rdf:
					case rss:
						format = "xml";
						break;
					default:
						format = "text";
				}
				break;
			default:
				format = "text";
		}
	}

	public String getFormat() {
		return format;
	}
	
}
