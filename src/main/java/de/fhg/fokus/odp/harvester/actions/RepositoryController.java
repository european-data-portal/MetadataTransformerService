package de.fhg.fokus.odp.harvester.actions;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.transaction.Transactional;

import de.fhg.fokus.odp.harvester.persistence.RepositoriesManager;
import de.fhg.fokus.odp.harvester.persistence.entities.Repository;
import de.fhg.fokus.odp.harvester.persistence.enums.RepositoryType;

import java.io.Serializable;

@Named
@ViewScoped
public class RepositoryController implements Serializable {

	private static final long serialVersionUID = -3459450762127122511L;

	private String searchTerm = "";

	private List<RepositoryType> selectedRepositoryTypes;
	
	private List<Repository> repositories = new ArrayList<>();

	private final Integer PAGESIZE = 15;

	private Integer offset = 0;

	private Integer total;

	@Inject
	private RepositoriesManager repositoriesManager;

	public void setOffset(Integer offset) {
		this.offset = offset;
	}

	public Integer getOffset() {
		return offset;
	}

	public Integer getRows() {
		return total - PAGESIZE > offset ? PAGESIZE : total - offset;
	}

	public Integer getPageSize() {
		return PAGESIZE;
	}

	public Integer getTotal() {
		return total;
	}

	@Transactional
	public void load() {
		selectedRepositoryTypes = new ArrayList<>(Arrays.asList(RepositoryType.values()));
		total = repositoriesManager.countRepositories(searchTerm.toLowerCase().trim(), selectedRepositoryTypes).intValue();
		repositories = repositoriesManager.listRepositories(PAGESIZE, offset, searchTerm, selectedRepositoryTypes);
	}

	@Transactional
	public void search() {
		repositories = repositoriesManager.listRepositories(PAGESIZE, offset, searchTerm, selectedRepositoryTypes);
		total = repositoriesManager.countRepositories(searchTerm.toLowerCase().trim(), selectedRepositoryTypes).intValue();
	}

	public List<Repository> getAllRepositories() {
		return repositoriesManager.listRepositories(null, null, "", new ArrayList<>(Arrays.asList(RepositoryType.values())));
	}

	public List<Repository> getRepositories() {
		return repositories;
	}

	public String getMostUsedRepository() {
		return repositoriesManager.getMostUsedRepository();
	}

	public String countUserRepositories() {
		return String.valueOf(repositoriesManager.countUserRepositories());
	}

	public RepositoryType[] getRepositoryTypes() {
		return RepositoryType.values();
	}

	public String getSearchTerm() {
		return searchTerm;
	}
	
	public void setSearchTerm(String searchTerm) {
		this.searchTerm = searchTerm;
	}

	public List<RepositoryType> getSelectedRepositoryTypes() {
		return selectedRepositoryTypes;
	}

	public void setSelectedRepositoryTypes(List<RepositoryType> selectedRepositoryTypes) {
		this.selectedRepositoryTypes = selectedRepositoryTypes;
		search();
	}

	@Transactional
	public void nextPage() {
		offset += PAGESIZE;
		repositories = repositoriesManager.listRepositories(PAGESIZE, offset, searchTerm, selectedRepositoryTypes);
	}

	@Transactional
	public void previousPage() {
		offset -=PAGESIZE;
		repositories = repositoriesManager.listRepositories(PAGESIZE, offset, searchTerm, selectedRepositoryTypes);
	}

}
