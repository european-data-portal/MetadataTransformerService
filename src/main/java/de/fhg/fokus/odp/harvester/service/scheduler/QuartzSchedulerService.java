package de.fhg.fokus.odp.harvester.service.scheduler;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import static org.quartz.JobBuilder.*;
import static org.quartz.JobKey.*;
import static org.quartz.TriggerBuilder.*;
import static org.quartz.TriggerKey.*;

import java.util.Calendar;
import java.util.Date;

import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.fhg.fokus.odp.harvester.cdi.CdiJobFactory;
import de.fhg.fokus.odp.harvester.persistence.enums.Frequency;

@ApplicationScoped
public class QuartzSchedulerService {
	
	private final Logger log = LoggerFactory.getLogger(getClass());
	
	private Scheduler scheduler;
	
	@Inject
	private CdiJobFactory cdiJobFactory;
	
	@PostConstruct
	public void init() {
		try {
			scheduler = new StdSchedulerFactory().getScheduler();
			scheduler.setJobFactory(cdiJobFactory);
			scheduler.start();
		} catch (SchedulerException e) {
			log.error("Getting scheduler", e);
		}
	}

	public Date scheduleJob(String identity, Frequency frequency, Date firstTime) {
		JobDetail detail = newJob(HarvesterJob.class).withIdentity(identity + (frequency == Frequency.manually ? ".manually" : ""), identity).build();
		ScheduleBuilder<?> scheduleBuilder = null;
		switch (frequency) {
			case every_five_minutes:
				scheduleBuilder = CalendarIntervalScheduleBuilder.calendarIntervalSchedule().withIntervalInMinutes(5).withMisfireHandlingInstructionDoNothing();
				break;
			case hourly:
				scheduleBuilder = CalendarIntervalScheduleBuilder.calendarIntervalSchedule().withIntervalInHours(1).withMisfireHandlingInstructionDoNothing();
				break;
			case daily:
				scheduleBuilder = CalendarIntervalScheduleBuilder.calendarIntervalSchedule().withIntervalInDays(1).withMisfireHandlingInstructionDoNothing();
				break;
			case weekly:
				scheduleBuilder = CalendarIntervalScheduleBuilder.calendarIntervalSchedule().withIntervalInWeeks(1).withMisfireHandlingInstructionDoNothing();
				break;
			case monthly:
				scheduleBuilder = CalendarIntervalScheduleBuilder.calendarIntervalSchedule().withIntervalInMonths(1).withMisfireHandlingInstructionDoNothing();
				break;
			case yearly:
				scheduleBuilder = CalendarIntervalScheduleBuilder.calendarIntervalSchedule().withIntervalInYears(1).withMisfireHandlingInstructionDoNothing();
				break;
			default:
				break;
		}
		if (scheduleBuilder != null) {
			if (firstTime == null) {
				Calendar calendar = Calendar.getInstance();
				calendar.add(Calendar.MINUTE, 5);
				firstTime = calendar.getTime();
			}
			Trigger trigger = newTrigger().withIdentity(identity, identity).withSchedule(scheduleBuilder).startAt(firstTime).build();
			Date next = trigger.getFireTimeAfter(new Date());	
			trigger = newTrigger().withIdentity(identity, identity).withSchedule(scheduleBuilder).startAt(next).build();
			
			try {
				Trigger oldTrigger = scheduler.getTrigger(triggerKey(identity, identity));
				if (oldTrigger != null) {
					Date first = scheduler.rescheduleJob(triggerKey(identity, identity), trigger);
					Date f1 = trigger.getNextFireTime();
					return first;
				} else {
					Date first = scheduler.scheduleJob(detail, trigger);
					Date f1 = trigger.getNextFireTime();
					return first;					
				}
			} catch (SchedulerException e) {
				log.error("Scheduling new job", e);
			}
		} else if (frequency == Frequency.manually) {
			Trigger trigger = newTrigger().startNow().build();
			
			try {
				scheduler.scheduleJob(detail, trigger);
			} catch (SchedulerException e) {
				log.error("Starting new job", e);
			}
		}
		return null;
	}

	public boolean deleteJob(String identity) {
		try {
			return scheduler.deleteJob(jobKey(identity, identity));
		} catch (SchedulerException e) {
			log.error("deleting job", e);
		}
		return false;
	}

	public void interruptJob(String identity) {
		JobExecutionContext context = findJobExecutionContext(identity);
		if (context != null) {
			try {
				scheduler.interrupt(context.getJobDetail().getKey());
			} catch (SchedulerException e) {
				log.error("interrupting job", e);
			}					
		}
	}
	
	private JobExecutionContext findJobExecutionContext(String identity) {
		try {
			for (JobExecutionContext context : scheduler.getCurrentlyExecutingJobs()) {
				if (context.getJobDetail().getKey().getGroup().equals(identity)) {
					return context;
				}
			}
		} catch (SchedulerException e) {
			log.error("find running job execution context", e);
		}
		return null;
	}
	
	public InterruptableJob getHarvesterJob(String identifier) {
		JobExecutionContext context = findJobExecutionContext(identifier);
		if (context != null) {
			return (InterruptableJob) context.getJobInstance();
		} else {
			return null;
		}
	}

	public boolean isRunning(String identifier) {
		return findJobExecutionContext(identifier) != null;
	}
	
}
