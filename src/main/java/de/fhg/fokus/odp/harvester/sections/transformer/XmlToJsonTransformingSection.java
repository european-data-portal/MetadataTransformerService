package de.fhg.fokus.odp.harvester.sections.transformer;

import com.fasterxml.jackson.core.JsonParser.Feature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import de.fhg.fokus.odp.harvester.persistence.enums.LogEnums;
import de.fhg.fokus.odp.harvester.persistence.enums.ServiceStage;
import de.fhg.fokus.odp.harvester.sections.JobSection;
import de.fhg.fokus.odp.harvester.sections.importer.ImportingSection;
import de.fhg.fokus.odp.harvester.service.RunHandler;
import de.fhg.fokus.odp.harvester.transformer.scripttransformer.XslScriptTransformer;

import org.jdom2.Document;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.Dependent;
import javax.xml.transform.TransformerException;
import java.io.IOException;
import java.util.Collections;

/**
 * Transforms an XML document to another JsonObject using the mapping provided by the harvester
 * Created by sim on 20.09.2015.
 */
@Dependent
public class XmlToJsonTransformingSection extends TransformingSection<Document, ObjectNode> {

    private final Logger log = LoggerFactory.getLogger(getClass());

	private static final ObjectNode POISON = new ObjectMapper().createObjectNode();

	private XslScriptTransformer transformer;
	
	@Override
	public ObjectNode poisonObject() {
		return POISON;
	}
	
	public boolean init(RunHandler run, JobSection section, int capacity) {
		if (super.init(run, section, capacity)) {
			transformer = new XslScriptTransformer(script, Collections.singletonMap("repo_lang", run.getHarvester().getSource().getLanguage().getLanguageKey()));
			return true;
		} else {
			return false;
		}
	}
	
	@Override
	public ObjectNode transform(Document dataset) {
    	String targetDataset;
        try {
			if (log.isTraceEnabled()) {
				log.trace("transforming input:\n{}", new XMLOutputter(Format.getPrettyFormat()).outputString(dataset));
			}

			targetDataset = transformer.transform(dataset);

        	ObjectMapper mapper = new ObjectMapper();
        	mapper.configure(Feature.ALLOW_UNQUOTED_CONTROL_CHARS, true);
        	mapper.configure(Feature.ALLOW_BACKSLASH_ESCAPING_ANY_CHARACTER, true);
        	mapper.configure(Feature.ALLOW_UNQUOTED_FIELD_NAMES, true);

        	JsonNode tree = mapper.readTree(targetDataset);
        	if (tree.isObject()) {
				ObjectNode target = (ObjectNode) tree;
				String originalSource = ((ImportingSection<Document>) attached).getDatasetUrl(dataset);
				if (originalSource != null) {
					target.put("original_source", originalSource);
				}

				if (log.isTraceEnabled()) {
					log.trace("transforming output:\n{}", mapper.writerWithDefaultPrettyPrinter().writeValueAsString(target));
				}

				return target;
			} else if (tree.isTextual()) {
				log.error(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(tree));
			}

		} catch (TransformerException | IOException e) {
			run.incrementRejected();
			String d = new XMLOutputter(Format.getPrettyFormat()).outputString(dataset);
			log.error("transforming dataset: {}", d, e);
			run.addLogError(e.getMessage(), ServiceStage.transformation_stage, LogEnums.Category.transformation, d);
		}

        return null;
	}
	
}
