package de.fhg.fokus.odp.harvester.application;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath("/rest")
public class ApplicationResource extends Application {

}
