package de.fhg.fokus.odp.harvester.sections.importer;

import de.fhg.fokus.odp.harvester.persistence.enums.LogEnums;
import de.fhg.fokus.odp.harvester.persistence.enums.ServiceStage;
import de.fhg.fokus.odp.harvester.sections.JobSection;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

import javax.enterprise.context.Dependent;

import de.fhg.fokus.odp.harvester.service.RunHandler;
import de.fhg.fokus.odp.harvester.util.Hydra;
import de.fhg.fokus.odp.harvester.util.JenaUtils;
import org.apache.commons.io.IOUtils;
import org.apache.jena.query.Dataset;
import org.apache.jena.rdf.model.*;
import org.apache.jena.riot.*;
import org.apache.jena.vocabulary.DCAT;
import org.apache.jena.vocabulary.DCTerms;
import org.apache.jena.vocabulary.RDF;
import org.jdom2.Document;
import org.jdom2.JDOMException;
import org.jdom2.Namespace;
import org.jdom2.input.SAXBuilder;

@Dependent
public class RDFImportingSection extends ImportingSection<Document> {

    private static final Document POISON = new Document();

    private String nextPage;

    private Set<String> identifiers = new HashSet<>();

    private AtomicInteger read = new AtomicInteger();

    public RDFImportingSection() {
        super(Document.class);
    }

    @Override
    protected Document poisonObject() {
        return POISON;
    }

    @Override
    public Void call() {
        log.info("Starting import section for {}.", run.getHarvester().getName());

        try {
            do {
                List<Document> datasets = nextPage();

                for (Document dataset : datasets) {
                    put(dataset);
                }
            } while (nextPage != null);

            finish();

            log.info("Finished import section for {}. {} datasets read, {} datasets imported.", run.getHarvester().getName(), read.get(), putCounter());
            run.addLogInfo("Finished import section. " + read.get() + " datasets read, " + putCounter() + " datasets imported.", ServiceStage.import_stage, LogEnums.Category.system);

        } catch (InterruptedException e) {
            log.warn("Import thread interrupted", e);
            run.addLogWarning("Import section interrupted.", ServiceStage.import_stage, LogEnums.Category.system);
        }
        return null;
    }

    protected List<Document> nextPage() throws InterruptedException {
        String url = nextPage != null ? nextPage : run.getHarvester().getSource().getFormattedUrl();

        Model model;
        try {
            Dataset dataset = RDFDataMgr.loadDataset(url);
            model = dataset.getDefaultModel();
            if (model.isEmpty()) {
                Iterator<String> names = dataset.listNames();
                if (names.hasNext()) {
                    model = dataset.getNamedModel(names.next());
                }
            }
            dataset.close();
        } catch (org.apache.jena.atlas.web.HttpException e) {
            failed("(" + e.getMessage() + ") while reading model from " + url, e);
            nextPage = null;
            return Collections.emptyList();
        } catch (RiotException e) {
            if (e.getMessage().contains("E302")) {
                throw new InterruptedException("");
            }
            failed("Model read from source " + url + ": " + e.getMessage(), e);
            nextPage = null;
            return Collections.emptyList();
        }

        log.info("model read from {}", url);

        List<Document> page = parseResult(model);

        // handling hydra paged collection
        Hydra paging = Hydra.findPaging(model);
        nextPage = paging != null ? paging.next() : null;
        int totalItems = paging != null ? paging.total() : read.get();

        run.setNumberToProcess(totalItems);

        model.close();

        return page;
    }

    private List<Document> parseResult(Model model) {
        List<Document> datasets = new ArrayList<>();

        ResIterator it = model.listResourcesWithProperty(RDF.type, DCAT.Dataset);
        while (it.hasNext()) {
            Resource resource = it.nextResource();
            read.incrementAndGet();

            String id = JenaUtils.extractIdentifier(resource);
            if (id != null) {
                identifiers.add(id);
            }

            Model d = JenaUtils.extractResource(resource);
            if (d == null) {
                run.incrementRejected();
                run.addLogError("Could not exract dataset resource from model (" + id + ")", ServiceStage.import_stage, LogEnums.Category.dataset, null);
                continue;
            }

            StmtIterator itType = resource.listProperties(RDF.type);
            itType.forEachRemaining(statement -> {
                if (!statement.getObject().equals(DCAT.Dataset)) {
                    d.remove(statement);
                }
            });

            ResIterator itDist = d.listResourcesWithProperty(RDF.type, DCAT.Distribution);
            while (itDist.hasNext()) {
                Resource dist = itDist.nextResource();
                dist.removeAll(DCTerms.source);
            }

            ByteArrayOutputStream output = new ByteArrayOutputStream();
            try {
                RDFDataMgr.write(output, d, RDFFormat.RDFXML_ABBREV);
                SAXBuilder builder = new SAXBuilder();
                Document doc = builder.build(IOUtils.toInputStream(output.toString("UTF-8"), Charset.forName("UTF-8")));
                datasets.add(new Document(doc.getRootElement().getChild("Dataset", Namespace.getNamespace("http://www.w3.org/ns/dcat#")).detach()));
            } catch (JDOMException | IOException e) {
                log.error("building document from model", e);
                run.incrementRejected();
                run.addLogError(e.getMessage(), ServiceStage.import_stage, LogEnums.Category.dataset, output.toString());
            } catch (Exception e) {
                log.error(output.toString());
                log.error("write model", e);
                run.incrementRejected();
                run.addLogError(e.getMessage(), ServiceStage.import_stage, LogEnums.Category.dataset, null);
            }

            d.close();
        }

        return datasets;
    }

    @Override
    public Set<String> listIdentifiers() {
        return identifiers;
    }

}
