package de.fhg.fokus.odp.harvester.sections.exporter;

import com.fasterxml.jackson.databind.node.ObjectNode;
import de.fhg.fokus.odp.harvester.persistence.enums.LogEnums;
import de.fhg.fokus.odp.harvester.persistence.enums.ServiceStage;
import de.fhg.fokus.odp.harvester.sections.JobSection;
import de.fhg.fokus.odp.harvester.sections.QueuedJobSection;

import javax.enterprise.context.Dependent;

@Dependent
public class MemoryCollectingSection  extends JobSection {

    public Void call() {
        QueuedJobSection<Object> queuedSection = (QueuedJobSection<Object>) attached;
        try {
            Object dataset = queuedSection.take();
            while(!queuedSection.isFinished(dataset)) {
                run.incrementAdded();
                dataset = queuedSection.take();
            }

            queuedSection.finish();

            log.info("Finished export section for {}.", run.getHarvester().getName());
            run.addLogInfo("Finished export section. " + queuedSection.takeCounter() + " datasets processed.", ServiceStage.export_stage, LogEnums.Category.system);

        } catch (InterruptedException e) {
            log.warn("Export thread interrupted", e);
            run.addLogWarning("Export section interrupted.", ServiceStage.export_stage, LogEnums.Category.system);
        }

        return null;
    }
}
