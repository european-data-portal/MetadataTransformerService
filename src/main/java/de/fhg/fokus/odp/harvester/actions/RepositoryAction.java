package de.fhg.fokus.odp.harvester.actions;

import java.io.IOException;
import java.io.Serializable;
import java.util.*;
import java.util.stream.Collectors;

import javax.enterprise.inject.Produces;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.transaction.Transactional;

import de.fhg.fokus.odp.harvester.application.InspireAdapter;
import de.fhg.fokus.odp.harvester.application.InspireHarvesterInfo;
import org.picketlink.Identity;

import de.fhg.fokus.odp.harvester.application.AppLocalization;
import de.fhg.fokus.odp.harvester.persistence.RepositoriesManager;
import de.fhg.fokus.odp.harvester.persistence.entities.Repository;
import de.fhg.fokus.odp.harvester.persistence.enums.RepositoryLanguage;
import de.fhg.fokus.odp.harvester.persistence.enums.RepositoryType;
import de.fhg.fokus.odp.harvester.persistence.enums.Visibility;
import de.fhg.fokus.odp.harvester.rdf.CountriesAuthorityTable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Named
@ViewScoped
public class RepositoryAction implements Serializable {

	private static final long serialVersionUID = -2683217693428389360L;

	protected final Logger log = LoggerFactory.getLogger(getClass());

	@Inject
	private RepositoriesManager repositoriesManager;

	@Inject
	private Identity identity;

	@Inject
	private AppLocalization localization;

	@Inject
	private CountriesAuthorityTable mdrCountries;

	@Inject
	private InspireAdapter inspireAdapter;

	private Repository repository;

	private Integer id;

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

	private List<String> inspireTypeNames = new ArrayList<>();
	private List<Boolean> inspireTypeValues = new ArrayList<>();

	@Transactional
	public void load() {
		inspireAdapter.fetchHarvesters();
		inspireTypeNames.addAll(inspireAdapter.getHarvesters().stream().map(i -> i.getType()).collect(Collectors.toSet()));
		inspireTypeNames.forEach(name -> inspireTypeValues.add(Boolean.TRUE));

		if (id != null) {
			repository = repositoriesManager.find(id);
		} else {
			repository = new Repository();
			repository.setRepositoryType(RepositoryType.ckan);
			repository.setLanguage(RepositoryLanguage.ENGLISH);
			repository.setVisibility(Visibility.private_visibility);
			repository.setOwner(identity.getAccount().getId());
		}
	}

	@Transactional
	public void save() {
		if (id == null && repositoriesManager.nameExists(repository.getName())) {
			AppLocalization.showMessage(FacesMessage.SEVERITY_ERROR, "msgErrNameExists", null);
		} else {
			if (id == null) {
				repositoriesManager.persist(repository);
				id = repository.getId();
				AppLocalization.showMessage(FacesMessage.SEVERITY_INFO, "msgInfoRepoCreated", null);
			} else {
				repository = repositoriesManager.update(repository);
				AppLocalization.showMessage(FacesMessage.SEVERITY_INFO, "msgInfoRepoUpdated", null);
			}
		}
	}

	//Delete Repository from within Repository itself. Redirect to Dashboard
	@Transactional
	public String delete() {
		if (!repositoriesManager.isRepositoryInUse(repository)) {
			repositoriesManager.remove(repository);
			return "/user/dashboard?faces-redirect=true";
		} else {
			AppLocalization.showMessage(FacesMessage.SEVERITY_ERROR, "msgErrRepoInUse", null);
			return null;
		}
	}

	//Delete Repositories from Repositories list. Reloads Page
	@Transactional
	public void delete(Repository repository) throws IOException {
		repositoriesManager.remove(repository);
		FacesContext.getCurrentInstance().getExternalContext().redirect("/MetadataTransformerService/public/repositories.xhtml");
	}

	@Produces
	@Named
	public Repository getRepository() {
		return repository;
	}

	public Visibility[] getVisibilities() {
		return Visibility.values();
	}

	public RepositoryLanguage[] getLanguages() {
		return RepositoryLanguage.values();
	}

	public Map<String, String> getCountries() {
		return mdrCountries.getEurope(localization.getLocaleCode());
	}

	public void setSpatial(Set<String> spatial) {
		repository.setSpatial(spatial);
	}

	public Set<String> getSpatial() {
		return repository.getSpatial() != null ? new HashSet<>(repository.getSpatial()) : null;
	}

	public long countSelectedTypes(String selectedType) {
		return inspireAdapter.getHarvesters().stream().filter(i -> i.getType().equals(selectedType)).count();
	}

	public List<InspireHarvesterInfo> getInspireHarvesters() {
		return inspireAdapter.getHarvesters().stream().filter(i -> inspireTypeValues.get(inspireTypeNames.indexOf(i.getType()))).collect(Collectors.toList());
	}

	public void applyInspireHarvester(InspireHarvesterInfo info) {
		repository.setName(info.getName());
		repository.setDescription(info.getDescription());
		repository.setRepositoryType(RepositoryType.dcatap);
		repository.setUrl(info.getEndpoint());
		repository.setIncremental(info.isSelective());
	}

	public List<String> getInspireTypeNames() {
		return inspireTypeNames;
	}

	public List<Boolean> getInspireTypeValues() {
		return inspireTypeValues;
	}

	public void setInspireTypeValues(List<Boolean> values) {
		inspireTypeValues = values;
	}

}
