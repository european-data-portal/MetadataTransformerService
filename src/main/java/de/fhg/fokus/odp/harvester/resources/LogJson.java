package de.fhg.fokus.odp.harvester.resources;

import java.util.Date;

import de.fhg.fokus.odp.harvester.persistence.enums.LogEnums;
import de.fhg.fokus.odp.harvester.persistence.enums.ServiceStage;

public class LogJson {

	private Integer id;
	
	private String message;
	
	private Integer run;
	
	private ServiceStage stage;
	
	private LogEnums.Severity severity;
	
	private LogEnums.Category category;

	private Date created;

	private boolean attachment;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Integer getRun() {
		return run;
	}

	public void setRun(Integer run) {
		this.run = run;
	}

	public ServiceStage getStage() {
		return stage;
	}

	public void setStage(ServiceStage stage) {
		this.stage = stage;
	}

	public LogEnums.Severity getSeverity() {
		return severity;
	}

	public void setSeverity(LogEnums.Severity severity) {
		this.severity = severity;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public LogEnums.Category getCategory() {
		return category;
	}

	public void setCategory(LogEnums.Category category) {
		this.category = category;
	}

	public boolean isAttachment() {
		return attachment;
	}

	public void setAttachment(boolean attachment) {
		this.attachment = attachment;
	}

}
