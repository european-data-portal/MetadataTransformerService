package de.fhg.fokus.odp.harvester.application;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import java.io.Serializable;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

@Named
@SessionScoped
public class AppLocalization implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8598984051675779155L;

	private static final Logger log = LoggerFactory.getLogger(AppLocalization.class);

	public static final String MSG_BUNDLE = "de.fhg.fokus.odp.harvester.messages.Messages";

	private String localeCode = "en";

	private String timeZone = "Europe/Berlin";
	
	public String getLocaleCode() {
		return localeCode;
	}

	public void setLocaleCode(String localeCode) {
		this.localeCode = localeCode;
	}

	public void changeLanguage(String localeCode) {
		this.localeCode = localeCode;
		FacesContext.getCurrentInstance().getViewRoot().setLocale(new Locale(localeCode));		
	}
	
	@PostConstruct
	public void init() {
		Locale locale = FacesContext.getCurrentInstance().getExternalContext().getRequestLocale();
		localeCode = locale.getLanguage();
		FacesContext.getCurrentInstance().getViewRoot().setLocale(new Locale(localeCode));
	}

	/**
	 * Prints a message to the frontend.
	 * The message may consist of a localized part and a custom addition
	 * TODO implement properties messages with variables
	 *
	 * @param severity
	 * @param msgKey a key pointing to a localized message in the resource bundle.
	 *               Passing 'null' ignores this parameter
	 * @param customMsg
	 * 				a custom String to be appended to the localized message.
	 * 				Passing 'null' ignores this parameter
     */
	public static void showMessage(FacesMessage.Severity severity, String msgKey, String customMsg) {
		try {
			String msg = "";

			if (msgKey != null) {
				msg = ResourceBundle.getBundle(MSG_BUNDLE, FacesContext.getCurrentInstance().getViewRoot().getLocale()).getString(msgKey);
			}

			if (customMsg != null) {
				//Add space when concating two messages, omit otherwise
				msg += msg.isEmpty() ? customMsg : " " + customMsg;
			}

			//Only show valid message
			if (!msg.isEmpty()) {
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(severity, msg, null));
			} else {
				log.error("Cannot show empty message");
			}
		} catch (MissingResourceException e) {
			log.error("localization", e);
		}
	}

	public String getTimeZone() {
		return timeZone;
	}

	public void setTimeZone(String timeZone) {
		this.timeZone = timeZone;
	}
	
	public String getLanguage() {
		return new Locale(localeCode).getDisplayName(new Locale(localeCode));
	}
	
	public String languageName(String code) {
		return new Locale(code).getDisplayLanguage(new Locale(code));
	}
	
}
