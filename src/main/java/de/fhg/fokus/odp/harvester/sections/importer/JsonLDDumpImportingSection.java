package de.fhg.fokus.odp.harvester.sections.importer;

import de.fhg.fokus.odp.harvester.exceptions.GeneralHttpException;
import de.fhg.fokus.odp.harvester.exceptions.ProtocolException;
import de.fhg.fokus.odp.harvester.persistence.enums.LogEnums;
import de.fhg.fokus.odp.harvester.persistence.enums.ServiceStage;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import javax.enterprise.context.Dependent;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

@Dependent
public class JsonLDDumpImportingSection extends ImportingSection<ObjectNode> {

	private static final ObjectNode POISON = new ObjectMapper().createObjectNode();
	
    private final Set<String> identifiers = new HashSet<>();

    private AtomicInteger read = new AtomicInteger();

    public JsonLDDumpImportingSection() {
    	super(ObjectNode.class);
	}

	@Override
	protected ObjectNode poisonObject() {
		return POISON;
	}

	@Override
	public Void call() {
		log.info("Starting import section for {}.", run.getHarvester().getName());
		try {
			ObjectNode result = httpClient.get("");
			if (result != null) {
				List<ObjectNode> datasets = parseResult(result);
				for (ObjectNode dataset : datasets) {
					put(dataset);
				}
			}

			finish();

		} catch (GeneralHttpException e) {
			failed("(" + e.getStatusCode() + " " + e.getMessage() + ") " + e.getBody(), e);
		} catch (ProtocolException | IOException e) {
			failed(e.getMessage(), e);
		} catch (InterruptedException e) {
			log.warn("Thread interrupted", e);
			run.addLogWarning("Import section interrupted.", ServiceStage.import_stage, LogEnums.Category.system);
			return null;
		}

		log.info("Finished import section for {}.", run.getHarvester().getName());
		run.addLogInfo("Finished import section. " + read.get() + " datasets read, " + putCounter() + " datasets imported.", ServiceStage.import_stage, LogEnums.Category.system);

		return null;
	}

	private List<ObjectNode> parseResult(ObjectNode result) {
		ArrayList<ObjectNode> list = new ArrayList<>();
		JsonNode results = result.path("dataset");
		if (results.isMissingNode()) {
			results = result.path("datasets");
		}
		if (results.isArray()) {
			read.addAndGet(results.size());
			list.ensureCapacity(results.size());
			results.forEach(element -> {
				if (element.hasNonNull("id")) {
					identifiers.add(element.path("id").textValue());
				} else {
					identifiers.add(element.path("identifier").textValue());
				}
				list.add((ObjectNode) element);
			});
		}
		run.setNumberToProcess(list.size());
		return list;    	
	}

	@Override
	public Set<String> listIdentifiers() {
		return identifiers;
	}

}
