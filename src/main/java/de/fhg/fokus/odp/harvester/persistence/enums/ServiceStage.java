package de.fhg.fokus.odp.harvester.persistence.enums;

public enum ServiceStage {
	init_stage("Initialize"),
	export_stage("Export"),
	transformation_stage("Transform"),
	import_stage("Import"),
	filter_stage("Filter"),
	mapping_stage("Mapping"),
	delete_stage("Delete");

	private final String label;

	ServiceStage(String label) {
		this.label = label;
	}

	public String getLabel() {
		return this.label;
	}
}