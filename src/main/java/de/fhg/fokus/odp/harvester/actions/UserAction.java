package de.fhg.fokus.odp.harvester.actions;

import java.io.Serializable;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.inject.Inject;
import javax.inject.Named;
import javax.transaction.Transactional;

import de.fhg.fokus.odp.harvester.application.AppLocalization;
import de.fhg.fokus.odp.harvester.persistence.HarvestersManager;
import de.fhg.fokus.odp.harvester.persistence.RepositoriesManager;
import org.picketlink.Identity;
import org.picketlink.Identity.AuthenticationResult;
import org.picketlink.credential.DefaultLoginCredentials;
import org.picketlink.idm.IdentityManager;
import org.picketlink.idm.RelationshipManager;
import org.picketlink.idm.credential.Password;
import org.picketlink.idm.model.basic.BasicModel;
import org.picketlink.idm.model.basic.Grant;
import org.picketlink.idm.model.basic.User;

import de.fhg.fokus.odp.harvester.persistence.ActivitiesManager;

@Named
@RequestScoped
public class UserAction implements Serializable {

	private static final long serialVersionUID = -1533010002030134172L;
	
	private String password;
	private String confirmedPassword;

	private User user;
	
	@Inject
	private ActivitiesManager activitiesManager;

	@Inject
	private IdentityManager identityManager;

	@Inject
	private RelationshipManager relationshipManager;

	@Inject
	private RepositoriesManager repositoriesManager;

	@Inject
	private HarvestersManager harvestersManager;

	@Inject
	private Identity identity;
	
	@Inject
	private DefaultLoginCredentials credentials;
	
	@PostConstruct
	public void init() {
		if (identity.isLoggedIn()) {
			user = (User) identity.getAccount();
		} else {
			user = new User();
		}
	}
	
	@Transactional
	public String register() {
		
		// check input
		// 1. username not exist
		// 2. password confirmed
		User check = BasicModel.getUser(identityManager, user.getLoginName());
		if (check != null) {
			AppLocalization.showMessage(FacesMessage.SEVERITY_ERROR, "msgErrNameExists", null);
			return "";
		}
		
		if (!password.equals(confirmedPassword)) {
			AppLocalization.showMessage(FacesMessage.SEVERITY_ERROR, "msgErrUserPwMismatch", null);
			return "";
		}
		
		identityManager.add(user);
		identityManager.updateCredential(user, new Password(password));

		Grant grant = new Grant(user, BasicModel.getRole(identityManager, "User"));
		relationshipManager.add(grant);

		activitiesManager.userCreated(user, null);
		
		credentials.setUserId(user.getLoginName());
		credentials.setPassword(password);
		identity.login();
		
		return "/user/dashboard?faces-redirect=true";
	}

	public String login() {
		AuthenticationResult result = identity.login();
		if (result == AuthenticationResult.SUCCESS) {
			return "dashboard?faces-redirect=true";			
		} else {
			AppLocalization.showMessage(FacesMessage.SEVERITY_ERROR, "msgErrUserAuthFail", null);
			return null;
		}
	}
	
	public String logout() {
		identity.logout();
		return "/public/home?faces-redirect=true";
	}

	// doesn't work, throws IdentityManagementException
	public String removeUser() {
		// don't delete account if repositories or harvesters are still attached
		if (repositoriesManager.countUserRepositories() > 0 || harvestersManager.countUserHarvesters() > 0) {
			AppLocalization.showMessage(FacesMessage.SEVERITY_ERROR, "msgErrUserDeleteFail", null);
			return null;
		} else {
			String redirect = logout();
			identityManager.remove(user);
			return redirect;
		}
	}

	/**
	 * Builds the URL to request a profile picture from gravatar
	 * https://de.gravatar.com/
	 *
	 * @return A valid gravatar URL if a hash has been generated successfully and null otherwise
	 */
	public String getGravatarRequest() {
		String request = "https://www.gravatar.com/avatar/";

		if (getTrimmedAndHashedEmail() != null) {
			request += getTrimmedAndHashedEmail() + "?d=mm";
		} else {
			request += "?d=mm&f=y";
		}

		return request;
	}

	/**
	 * Trims and lowercases the currently logged in user's eMail and calculates an MD5 hash.
	 * This is required by the gravatar service for the fetching of profile pictures.
	 *
	 * @return 	MD5 hash of trimmed and lowercased email. Hash is also converted to lowercase
	 * 			Returns null in case an exception is thrown
	 */
	private String getTrimmedAndHashedEmail() {
		if (user.getEmail() == null) {
			return null;
		}

		String hashAsString = null;
		String preparedEmail = user.getEmail().trim().toLowerCase();

		try {

			// Calculate MD5 Hash
			MessageDigest md = MessageDigest.getInstance("MD5");
			md.update(preparedEmail.getBytes());
			byte hashedEmail[] = md.digest();

			// Convert MD5 byte stream to String representation
			StringBuilder sb = new StringBuilder();
			for (byte b : hashedEmail)
				sb.append(Integer.toHexString((b & 0xFF) | 0x100).substring(1,3));

			hashAsString = sb.toString().toLowerCase();

		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}

		return hashAsString;
	}

	@Transactional
	public void save() {
		if (password != null) {
			if (password.equals(confirmedPassword)) {
				identityManager.updateCredential(user, new Password(password));
			} else {
				AppLocalization.showMessage(FacesMessage.SEVERITY_ERROR, "msgErrorPasswordChange", null);
			}
		}
		identityManager.update(user);
		// evaluate and apply new password if necessary
		AppLocalization.showMessage(FacesMessage.SEVERITY_INFO, "msgInfoUserUpdated", null);
	}

	public String getUsername() {
		return user.getLoginName();
	}

	public void setUsername(String username) {
		user.setLoginName(username);
	}

	public String getFirstName() {
		return user.getFirstName();
	}

	public void setFirstName(String firstName) {
		user.setFirstName(firstName);
	}

	public String getLastName() {
		return user.getLastName();
	}

	public void setLastName(String lastName) {
		user.setLastName(lastName);
	}

	public String getEmail() {
		return user.getEmail();
	}

	public void setEmail(String email) {
		user.setEmail(email);
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	public String getConfirmedPassword() {
		return confirmedPassword;
	}

	public void setConfirmedPassword(String password) {
		this.confirmedPassword = password;
	}

}
