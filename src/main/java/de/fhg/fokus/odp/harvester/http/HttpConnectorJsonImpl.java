package de.fhg.fokus.odp.harvester.http;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.Header;

public class HttpConnectorJsonImpl extends HttpConnector<JsonNode> {
	
	protected HttpConnectorJsonImpl(String host, Header[] headers) {
		super(host, headers);
	}

	@Override
	protected JsonNode readContent(InputStream content, Charset charset) {
		try {
			return new ObjectMapper().readValue(content, JsonNode.class);
		} catch (IOException e) {
			log.error("parsing input stream to json", e);
		}
		return null;
	}
	
}
