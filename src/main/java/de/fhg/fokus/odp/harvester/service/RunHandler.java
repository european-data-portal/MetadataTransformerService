package de.fhg.fokus.odp.harvester.service;

import de.fhg.fokus.odp.harvester.persistence.HarvestersManager;
import de.fhg.fokus.odp.harvester.persistence.RunsManager;
import de.fhg.fokus.odp.harvester.persistence.entities.Harvester;
import de.fhg.fokus.odp.harvester.persistence.entities.Log;
import de.fhg.fokus.odp.harvester.persistence.entities.Run;
import de.fhg.fokus.odp.harvester.persistence.enums.LogEnums;
import de.fhg.fokus.odp.harvester.persistence.enums.RunState;
import de.fhg.fokus.odp.harvester.persistence.enums.ServiceStage;
import org.quartz.JobExecutionContext;

import javax.enterprise.context.Dependent;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Set;

/**
 * Created by sim on 28.06.2017.
 */
@Dependent
public class RunHandler {

    private Run run;

    @Inject
    private HarvestersManager harvestersManager;

    @Inject
    private RunsManager runsManager;

    @Transactional
    public void initRun(String harvesterId, JobExecutionContext context) {
        Harvester harvester = harvestersManager.find(Integer.valueOf(harvesterId));

        run = new Run();
        run.setHarvester(harvester);
        run.setRunState(RunState.running);
        run.setStartTime(new Date());
        run.setIncremental(harvester.getSource().isIncremental());
        runsManager.persist(run);

        harvester.getRuns().add(run);
        if (!context.getJobDetail().getKey().getName().endsWith(".manually")) {
            harvester.setNextHarvestDate(context.getNextFireTime());
        }
    }

    @Transactional
    public Run getRun() {
        return find();
    }

    @Transactional
    public Harvester getHarvester() {
        return find().getHarvester();
    }

    @Transactional
    public Run getLatestRun() {
        Optional<Run> success = find().getHarvester().getRuns().stream().filter(r -> r.getRunState() == RunState.finished).findFirst();
        return success.isPresent() ? success.get() : null;
    }

    @Transactional
    public void setNumberToProcess(int number) {
        find().setNumberToProcess(number);
    }

    @Transactional
    public synchronized void incrementAdded() {
        find().setNumberAdded(run.getNumberAdded() + 1);
    }

    @Transactional
    public synchronized void incrementUpdated() {
        find().setNumberUpdated(run.getNumberUpdated() + 1);
    }

    @Transactional
    public synchronized void incrementSkipped() {
        find().setNumberSkipped(run.getNumberSkipped() + 1);
    }

    @Transactional
    public synchronized void incrementRejected() {
        find().setNumberRejected(run.getNumberRejected() + 1);
    }

    @Transactional
    public synchronized void incrementDeleted() {
        find().setNumberDeleted(run.getNumberDeleted() + 1);
    }

    @Transactional
    public void addLogInfo(String message, ServiceStage stage, LogEnums.Category category) {
        addLog(message, LogEnums.Severity.info, stage, category, null);
    }

    @Transactional
    public void addLogWarning(String message, ServiceStage stage, LogEnums.Category category) {
        addLog(message, LogEnums.Severity.warning, stage, category, null);
    }

    @Transactional
    public void addLogError(String message, ServiceStage stage, LogEnums.Category category, String attachment) {
        addLog(message, LogEnums.Severity.error, stage, category, attachment);
    }

    private synchronized void addLog(String message, LogEnums.Severity severity, ServiceStage stage, LogEnums.Category category, String attachment) {
        find();
        Log log = new Log();
        log.setSeverity(severity);
        log.setRun(run);
        log.setServiceStage(stage);
        log.setMessage(message);
        if (category == null) {
            log.setCategory(log.parseMsgForCategory(message));
        } else {
            log.setCategory(category);
        }

        if (attachment != null)
            log.setAttachment(attachment);

        run.getLogs().add(log);
    }

    @Transactional
    public void setState(RunState state) {
        getRun().setRunState(state);
        if (state != RunState.running) {
            getRun().setEndTime(new Date());
        }
    }

    private Run find() {
        run = runsManager.find(run.getId());
        return run;
    }

}
