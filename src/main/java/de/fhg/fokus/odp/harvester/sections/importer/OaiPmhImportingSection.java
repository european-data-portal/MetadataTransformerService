package de.fhg.fokus.odp.harvester.sections.importer;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalUnit;
import java.util.*;

import javax.enterprise.context.Dependent;

import de.fhg.fokus.odp.harvester.exceptions.ProtocolException;
import de.fhg.fokus.odp.harvester.http.oaipmh.OAIPMHError;
import de.fhg.fokus.odp.harvester.http.oaipmh.OAIPMHResponse;
import de.fhg.fokus.odp.harvester.http.oaipmh.OAIPMHResult;
import de.fhg.fokus.odp.harvester.service.RunHandler;
import org.jdom2.Document;

import de.fhg.fokus.odp.harvester.exceptions.GeneralHttpException;
import de.fhg.fokus.odp.harvester.persistence.entities.Run;
import de.fhg.fokus.odp.harvester.persistence.enums.LogEnums;
import de.fhg.fokus.odp.harvester.persistence.enums.ServiceStage;
import de.fhg.fokus.odp.harvester.sections.JobSection;

@Dependent
public class OaiPmhImportingSection extends ImportingSection<Document> {

	private static final Document POISON = new Document();
	
	private String resumptionToken;

	private String filter;

	private List<String> metadataFormats = new ArrayList<>();

	public OaiPmhImportingSection() {
		super(Document.class);
	}

	@Override
	public Document poisonObject() {
		return POISON;
	}

	@Override
	public boolean init(RunHandler run, JobSection section, int capacity) {
		if (!super.init(run, section, capacity)) {
			return false;
		}

		try {
			Document resp = httpClient.get("?verb=ListMetadataFormats");
			OAIPMHResponse response = new OAIPMHResponse(resp);
			if (response.isSuccess()) {
				OAIPMHResult result = response.getResult();
				metadataFormats.addAll(result.getFormats());
			} else {
				OAIPMHError error = response.getError();
				run.addLogWarning("Detecting supported metadata formats: " + error.getMessage(), ServiceStage.init_stage, LogEnums.Category.dataset);
			}
		} catch(GeneralHttpException e) {
			run.addLogWarning("(" + e.getStatusCode() + " " + e.getMessage() +") " + e.getBody(), ServiceStage.init_stage, LogEnums.Category.connection);
		} catch (ProtocolException | IOException e) {
			run.addLogWarning(e.getMessage(), ServiceStage.init_stage, LogEnums.Category.connection);
		}

		if (metadataFormats.isEmpty()) {
			metadataFormats.add("dcat_ap");
		}

		if (run.getHarvester().getSource().isIncremental()) {
			Run latest = run.getLatestRun();
			if (latest != null) {
				LocalDate date = latest.getStartTime().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
				date = date.minusDays(1);
				DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
				filter = "from=" + date.format(formatter);
			}
		}

		return true;
	}
	
	@Override
	public Void call() {
		log.info("Starting import section for {}.", run.getHarvester().getName());
		try {
			do {
				List<Document> page = nextPage();
				for (Document dataset : page) {
					put(dataset);
				}
				log.debug("{}: {} of {} datasets fetched from remote.", run.getHarvester().getName(), putCounter(), run.getRun().getNumberToProcess());
			} while (resumptionToken != null && !resumptionToken.trim().isEmpty());

			finish();

		} catch (GeneralHttpException e) {
			failed("(" + e.getStatusCode() + " " + e.getMessage() + ") " + e.getBody(), e);
		} catch (ProtocolException | IOException e) {
			failed(e.getMessage(), e);
		} catch (InterruptedException e) {
			log.warn("Import thread interrupted", e);
			run.addLogWarning("Import section interrupted.", ServiceStage.import_stage, LogEnums.Category.system);
			return null;
		}

		log.info("Finished import section for {}.", run.getHarvester().getName());
		run.addLogInfo("Finished import section. " + putCounter() + " datasets imported.", ServiceStage.import_stage, LogEnums.Category.system);

		return null;
	}

    private List<Document> nextPage() throws IOException, ProtocolException {
    	String request = buildRequest();
    	Document result = httpClient.get(request);
		resumptionToken = null;
		return result != null ? parseResult(result) : Collections.emptyList();
	}

	private String buildRequest() {
		String url = run.getHarvester().getSource().getFormattedUrl();
		String query = url.contains("metadataPrefix") ? "&verb=ListRecords" : "?verb=ListRecords&metadataPrefix=dcat_ap";

		if (run.getHarvester().getSource().isIncremental()) {
			query += "&" + filter;
		}

		if (resumptionToken != null) {
			query += "&resumptionToken=" + resumptionToken;
		}

		return query;
	}

    private List<Document> parseResult(Document document) {

		OAIPMHResponse response = new OAIPMHResponse(document);
		if (response.isError()) {
			OAIPMHError error = response.getError();
			String code = error.getType();
			String msg = error.getMessage();
			run.addLogError(msg + " (" + code + ")", ServiceStage.import_stage, LogEnums.Category.connection, null);
			return Collections.emptyList();
		} else {
			OAIPMHResult result = response.getResult();
			String volume = result.completeSize();
			if (volume != null) {
				int size = Integer.parseInt(volume);
				run.setNumberToProcess(size);
			}
			resumptionToken = result.token();
			return result.getRecords();
		}
    }

	public String getDatasetUrl(Document dataset) { 
		return null;
	}

	@Override
	public Set<String> listIdentifiers() throws IOException, ProtocolException {
		List<String> identifiers = new ArrayList<>();

		String url = run.getHarvester().getSource().getFormattedUrl();
		String verb = url.contains("metadataPrefix") ? "&verb=ListIdentifiers" : "?verb=ListIdentifiers&metadataPrefix=dcat_ap";
		String token = null;

		do {
			String request = verb;
			if (token != null) {
				request += "&resumptionToken=" + token;
			}

			OAIPMHResponse response = new OAIPMHResponse(httpClient.get(request));
			if (response.isError()) {
				OAIPMHError error = response.getError();
				throw error.asException();
			} else {
				OAIPMHResult result = response.getResult();
				identifiers.addAll(result.getIdentifiers());
				token = result.token();
			}

		} while (token != null && !token.isEmpty());

		Set<String> distinct = new HashSet<>(identifiers);
		if (distinct.size() != identifiers.size()) {
			log.warn("list of identifiers contains {} duplicates", identifiers.size() - distinct.size());
		}

		return distinct;
	}

}
