package de.fhg.fokus.odp.harvester.sections.transformer;

import javax.enterprise.context.Dependent;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import de.fhg.fokus.odp.harvester.exceptions.TransformationException;
import de.fhg.fokus.odp.harvester.persistence.enums.LogEnums;
import de.fhg.fokus.odp.harvester.persistence.enums.ServiceStage;
import de.fhg.fokus.odp.harvester.sections.JobSection;
import de.fhg.fokus.odp.harvester.sections.importer.ImportingSection;
import de.fhg.fokus.odp.harvester.service.RunHandler;
import de.fhg.fokus.odp.harvester.transformer.scripttransformer.JavaScriptTransformer;

@Dependent
public class JsonToJsonTransformingSection extends TransformingSection<ObjectNode, ObjectNode> {

	private static final ObjectNode POISON = new ObjectMapper().createObjectNode();
	
	@Override
	public ObjectNode poisonObject() {
		return POISON;
	}
	
	@Override
	public ObjectNode transform(ObjectNode dataset) {
    	JavaScriptTransformer transformer = new JavaScriptTransformer(script);

        try {
        	ObjectNode transformed = transformer.transform(dataset);
        	
			String originalSource = ((ImportingSection<ObjectNode>) attached).getDatasetUrl(dataset);
	        if (originalSource != null) {
	            transformed.put("original_source", originalSource);
	        }				

	        return transformed;
		} catch (TransformationException e) {
			try {
				String d = new ObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(dataset);
				log.error("transforming dataset: {}", d, e);
				run.addLogError(e.getMessage(), ServiceStage.transformation_stage, LogEnums.Category.transformation, d);
			} catch (JsonProcessingException e1) {
				log.error("pretty print json", e1);
			}
			run.incrementRejected();
		}

        return null;
	}

}
