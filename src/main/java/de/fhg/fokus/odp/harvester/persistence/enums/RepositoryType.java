package de.fhg.fokus.odp.harvester.persistence.enums;

public enum RepositoryType {
    ckan("CKAN", true),
    dkan("DKAN", true),
    dcatap("DCAT-AP", false),
    sparql("SPARQL", false),
    udata("uData", false),
    rdf_ckan("RDF CKAN", true),
    rdf("RDF", false),
    jsonld_dump("JSON-LD Dump", false),
    rss("RSS", false),
    opendatasoft("OpenDataSoft", true),
    socrata("Socrata", false),
    dcip("DCIP", false),
    local_persist("Local DB (dev)", false),
    memory("Memory (dev)", false);

    private final String label;
    private final boolean requiresApiKey;
    
    RepositoryType(String label, boolean requiresApiKey) {
        this.label = label;
        this.requiresApiKey = requiresApiKey;
    }

    public String getLabel() {
        return this.label;
    }

    public boolean isRequiresApiKey() {
        return this.requiresApiKey;
    }
}