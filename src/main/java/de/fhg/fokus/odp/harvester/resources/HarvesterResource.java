package de.fhg.fokus.odp.harvester.resources;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

@Path("/harvester")
public interface HarvesterResource {

	@GET
	@Path("/")
	@Produces("application/json")
	ResponseJson listHarvesters(@QueryParam("rows") Integer rows, @QueryParam("start") Integer start, @QueryParam("search") String search);

	@GET
	@Path("/{id}")
	@Produces("application/json")
	ResponseJson getHarvester(@PathParam("id") String id);

	@GET
	@Path("/{id}/run")
	@Produces("application/json")
	ResponseJson listHarvesterRuns(@PathParam("id") String id, @QueryParam("rows") Integer rows, @QueryParam("start") Integer start);

	@GET
	@Path("/{id}/run/{runid}")
	@Produces("application/json")
	ResponseJson getRun(@PathParam("id") String id, @PathParam("runid") String runid);

	@GET
	@Path("/{id}/run/{runid}/log")
	@Produces("application/json")
	ResponseJson listLogs(@PathParam("id") String id, @PathParam("runid") String runid, @QueryParam("severity") String severity, @QueryParam("rows") Integer rows, @QueryParam("start") Integer start);

	@GET
	@Path("/{id}/run/{runid}/log/{logid}/attachment")
	@Produces("application/json")
	String getAttachment(@PathParam("id") String id, @PathParam("runid") String runid, @PathParam("logid") String logid);

}
