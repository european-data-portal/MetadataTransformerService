package de.fhg.fokus.odp.harvester.http;

import de.fhg.fokus.odp.harvester.exceptions.GeneralHttpException;
import de.fhg.fokus.odp.harvester.exceptions.ProtocolException;

public abstract class HttpError<T> {
    protected final T error;

    protected HttpError(T error) {
        this.error = error;
    }

    protected abstract String getType();

    protected abstract String getMessage();

    public ProtocolException asException() {
        return new ProtocolException("(" + getType() + ") " + getMessage());
    }
    
}
