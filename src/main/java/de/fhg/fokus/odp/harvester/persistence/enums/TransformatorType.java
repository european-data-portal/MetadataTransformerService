package de.fhg.fokus.odp.harvester.persistence.enums;

import de.fhg.fokus.odp.harvester.persistence.entities.Harvester;

public enum TransformatorType {
		JavaScript, XSLT, Manual;


	public static TransformatorType getTransformatorTypeForHarvester(Harvester harvester) {
		RepositoryType sourceRepositoryType = harvester.getSource().getRepositoryType();
		RepositoryType targetRepositoryType = harvester.getTarget().getRepositoryType();

		if (sourceRepositoryType.equals(RepositoryType.ckan)) {
			if (targetRepositoryType.equals(RepositoryType.ckan)) {
				// CKAN-to-CKAN
				return JavaScript;
			} else {
				// CKAN-to-?
				return null;
			}
		} else if (sourceRepositoryType.equals(RepositoryType.dcatap)) {
			if (targetRepositoryType.equals(RepositoryType.ckan)) {
				// DCAT-to-CKAN
				return XSLT;
			} else {
				// DCAT-to-?
				return null;
			}
		} else {
			return null;
		}
	}
}

