package de.fhg.fokus.odp.harvester.persistence.enums;

public enum RunState {
	scheduled("Scheduled"), not_scheduled("NotScheduled"), running("Running"), error("Error"), interrupted("Interrupted"), finished("Finished");

	private final String label;

	RunState(String label) {
		this.label = label;
	}

	public String getLabel() {
		return this.label;
	}
}