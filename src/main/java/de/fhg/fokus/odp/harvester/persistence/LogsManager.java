package de.fhg.fokus.odp.harvester.persistence;

import de.fhg.fokus.odp.harvester.persistence.entities.Log;
import de.fhg.fokus.odp.harvester.persistence.entities.Run;
import de.fhg.fokus.odp.harvester.persistence.enums.LogEnums;

import javax.enterprise.context.ApplicationScoped;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.*;
import java.util.stream.Collectors;

@ApplicationScoped
public class LogsManager {

    @PersistenceContext
    private EntityManager em;

    public Log find(Integer id) {
        return em.find(Log.class, id);
    }

    public List<Log> getFilteredLogs(Run run, List<LogEnums.Severity> severities, List<LogEnums.Category> categories, String searchTerm, Integer limit) {

        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Log> cq = cb.createQuery(Log.class);
        Root<Log> log = cq.from(Log.class);

        //Select logs with specified severities only
        List<Predicate> severityPredicates = new ArrayList<>();
        severities.forEach(severity -> severityPredicates.add(cb.equal(log.get("severity"), severity)));
        Predicate severityPredicate = cb.or(severityPredicates.toArray(new Predicate[severityPredicates.size()]));

        //Select logs with specified categories only
        List<Predicate> categoryPredicates = new ArrayList<>();
        categories.forEach(category -> categoryPredicates.add(cb.equal(log.get("category"), category)));
        Predicate categoryPredicate = cb.or(categoryPredicates.toArray(new Predicate[categoryPredicates.size()]));

        //Join selections and filter by run
        cq.select(log).where(severityPredicate, categoryPredicate, cb.equal(log.get("run"), run));

        ///Order by timestamp
        cq.orderBy(cb.desc(log.get("created")));

        //Paging does not work since the results is used in a JSF table (not datatable)
        List<Log> tmpResult = em.createQuery(cq).setMaxResults(limit).getResultList();

        //Searchterm is applied here because jpa does not support LIKE operator on CLOB
        return searchTerm.isEmpty() ? tmpResult : tmpResult.stream().filter(l ->
                l.getMessage().replaceAll("[^\\p{L}\\p{Z}]","").toLowerCase()   //Remove chars breaking regex
                .matches(".*" + searchTerm.toLowerCase() + ".*"))               //Match searchterm
                .collect(Collectors.toList());
    }

    private <T> TypedQuery<T> paging(TypedQuery<T> query, Integer rows, Integer start) {
        if (rows != null) {
            query.setMaxResults(rows);
        }
        if (start != null) {
            query.setFirstResult(start);
        }
        return query;
    }
}
