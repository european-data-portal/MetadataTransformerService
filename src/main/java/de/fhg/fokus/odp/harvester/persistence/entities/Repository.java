package de.fhg.fokus.odp.harvester.persistence.entities;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;

import de.fhg.fokus.odp.harvester.persistence.enums.RepositoryLanguage;
import de.fhg.fokus.odp.harvester.persistence.enums.RepositoryType;
import de.fhg.fokus.odp.harvester.persistence.enums.Visibility;

@Entity
@NamedQueries({
	@NamedQuery(name="repositories", query="select r from Repository r order by r.name"),
	@NamedQuery(name="repository_by_name", query="select r from Repository r where r.name = :name"),
	@NamedQuery(name="repositories_of_visibility", query="select r from Repository r where r.visibility = :visibility order by r.name"),
	@NamedQuery(name="repositories_of_visibility_or_from_owner", query="select r from Repository r where r.visibility = :visibility or r.owner = :owner order by r.name"),
	@NamedQuery(name="repositories_from_owner", query="select r from Repository r where r.owner = :owner order by r.name"),
	@NamedQuery(name="repositories_search", query="select r from Repository r where r.visibility = :visibility and lower(r.name) like :searchterm order by r.name"),
	@NamedQuery(name="repositories_loggedin_search", query="select r from Repository r where (r.owner = :owner or r.visibility = :visibility) and lower(r.name) like :searchterm order by r.name")
})
public class Repository {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;

	private String name;

	private String description;

	private String url;

	@Enumerated(EnumType.STRING)
	private Visibility visibility = Visibility.private_visibility;

	@Enumerated(EnumType.STRING)
	private RepositoryType repositoryType;

	@Enumerated(EnumType.STRING)
	private RepositoryLanguage language;
	
	@OneToMany(mappedBy = "source", fetch = FetchType.EAGER)
	private List<Harvester> harvesterSource = new ArrayList<>();

	@OneToMany(mappedBy = "target", fetch = FetchType.EAGER)
	private List<Harvester> harvesterTarget = new ArrayList<>();

	@ElementCollection(fetch = FetchType.EAGER)
	private Set<String> spatial;
	
	private String owner;

	private String homepage;

	private String publisher;
	
	private String publisherEmail;
	
	private boolean incremental;
	
	@OneToMany(mappedBy = "repository", fetch = FetchType.LAZY, orphanRemoval = true)
	@OrderBy("created")
	private List<Activity> activities;

	public List<Activity> getActivities() {
		return this.activities;
	}

	public void setActivities(List<Activity> acts) {
		this.activities = acts;
	}
	
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getUrl() {
		return this.url;
	}

	//Trims url and removes trailing slash if present. Required for API string building
	public String getFormattedUrl() {
		return this.url.trim().replaceAll("/$", "");
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Visibility getVisibility() {
		return this.visibility;
	}

	public void setVisibility(Visibility visibility) {
		this.visibility = visibility;
	}

	public List<Harvester> getHarvesterTarget() {
		return this.harvesterTarget;
	}

	public void setHarvesterTarget(List<Harvester> harvesterTarget) {
		this.harvesterTarget = harvesterTarget;
	}

	public List<Harvester> getHarvesterSource() {
		return this.harvesterSource;
	}

	public void setHarvesterSource(List<Harvester> harvesterSource) {
		this.harvesterSource = harvesterSource;
	}

	public RepositoryType getRepositoryType() {
		return repositoryType;
	}

	public void setRepositoryType(RepositoryType repositoryType) {
		this.repositoryType = repositoryType;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public RepositoryLanguage getLanguage() {
		return language;
	}

	public void setLanguage(RepositoryLanguage language) {
		this.language = language;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public String getHomepage() {
		return homepage;
	}

	public void setHomepage(String homepage) {
		this.homepage = homepage;
	}

	public String getPublisher() {
		return publisher;
	}

	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}

	public String getPublisherEmail() {
		return publisherEmail;
	}

	public void setPublisherEmail(String publisherEmail) {
		this.publisherEmail = publisherEmail;
	}

	public boolean isIncremental() {
		return incremental;
	}

	public void setIncremental(boolean incremental) {
		this.incremental = incremental;
	}

	public Set<String> getSpatial() {
		return spatial;
	}

	public void setSpatial(Set<String> spatial) {
		this.spatial = spatial;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((url == null) ? 0 : url.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Repository other = (Repository) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (url == null) {
			if (other.url != null)
				return false;
		} else if (!url.equals(other.url))
			return false;
		return true;
	}
}
