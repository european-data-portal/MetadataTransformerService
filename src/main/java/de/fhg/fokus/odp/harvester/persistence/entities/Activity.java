package de.fhg.fokus.odp.harvester.persistence.entities;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import de.fhg.fokus.odp.harvester.persistence.enums.ActivityType;

@Entity
@NamedQueries({
		@NamedQuery(name = "activities_of_type", query = "select a from Activity a where a.activityType = :type"),
		@NamedQuery(name = "activities_from_user", query = "select a from Activity a where a.owner = :owner"),
		@NamedQuery(name = "activities_from_harvester", query = "select a from Activity a where a.owner = :owner"),
		@NamedQuery(name = "activities_from_repository", query = "select a from Activity a where a.repository = :respository") })
public class Activity {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;

	private String message;

	private String entityName;

	private Integer entityId;

	private String owner;

	@ManyToOne(fetch = FetchType.EAGER)
	private Harvester harvester;

	@ManyToOne(fetch = FetchType.EAGER)
	private Repository repository;

	@Enumerated(EnumType.STRING)
	private ActivityType activityType;

	@Temporal(TemporalType.TIMESTAMP)
	private Date created;

	@PrePersist
	public void onCreate() {
		created = new Date();
	}

	public void setCreated(Date date) {
		this.created = date;
	}

	public Date getCreated() {
		return this.created;
	}

	public String getEntityName() {
		return this.entityName;
	}

	public void setEntityName(String name) {
		this.entityName = name;
	}

	public Integer getEntityId() {
		return this.entityId;
	}

	public void setEntityId(Integer id) {
		this.entityId = id;
	}

	public String getMessage() {
		return this.message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public ActivityType getActivityType() {
		return this.activityType;
	}

	public void setActivityType(ActivityType type) {
		this.activityType = type;
	}

	public Repository getRepository() {
		return repository;
	}

	public void setRepository(Repository repository) {
		this.repository = repository;
	}

	public Harvester getHarvester() {
		return harvester;
	}

	public void setHarvester(Harvester harvester) {
		this.harvester = harvester;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

}
