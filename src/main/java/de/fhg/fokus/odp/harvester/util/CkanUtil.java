package de.fhg.fokus.odp.harvester.util;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.apache.http.Header;
import org.apache.http.message.BasicHeader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonParser.Feature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import de.fhg.fokus.odp.harvester.exceptions.ValidationException;

/**
 * Utility methods for CKAN
 * Created by jpi on 23.06.2015.
 */
public class CkanUtil {

	private static final Logger log = LoggerFactory.getLogger(CkanUtil.class);
	
    private CkanUtil() {

    }

    public static String getPackageId(ObjectNode pack) {
        return pack.hasNonNull("id") ? pack.get("id").textValue() : "";
    }

    public static String getPackageName(ObjectNode pack) {
        return pack.hasNonNull("name") ? pack.get("name").textValue() : null;
    }

    public static String getPackageUrl(String url, String id) {
        return url + "/api/2/rest/dataset/" + id;
    }

    /**
     * Checks if the spatial field of a dataset contains valid values.
     * @param dataset the dataset
     */
    public static void checkIfSpatialIsCorrect(ObjectNode dataset) throws ValidationException {
    	JsonNode extras = dataset.path("extras");
        if (extras.isArray()) {
            for (JsonNode extra : extras) {
            	if (extra.path("key").textValue().equals("spatial")) {
            		JsonNode value = extra.path("value");

		        	ObjectMapper mapper = new ObjectMapper();
		        	mapper.configure(Feature.ALLOW_UNQUOTED_CONTROL_CHARS, true);
		        	mapper.configure(Feature.ALLOW_BACKSLASH_ESCAPING_ANY_CHARACTER, true);
		        	mapper.configure(Feature.ALLOW_UNQUOTED_FIELD_NAMES, true);
		        	
					try {
						JsonNode elem = mapper.readTree(value.textValue());
						JsonNode coordinates = elem.path("coordinates");
	                	if (coordinates.isArray()) {
	                        if (!checkCoordinatesArray((ArrayNode)coordinates)) {
	                        	throw new ValidationException("Coordinates in spatial are invalid.");
	                        }
	                	} else {
	                    	throw new ValidationException("Spatial coordinates are not a json array.");                    		
	                	}
					} catch (IOException e) {
                    	throw new ValidationException("Spatial field is not a json object.");                    		
					}
            	}
            }
        }
    }

    /**
     * Recursively checks if the coordinates in a coordinate array are correct
     * @param coordinateArray array or coordinates
     * @return true if correct, false otherwise
     */
    private static boolean checkCoordinatesArray(ArrayNode coordinateArray) {
        if (coordinateArray.path(0).isArray()) {
            boolean result = true;
            for (JsonNode coordinate : coordinateArray) {
            	if (coordinate.isArray()) {
                	result = result && checkCoordinatesArray((ArrayNode) coordinate);            		
            	} else {
            		result = false;
            		break;
            	}
            }
            return result;
        } else {
            double longitude = coordinateArray.path(0).asDouble();
            double latitude = coordinateArray.path(1).asDouble();
            return (-180 <= longitude && longitude <= 180 && -90 <= latitude && latitude <= 90);
        }
    }

    public static Header[] addApiKeyHeader(String apiKey, Header[] headers) {
        ArrayList<Header> list = new ArrayList<>();
        if (headers != null) {
            list.addAll(Arrays.asList(headers));
        }
        if (apiKey != null && !apiKey.isEmpty()) {
            list.add(new BasicHeader("Authorization", apiKey));
        }
        return list.toArray(new Header[]{});
    }

    public static void addHashValue(ObjectNode dataset) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            MessageDigest digest = MessageDigest.getInstance("MD5");
            String d = mapper.writeValueAsString(dataset);
            byte[] hash = digest.digest(d.getBytes("UTF-8"));

            ArrayNode extras = dataset.withArray("extras");
            ObjectNode modhash = mapper.createObjectNode();
            modhash.put("key", "modhash");
            modhash.put("value", hash);
            extras.add(modhash);

        } catch (NoSuchAlgorithmException | JsonProcessingException | UnsupportedEncodingException e) {
            log.error("calculating and adding modification hash value", e);
        }
    }

    public static boolean hashEqual(ObjectNode dataset, ObjectNode existingDataset) {
        try {
            byte[] hash1 = null;
            ArrayNode originalExtras = dataset.withArray("extras");
            for (JsonNode extra : originalExtras) {
                if (extra.path("key").textValue().equals("modhash")) {
                    hash1 = extra.path("value").binaryValue();
                }
            }
            byte[] hash2 = null;
            ArrayNode existingExtras = existingDataset.withArray("extras");
            for (JsonNode extra : existingExtras) {
                if (extra.path("key").textValue().equals("modhash")) {
                    hash2 = extra.path("value").binaryValue();
                }
            }
            return hash1 != null && hash2 != null && MessageDigest.isEqual(hash1, hash2);
        } catch (IOException e) {
            log.error("reading and comparing modification hash values", e);
        }
        return false;
    }
}
