package de.fhg.fokus.odp.harvester.exceptions.oaipmh;

import de.fhg.fokus.odp.harvester.exceptions.ProtocolException;

public class IdDoesNotExistException extends ProtocolException {
    
    /**
	 * 
	 */
	private static final long serialVersionUID = 5298448030600933653L;
	
	private String message = " 	The metadata format identified by the value given for the metadataPrefix argument is not supported by the item or by the repository.";

    public IdDoesNotExistException() {
        super();    
    }
    
      public IdDoesNotExistException(String message) {
        super(message);
        this.message = message;
    }
    
    public IdDoesNotExistException(Throwable throwable) {
        super(throwable);
        message = throwable.getMessage();
    }

  
    public IdDoesNotExistException(String message, Throwable throwable) {
        super(message, throwable);
        this.message = message;
    }
    
  
    public String getMessage() {
        return message;
    }
    
   
    public void setMessage(String message) {
        this.message = message;
    }

	
}
