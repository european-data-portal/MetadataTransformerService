package de.fhg.fokus.odp.harvester.exceptions;

import org.apache.http.client.HttpResponseException;

public class GeneralHttpException extends HttpResponseException {
    
	private String body;

    public GeneralHttpException(int statusCode, String phrase, String body) {
        super(statusCode, phrase);
        this.body = body;
    }

    public String getBody() {
        return body;
    }
    
}
