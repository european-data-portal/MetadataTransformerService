package de.fhg.fokus.odp.harvester.util.xml;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.filter.Filters;
import org.jdom2.xpath.XPathExpression;
import org.jdom2.xpath.XPathFactory;

/**
 * Created by jpi on 10.09.2015.
 */
public class DadosGovPtRssXmlDocumentHandler {

    private static final String XML_PATH_DADOSGOVPT_LIST_DATASETS = "//*[namespace-uri() = 'http://www.w3.org/2005/Atom' and local-name() = 'entry']";
    private static final String XML_PATH_DADOSGOVPT_IMPORT_DATASET = "//*[namespace-uri() = 'http://www.w3.org/2005/Atom' and local-name() = 'entry' and *[namespace-uri() = 'http://www.w3.org/2005/Atom' and local-name() = 'content']/*[namespace-uri() = 'http://schemas.microsoft.com/ado/2007/08/dataservices/metadata' and local-name() = 'properties']/*[namespace-uri() = 'http://schemas.microsoft.com/ado/2007/08/dataservices' and local-name() = 'PartitionKey'] = $id]";

    private static final XPathExpression<Element> records = XPathFactory.instance().compile(XML_PATH_DADOSGOVPT_LIST_DATASETS, Filters.element());

    private Document document;

    private DadosGovPtRssXmlDocumentHandler(Document document) {
        this.document = document;
    }

    public static DadosGovPtRssXmlDocumentHandler createHandler(Document document) {
        return new DadosGovPtRssXmlDocumentHandler(document);
    }

    public Document getRecord(String id) {
        XPathFactory xpFactory = XPathFactory.instance();
        XPathExpression<Element> record = xpFactory.compile(XML_PATH_DADOSGOVPT_IMPORT_DATASET.replace("$id", id), Filters.element());
        
        List<Element> elements = record.evaluate(document);
        return elements.isEmpty() ? null : new Document(elements.get(0).detach());
    }
    
    public List<Document> getRecords() {
        List<Element> elements = records.evaluate(document);
        List<Document> records = new ArrayList<>(elements.size());

        records.addAll(elements.stream().map(next -> new Document(next.detach())).collect(Collectors.toList()));
        return records;
    }

}
