package de.fhg.fokus.odp.harvester.http.opendatasoft;

import com.fasterxml.jackson.databind.JsonNode;

import de.fhg.fokus.odp.harvester.http.HttpResult;

public class OpenDataSoftResult extends HttpResult<JsonNode> {

    public OpenDataSoftResult(JsonNode result) {
        super(result);
    }

    @Override
    public JsonNode getContent() {
        return result;
    }
    
}
