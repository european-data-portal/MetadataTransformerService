package de.fhg.fokus.odp.harvester.exceptions.oaipmh;

import de.fhg.fokus.odp.harvester.exceptions.ProtocolException;

public class NoRecordsMatchException extends ProtocolException {
    
    /**
	 * 
	 */
	private static final long serialVersionUID = 2512864808126097553L;
	
	private String message = "The combination of the values of the from, until, set and metadataPrefix arguments results in an empty list.";

    public NoRecordsMatchException() {
        super();    
    }
    
      public NoRecordsMatchException(String message) {
        super(message);
        this.message = message;
    }
    
    public NoRecordsMatchException(Throwable throwable) {
        super(throwable);
        message = throwable.getMessage();
    }

  
    public NoRecordsMatchException(String message, Throwable throwable) {
        super(message, throwable);
        this.message = message;
    }
    
  
    public String getMessage() {
        return message;
    }
    
   
    public void setMessage(String message) {
        this.message = message;
    }

	
}
