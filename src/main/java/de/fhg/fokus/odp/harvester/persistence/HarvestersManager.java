package de.fhg.fokus.odp.harvester.persistence;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import javax.transaction.Transactional;

import de.fhg.fokus.odp.harvester.persistence.enums.RunState;
import org.picketlink.Identity;

import de.fhg.fokus.odp.harvester.persistence.entities.Harvester;
import de.fhg.fokus.odp.harvester.persistence.entities.Run;
import de.fhg.fokus.odp.harvester.persistence.enums.Visibility;

@ApplicationScoped
public class HarvestersManager implements Serializable {

	@PersistenceContext
	private EntityManager em;

	@Inject
	private Identity identity;

	public Harvester find(Integer id) {
		return em.find(Harvester.class, id);
	}

	public void persist(Harvester harvester) {
		em.persist(harvester);
	}
	
	public void remove(Harvester harvester) {
		em.remove(em.contains(harvester) ? harvester : em.merge(harvester));
	}
		
	public Harvester update(Harvester harvester) {
		return em.merge(harvester);
	}
	
	public void refresh(Harvester harvester) {
		if (!em.contains(harvester)) {
			harvester = em.merge(harvester);
		}
		em.refresh(harvester);
	}

	public List<Harvester> listOwner(String ownerId) {
		return em.createNamedQuery("harvesters_from_owner", Harvester.class).setParameter("owner", ownerId).getResultList();
	}

	public List<Harvester> listPublic() {
		return em.createNamedQuery("harvesters_of_visibility", Harvester.class).setParameter("visibility", Visibility.public_visibility).getResultList();
	}

	public List<Harvester> listOwnerOrPublic(String ownerId) {
		return em.createNamedQuery("harvesters_of_visibility_or_from_owner", Harvester.class).setParameter("owner", ownerId).setParameter("visibility", Visibility.public_visibility).getResultList();
	}

	@Transactional
	public Long count() {
		return em.createQuery("select count(h) from Harvester h", Long.class).getSingleResult();
	}
	
	@Transactional
	public List<Harvester> list(Integer rows, Integer start) {
		return paging(em.createNamedQuery("harvesters", Harvester.class), rows, start).getResultList();
	}

	@Transactional
	public Long countUserHarvesters() {
		return identity.isLoggedIn() ? em.createQuery("select count(h) from Harvester h where h.owner = :owner", Long.class).setParameter("owner", identity.getAccount().getId()).getSingleResult() : 0L;
	}

	@Transactional
	public List<Harvester> listUserHarvesters(Integer rows, Integer start) {
		TypedQuery<Harvester> query = em.createNamedQuery("harvesters_from_owner", Harvester.class).setParameter("owner", identity.getAccount().getId());
		return paging(query, rows, start).getResultList();		
	}

	public Long countHarvesters(String searchterm, boolean errorsOnly) {
		CriteriaBuilder builder = em.getCriteriaBuilder();
		CriteriaQuery<Long> critquery = builder.createQuery(Long.class);
		Root<Harvester> harvester = critquery.from(Harvester.class);
		
		List<Predicate> predicates = new ArrayList<>();
		Map<String, Object> params = new HashMap<>();

		ParameterExpression<Visibility> visibilityParam = builder.parameter(Visibility.class, "visibility");
		Predicate visibilityPredicate = builder.equal(harvester.<Visibility>get("visibility"), visibilityParam);
		params.put("visibility", Visibility.public_visibility);
		
		// owner or public, or only public
		if (identity.isLoggedIn()) {
			ParameterExpression<String> ownerParam = builder.parameter(String.class, "owner");
			predicates.add(builder.or(builder.equal(harvester.<String>get("owner"), ownerParam), visibilityPredicate));
			params.put("owner", identity.getAccount().getId());
		} else {
			predicates.add(visibilityPredicate);
		}

		// name like search term
		if (!searchterm.isEmpty()) {
			ParameterExpression<String> searchParam = builder.parameter(String.class, "searchterm");
			predicates.add(builder.like(builder.lower(harvester.get("name")), searchParam));
			params.put("searchterm", "%" + searchterm.toLowerCase().trim() + "%");
		}

		// only with failed runs
		if (errorsOnly) {
			Join<Harvester, Run> harvesterRunJoin = harvester.join("runs");
			predicates.add(builder.equal(harvesterRunJoin.get("runStatus"), RunState.error));
		}
		
		critquery.where(predicates.toArray(new Predicate[predicates.size()]));
		
		TypedQuery<Long> query = em.createQuery(critquery.select(builder.count(harvester)));
		for (Entry<String, Object> param : params.entrySet()) {
			query = query.setParameter(param.getKey(), param.getValue());
		}
		
		return query.getSingleResult();
	}

	public List<Harvester> listHarvesters(Integer rows, Integer start, String searchterm, boolean errorsOnly) {
		CriteriaBuilder builder = em.getCriteriaBuilder();
		CriteriaQuery<Harvester> critquery = builder.createQuery(Harvester.class);
		Root<Harvester> harvester = critquery.from(Harvester.class);

		List<Predicate> predicates = new ArrayList<>();
		Map<String, Object> params = new HashMap<>();

		ParameterExpression<Visibility> visibilityParam = builder.parameter(Visibility.class, "visibility");
		Predicate visibilityPredicate = builder.equal(harvester.<Visibility>get("visibility"), visibilityParam);
		params.put("visibility", Visibility.public_visibility);
		
		if (identity.isLoggedIn()) {
			ParameterExpression<String> ownerParam = builder.parameter(String.class, "owner");
			predicates.add(builder.or(builder.equal(harvester.<String>get("owner"), ownerParam), visibilityPredicate));
			params.put("owner", identity.getAccount().getId());
		} else {
			predicates.add(visibilityPredicate);
		}

		if (!searchterm.isEmpty()) {
			ParameterExpression<String> searchParam = builder.parameter(String.class, "searchterm");
			predicates.add(builder.like(builder.lower(harvester.get("name")), searchParam));
			params.put("searchterm", "%" + searchterm.toLowerCase().trim() + "%");
		}

		if (errorsOnly) {
			Join<Harvester, Run> harvesterRunJoin = harvester.join("runs");
			predicates.add(builder.equal(harvesterRunJoin.get("runStatus"), RunState.error));
		}
		
		critquery.where(predicates.toArray(new Predicate[predicates.size()]));
		critquery.orderBy(builder.asc(harvester.get("name")));

		TypedQuery<Harvester> query = em.createQuery(critquery.select(harvester));
		for (Entry<String, Object> param : params.entrySet()) {
			query = query.setParameter(param.getKey(), param.getValue());
		}

		return paging(query, rows, start).getResultList();
	}
	
	@Transactional
	public List<Harvester> listRunningHarvesters() {
		if (identity.isLoggedIn()) {
			return em.createNamedQuery("harvesters_running", Harvester.class)
					.setParameter("owner", identity.getAccount().getId()).getResultList()
					.stream().distinct().collect(Collectors.toList());
		} else {
			return new ArrayList<>();
		}
	}

	@Transactional
	public List<Harvester> listRecentlyFinishedHarvesters(int limit) {
		if (identity.isLoggedIn()) {
			return em.createNamedQuery("harvesters_finished_from_owner", Harvester.class)
					 .setParameter("owner", identity.getAccount().getId()).setMaxResults(limit).getResultList();
		} else {
			return new ArrayList<>();
		}
	}

	@Transactional
	public List<Harvester> listScheduledHarvesters(int limit) {
		
		if (identity.isLoggedIn()) {
			return em.createNamedQuery("harvesters_scheduled_from_owner", Harvester.class)
					.setParameter("owner", identity.getAccount().getId()).setMaxResults(limit).getResultList();
		} else {
			return new ArrayList<>();
		}
	}

	public boolean isGitRepoInUse(String repoUrl) {
		//if url is null treat as in use to prevent further errors
		return repoUrl == null || em.createQuery("select count(h) from Harvester h where lower(h.gitRepoUrl) = :repoUrl", Long.class)
				.setParameter("repoUrl", repoUrl.toLowerCase()).getSingleResult() > 0;
	}

	public boolean nameExists(String name) {
		return em.createQuery("select count(h) from Harvester h where lower(h.name) = :name", Long.class).setParameter("name", name.toLowerCase()).getSingleResult() > 0;
	}

	private <T> TypedQuery<T> paging(TypedQuery<T> query, Integer rows, Integer start) {
		if (rows != null) {
			query.setMaxResults(rows);
		}
		if (start != null) {
			query.setFirstResult(start);
		}
		return query;
	}
	
}
