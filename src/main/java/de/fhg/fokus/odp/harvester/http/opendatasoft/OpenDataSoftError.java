package de.fhg.fokus.odp.harvester.http.opendatasoft;

import com.fasterxml.jackson.databind.JsonNode;

import de.fhg.fokus.odp.harvester.http.HttpError;

public class OpenDataSoftError extends HttpError<JsonNode> {

    public OpenDataSoftError(JsonNode error) {
        super(error);
    }

    @Override
    public String getType() {
        return error.path("code").textValue();
    } // "errorcode"??

    @Override
    public String getMessage() {
        return error.path("message").textValue();
    }
    
}
