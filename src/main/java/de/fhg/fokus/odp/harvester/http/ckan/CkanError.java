package de.fhg.fokus.odp.harvester.http.ckan;

import com.fasterxml.jackson.databind.JsonNode;

import de.fhg.fokus.odp.harvester.http.HttpError;

public class CkanError extends HttpError<JsonNode> {

	public CkanError(JsonNode error) {
		super(error);
	}

	@Override
	public String getType() {
		return error.path("__type").textValue();		
	}

	@Override
	public String getMessage() {
		return error.path("message").textValue();
	}
	
}
