package de.fhg.fokus.odp.harvester.http.ckan;

import com.fasterxml.jackson.databind.JsonNode;

import de.fhg.fokus.odp.harvester.http.HttpResult;

public class CkanResult extends HttpResult<JsonNode> {
	
	public CkanResult(JsonNode result) {
		super(result);
	}

	@Override
	public JsonNode getContent() {
		return result;
	}
	
}
