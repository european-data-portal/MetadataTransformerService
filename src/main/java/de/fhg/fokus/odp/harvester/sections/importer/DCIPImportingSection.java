package de.fhg.fokus.odp.harvester.sections.importer;

import de.fhg.fokus.odp.harvester.exceptions.GeneralHttpException;
import de.fhg.fokus.odp.harvester.exceptions.ProtocolException;
import de.fhg.fokus.odp.harvester.persistence.enums.LogEnums;
import de.fhg.fokus.odp.harvester.persistence.enums.ServiceStage;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import javax.enterprise.context.Dependent;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

@Dependent
public class DCIPImportingSection extends ImportingSection<ObjectNode> {

	private static final ObjectNode POISON = new ObjectMapper().createObjectNode();
	
    private final Set<String> identifiers = new HashSet<>();

    private AtomicInteger read = new AtomicInteger();

    public DCIPImportingSection() {
    	super(ObjectNode.class);
	}

	@Override
	protected ObjectNode poisonObject() {
		return POISON;
	}

	@Override
	public Void call() {
		log.info("Starting import section for {}.", run.getHarvester().getName());

		int page = 0;
		try {
			List<ObjectNode> datasets;
			do {
				datasets = nextPage(++page);
				for (ObjectNode dataset : datasets) {
					put(dataset);
				}
			} while (!datasets.isEmpty());

			finish();

		} catch (GeneralHttpException e) {
			failed("(" + e.getStatusCode() + " " + e.getMessage() + ") " + e.getBody(), e);
		} catch (ProtocolException | IOException e) {
			failed(e.getMessage(), e);
		} catch (InterruptedException e) {
			log.warn("Thread interrupted", e);
			run.addLogWarning("Import section interrupted.", ServiceStage.import_stage, LogEnums.Category.system);
			return null;
		}

		log.info("Finished import section for {}.", run.getHarvester().getName());
		run.addLogInfo("Finished import section. " + read.get() + " datasets read, " + putCounter() + " datasets imported.", ServiceStage.import_stage, LogEnums.Category.system);

		return null;
	}

	protected List<ObjectNode> nextPage(int page) throws IOException, ProtocolException {
        ObjectNode result = httpClient.get("?page=" + page);
        return result != null ? parseResult(result) : Collections.emptyList();
	}
	
	private List<ObjectNode> parseResult(ObjectNode result) {
		ArrayList<ObjectNode> list = new ArrayList<>();
		JsonNode results = result.path("dataset");
		if (results.isMissingNode()) {
			results = result.path("datasets");
		}
		if (results.isArray()) {
			list.ensureCapacity(results.size());
			for (JsonNode element : results) {
				if (element.hasNonNull("id")) {
					identifiers.add(element.path("id").textValue());
				} else {
					identifiers.add(element.path("identifier").textValue());					
				}
				list.add((ObjectNode) element);
			}
		}
		read.addAndGet(list.size());
		run.setNumberToProcess(read.get());
		return list;    	
	}

	@Override
	public Set<String> listIdentifiers() throws IOException, ProtocolException {
		return identifiers;
	}

}
