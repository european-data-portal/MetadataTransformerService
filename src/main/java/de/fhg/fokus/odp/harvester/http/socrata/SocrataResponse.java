package de.fhg.fokus.odp.harvester.http.socrata;

import com.fasterxml.jackson.databind.JsonNode;
import de.fhg.fokus.odp.harvester.http.HttpResponse;

public class SocrataResponse extends HttpResponse<JsonNode> {

    public SocrataResponse(JsonNode content) {
        super(content);
    }

    @Override
    public boolean isError() {
        return content == null || content.path("results").isMissingNode();
    }

    @Override
    public boolean isSuccess() {
        return !isError();
    }

    @Override
    public SocrataResult getResult() {
        return isSuccess() ? new SocrataResult(content) : null;
    }

    @Override
    public SocrataError getError() {
        return isError() ? new SocrataError(content) : null;
    }
}