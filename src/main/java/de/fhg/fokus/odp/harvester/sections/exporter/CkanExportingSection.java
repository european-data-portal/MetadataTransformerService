package de.fhg.fokus.odp.harvester.sections.exporter;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

import javax.enterprise.context.Dependent;
import javax.inject.Inject;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.node.TextNode;

import de.fhg.fokus.odp.harvester.exceptions.GeneralHttpException;
import de.fhg.fokus.odp.harvester.exceptions.ProtocolException;
import de.fhg.fokus.odp.harvester.exceptions.SectionFailedException;
import de.fhg.fokus.odp.harvester.exceptions.ValidationException;
import de.fhg.fokus.odp.harvester.http.HttpConnector;
import de.fhg.fokus.odp.harvester.http.ckan.CkanError;
import de.fhg.fokus.odp.harvester.http.ckan.CkanResponse;
import de.fhg.fokus.odp.harvester.persistence.entities.Harvester;
import de.fhg.fokus.odp.harvester.persistence.entities.Repository;
import de.fhg.fokus.odp.harvester.persistence.enums.LogEnums;
import de.fhg.fokus.odp.harvester.persistence.enums.ServiceStage;
import de.fhg.fokus.odp.harvester.rdf.CountriesAuthorityTable;
import de.fhg.fokus.odp.harvester.sections.JobSection;
import de.fhg.fokus.odp.harvester.sections.QueuedJobSection;
import de.fhg.fokus.odp.harvester.service.RunHandler;
import de.fhg.fokus.odp.harvester.util.CkanUtil;
import org.apache.http.Header;
import org.apache.http.NoHttpResponseException;

@Dependent
public class CkanExportingSection extends JobSection {

	private static final String ORGANIZATION_TYPE = "catalogue_publisher";

	private HttpConnector<ObjectNode> httpClient;

	private String organizationId;
	private String organizationName;

	private QueuedJobSection<ObjectNode> queuedSection;

	private AtomicInteger counter = new AtomicInteger();

	@Inject
	private CountriesAuthorityTable countriesAuthorityTable;

	public boolean init(RunHandler run, JobSection section) {
		if (!super.init(run, section)) {
			return false;
		}
		queuedSection = (QueuedJobSection<ObjectNode>) attached;

		Harvester harvester = run.getHarvester();
		Header[] headers = CkanUtil.addApiKeyHeader(harvester.getTargetApiKey(), null);
		httpClient = HttpConnector.createConnector(harvester.getTarget().getUrl(), headers, ObjectNode.class);

		Repository source = harvester.getSource();

		String name;
		try {
			name = mungeTitle(source.getName());
		} catch (ProtocolException | IOException e) {
			log.error("munging organization title", e);
			run.addLogError("Munging organization title: " + e.getMessage(), ServiceStage.init_stage, LogEnums.Category.dataset, null);
			return false;
		}

		ObjectMapper mapper = new ObjectMapper();
		ObjectNode organization = mapper.createObjectNode();
		organization.put("name", name);
		organization.put("title", source.getName());
		organization.put("description", source.getDescription());
		organization.put("type", ORGANIZATION_TYPE);
		ObjectNode publisher = organization.with("publisher");
		publisher.put("type", "http://xmlns.com/foaf/0.1/Organization");
		publisher.put("name", source.getPublisher());
		if (source.getPublisherEmail() != null && !source.getPublisherEmail().trim().isEmpty()) {
			publisher.put("email", harvester.getSource().getPublisherEmail().trim());
		}
		if (source.getHomepage() != null) {
			organization.put("homepage", source.getHomepage());
		}
		if (!source.getSpatial().isEmpty()) {
			ArrayNode spatial = organization.withArray("spatial");
			for (String s : source.getSpatial()) {
				// map temporarily to alpha 2 code, but should be the original
				// uriref
				spatial.add(countriesAuthorityTable.asAlpha2Code(s));
			}
		}

		try {
			ObjectNode org = existsOrganization(name);
			organization.put("id", org.get("id").textValue());
			org = updateOrganization(organization);
			organizationName = org.path("name").textValue();
			organizationId = org.path("id").textValue();
		} catch (GeneralHttpException e) {
			switch (e.getStatusCode()) {
				case 404: // Not Found
					try {
						ObjectNode org = createOrganization(organization);
						organizationName = org.path("name").textValue();
						organizationId = org.path("id").textValue();
					} catch (GeneralHttpException e1) {
						log.error("organization creation", e1);
						run.addLogError("Organization creation: " + e1.getStatusCode() + " " + e1.getMessage(), ServiceStage.init_stage, LogEnums.Category.connection, null);
						return false;
					} catch (ProtocolException | IOException e1) {
						log.error("organization creation", e1);
						run.addLogError("Organization creation: " + e1.getMessage(), ServiceStage.init_stage, LogEnums.Category.connection, null);
						return false;
					}
					break;
				default:
					log.error("organization update", e);
					run.addLogError("Organization update: " + e.getStatusCode() + " " + e.getMessage(), ServiceStage.init_stage, LogEnums.Category.system, null);
					return false;
			}
		} catch (ProtocolException | IOException e) {
			log.error("organization update", e);
			run.addLogError("Organization update: " + e.getMessage(), ServiceStage.init_stage, LogEnums.Category.connection, null);
			return false;
		}
		return true;
	}

	public Void call() throws SectionFailedException {
		log.info("Starting export section for {}.", run.getHarvester().getName());

		try {
			ObjectNode dataset = queuedSection.take();
			while (!queuedSection.isFinished(dataset)) {
				final int current = counter.incrementAndGet();
				completeDataset(dataset);
				try {
					validateDataset(dataset);
					exportDataset(dataset);
					log.trace("Exported ({}) {}", current, dataset.path("name").textValue());
				} catch (ValidationException e) {
					log.error("Dataset {} invalid", current);
					run.incrementRejected();
					run.addLogError("(" + dataset.path("name").textValue() + ") " + e.getMessage(), ServiceStage.export_stage, LogEnums.Category.dataset, null);
				} catch (IOException e) {
					run.incrementRejected();
					log.error("IO error occured while exporting dataset {}: {}.", dataset.toString(), e.getMessage());
					run.addLogError("IO error: " + e.getMessage(), ServiceStage.export_stage, LogEnums.Category.connection, null);
					throw new SectionFailedException("Export section", e);
				}

				dataset = queuedSection.take();
			}

			// queue in again for all other threads working on this queue
			queuedSection.finish();

			log.info("Finished export section for {}.", run.getHarvester().getName());
			run.addLogInfo("Finished export section. " + queuedSection.takeCounter() + " datasets processed.", ServiceStage.export_stage, LogEnums.Category.system);

		} catch (InterruptedException e) {
			log.warn("Export thread interrupted", e);
			run.addLogWarning("Export section interrupted.", ServiceStage.export_stage, LogEnums.Category.system);
		}
		return null;
	}

	private void exportDataset(ObjectNode dataset) throws IOException {
		String packageName = CkanUtil.getPackageName(dataset);
		if (packageName == null || packageName.trim().isEmpty()) {
			run.incrementRejected();
			run.addLogError("Dataset has no unique identifier", ServiceStage.export_stage, LogEnums.Category.dataset,
					new ObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(dataset));
			return;
		}

		// completeDataset ensures that this value exists!
		String identifier = dataset.path("identifier").path(0).textValue();

		// create hash value
		addHashValue(dataset);

		try {
			// now we have to look for a dataset with the same identifier in
			// this catalogue
			ObjectNode result = searchInCatalogue(identifier);

			switch (result == null ? 0 : result.path("count").asInt()) {
				case 0:
					String name = mungeName(packageName);
					if (!isNameValid(name, "package")) {
						name = findName(name);
					}

					dataset.put("name", name);
					ObjectNode aobj = createDataset(dataset);
					if (aobj != null) {
						run.incrementAdded();
					} else {
						run.incrementRejected();
					}
					break;
				case 1:
					ObjectNode existingDataset = (ObjectNode) result.path("results").get(0);

					if (!hashEqual(dataset, existingDataset) || run.getHarvester().isForceUpdate()) {
						dataset.put("name", existingDataset.path("name").textValue());

						ObjectNode uobj = updateDataset(dataset);
						if (uobj != null) {
							run.incrementUpdated();
						} else {
							run.incrementRejected();
						}
					} else {
						log.debug("dataset {} not modified, no update", existingDataset.path("name").textValue());
						run.incrementSkipped();
					}
					break;
				default:
					// hell, that means something went totally wrong!?
					log.error("hell, that means something went totally wrong?! {}: {}", result.path("count").asInt(), identifier);
					run.addLogWarning(
							"Different datasets with identical identifier exists (" + identifier + ": "
									+ result.path("count").asInt()
									+ " times). Doing some cleanup!",
							ServiceStage.export_stage, LogEnums.Category.dataset);

					// we should remove all and create the new one!!!
					for (JsonNode d : result.withArray("results")) {
						if (d.isObject()) {
							purgeDataset(d.path("id").textValue());
						}
					}
					String newname = mungeName(packageName);
					if (!isNameValid(newname, "package")) {
						newname = findName(newname);
					}

					dataset.put("name", newname);
					ObjectNode cobj = createDataset(dataset);
					if (cobj != null) {
						run.incrementUpdated(); // its an update cause of cleanup scenario
					} else {
						run.incrementRejected();
					}
			}
		} catch (GeneralHttpException e) {
			run.incrementRejected();
			log.error("HTTP error occured while exporting dataset: " + e.getBody(), e);
			run.addLogError("(" + e.getStatusCode() + " " + e.getMessage() + ") " + e.getBody(), ServiceStage.export_stage, LogEnums.Category.dataset, new ObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(dataset));
		} catch (ProtocolException | IOException e) {
			run.incrementRejected();
			log.error("error occured while exporting dataset", e);
			run.addLogError(e.getMessage(), ServiceStage.export_stage, LogEnums.Category.dataset, new ObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(dataset));
		}

	}

	private ObjectNode createDataset(ObjectNode dataset) throws IOException, ProtocolException {
		JsonNode response = httpClient.post("/api/action/package_create", dataset.toString());
		return handleOrganizationResponse(response);
	}

	public ObjectNode updateDataset(ObjectNode dataset) throws IOException, ProtocolException {
		JsonNode response = httpClient.post("/api/action/package_update", dataset.toString());
		return handleOrganizationResponse(response);
	}

	public void purgeDataset(String id) throws IOException, ProtocolException {
		ObjectNode content = new ObjectMapper().createObjectNode();
		content.put("id", id);
		int tries = 3;
		while (tries > 0) {
			try {
				JsonNode response = httpClient.post("/api/action/dataset_purge", content.toString());
				CkanResponse ckanResponse = new CkanResponse(response);
				if (ckanResponse.isError()) {
					log.error("trying to purge {}", id);
					throw ckanResponse.getError().asException();
				}
				tries = 0;
			} catch (NoHttpResponseException e) {
				--tries;
				if (tries == 0) {
					throw e;
				}
			} catch (GeneralHttpException e) {
				if (e.getStatusCode() == 404) {
					log.error("trying to purge {}", id);
					run.addLogWarning("Not found during delete: " + id, ServiceStage.delete_stage, LogEnums.Category.dataset);
					tries = 0;
				}
			}
		}
	}

	private void completeDataset(ObjectNode dataset) {
		dataset.replace("owner_org", new TextNode(organizationId));

		if (dataset.with("translation_meta").path("default").isMissingNode()) {
			dataset.with("translation_meta").set("default", new TextNode(run.getHarvester().getSource().getLanguage().getLanguageKey()));
		}

		if (dataset.hasNonNull("name")) {
			String name = dataset.path("name").textValue();
			JsonNode identifiers = dataset.path("identifier");
			if (identifiers.isArray() && !identifiers.path(0).textValue().equals(name)) {
				((ArrayNode) identifiers).insert(0, name);
			} else if (identifiers.isMissingNode()) {
				dataset.withArray("identifier").insert(0, name);
			}
		}
	}

	private void validateDataset(ObjectNode dataset) throws ValidationException {
		CkanUtil.checkIfSpatialIsCorrect(dataset);
	}

	private ObjectNode existsOrganization(String name) throws IOException, ProtocolException {
		JsonNode response = httpClient.get("/api/action/organization_show?include_datasets=False&id=" + name);
		return handleOrganizationResponse(response);
	}

	private ObjectNode createOrganization(ObjectNode organization) throws IOException, ProtocolException {
		JsonNode response = httpClient.post("/api/action/organization_create", organization.toString());
		return handleOrganizationResponse(response);
	}

	private ObjectNode updateOrganization(ObjectNode organization) throws IOException, ProtocolException {
		JsonNode response = httpClient.post("/api/action/organization_update", organization.toString());
		return handleOrganizationResponse(response);
	}

	private ObjectNode handleOrganizationResponse(JsonNode response) throws ProtocolException {
		CkanResponse ckanResponse = new CkanResponse(response);
		if (ckanResponse.isError()) {
			CkanError ckanError = ckanResponse.getError();
			throw ckanError.asException();
		} else if (ckanResponse.isSuccess()) {
			return (ObjectNode) ckanResponse.getResult().getContent();
		} else {
			log.debug(ckanResponse.getHelp());
		}
		return null;
	}

	private String mungeTitle(String title) throws IOException, ProtocolException {
		try {
			JsonNode response = httpClient.get("/api/util/dataset/munge_title_to_name?title=" + URLEncoder.encode(title, "UTF-8"));
			return response.textValue();
		} catch (UnsupportedEncodingException e) {
			log.error("encoding title {}", title, e);
		}
		return null;
	}

	private String mungeName(String name) throws IOException, ProtocolException {
		try {
			JsonNode response = httpClient.get("/api/util/dataset/munge_title_to_name?title=" + URLEncoder.encode(name, "UTF-8"));
			return response.textValue();
		} catch (UnsupportedEncodingException e) {
			log.error("encoding name {}", name, e);
		}
		return null;
	}

	private boolean isNameValid(String name, String type) throws IOException, ProtocolException {
		try {
			JsonNode response = httpClient.get("/api/util/is_slug_valid?slug=" + URLEncoder.encode(name, "UTF-8") + "&type=" + type);
			return response.path("valid").asBoolean() && name.length() <= 100;
		} catch (UnsupportedEncodingException e) {
			log.error("encoding name {}", name, e);
			return false;
		}
	}

	private ObjectNode searchInCatalogue(String identifier) throws IOException, ProtocolException {
		String search = "\"" + identifier + "\"";
		JsonNode response = httpClient.get(
				"/api/action/package_search?q=source_identifier:" + URLEncoder.encode(search, "UTF-8") + "%20organization:"
						+ organizationName + "&fq=%2Bstate:" + URLEncoder.encode("(active OR draft)", "UTF-8"));
		return handleOrganizationResponse(response);
	}

	private void addHashValue(ObjectNode dataset) {
		try {
			ObjectMapper mapper = new ObjectMapper();
			MessageDigest digest = MessageDigest.getInstance("MD5");
			String d = mapper.writeValueAsString(dataset);
			byte[] hash = digest.digest(d.getBytes("UTF-8"));

			ArrayNode extras = dataset.withArray("extras");
			ObjectNode modhash = mapper.createObjectNode();
			modhash.put("key", "modhash");
			modhash.put("value", hash);
			extras.add(modhash);

		} catch (NoSuchAlgorithmException | JsonProcessingException | UnsupportedEncodingException e) {
			log.error("calculating and adding modification hash value", e);
		}
	}

	private boolean hashEqual(ObjectNode dataset, ObjectNode existingDataset) {
		try {
			byte[] hash1 = null;
			ArrayNode originalExtras = dataset.withArray("extras");
			for (JsonNode extra : originalExtras) {
				if (extra.path("key").textValue().equals("modhash")) {
					hash1 = extra.path("value").binaryValue();
				}
			}
			byte[] hash2 = null;
			ArrayNode existingExtras = existingDataset.withArray("extras");
			for (JsonNode extra : existingExtras) {
				if (extra.path("key").textValue().equals("modhash")) {
					hash2 = extra.path("value").binaryValue();
				}
			}
			return hash1 != null && hash2 != null && MessageDigest.isEqual(hash1, hash2);
		} catch (IOException e) {
			log.error("reading and comparing modification hash values", e);
		}
		return false;
	}

	public void postDelete(Set<String> identifiers) throws IOException, ProtocolException {
		int start = 0;
		int rows = 100;
		ArrayNode delete = new ObjectMapper().createArrayNode();
		ArrayNode datasets = new ObjectMapper().createArrayNode();
		do {
			try {
				JsonNode response = httpClient.get("/api/action/package_search?q=organization:" + organizationName
						+ "&fq=%2Bstate:" + URLEncoder.encode("(active OR draft)", "UTF-8") + "&rows=" + rows
						+ "&start=" + start);
				CkanResponse ckanResponse = new CkanResponse(response);
				if (ckanResponse.isError()) {
					throw ckanResponse.getError().asException();
				} else if (ckanResponse.isSuccess()) {
					JsonNode results = ckanResponse.getResult().getContent().path("results");
					if (results.isArray()) {
						datasets = (ArrayNode) results;
						for (JsonNode node : datasets) {
							JsonNode ids = node.path("identifier");
							if (ids.isArray() && ids.size() > 0) {
								String id = ids.path(0).textValue();
								if (!identifiers.contains(id)) {
									log.debug("purge dataset\n{}", new ObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(node));
									delete.add(node.path("id").textValue());
								}
							} else {
								log.debug("purge dataset cause missing source identifier (cleanup)\n{}", new ObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(node));
								delete.add(node.path("id").textValue());
							}
						}
					}
				} else {
					throw new ProtocolException("unknown response format: " + new ObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(response));
				}
				start += rows;
			} catch (UnsupportedEncodingException e) {
				log.error("parsing json", e);
				return;
			}
			if (Thread.interrupted()) {
				return;
			}
		} while (datasets.size() > 0);

		if (delete.size() > 0) {
			run.addLogInfo("Start deleting " + delete.size() + " datasets.", ServiceStage.delete_stage, LogEnums.Category.system);
			log.debug("deleting the following datasets:");
			delete.forEach(del -> log.debug(del.toString()));
			for (JsonNode del : delete) {
				purgeDataset(del.textValue());
				run.incrementDeleted();
				if (Thread.interrupted()) {
					break;
				}
			}

			log.debug("Finished deletion section. Deleted {} datasets.", delete.size());
			run.addLogInfo("Finished deletion section. Deleted " + delete.size() + " datasets.", ServiceStage.delete_stage, LogEnums.Category.dataset);

		} else {
			log.debug("Finished deletion section. No datasets deleted.", delete.size());
			run.addLogInfo("Finished deletion section. No datasets deleted.", ServiceStage.delete_stage, LogEnums.Category.dataset);
		}

	}

	private String findName(String name) throws ProtocolException, IOException {
		String newname = name;
		if (newname.length() > 98) {
			newname = newname.substring(0, 97);
		}
		int i = 0;
		while (!isNameValid(newname + String.format("%02d", ++i), "package")) {
			// incrementing until valid
		}
		newname += String.format("%02d", i);
		return newname;
	}

	public void close() {
		if (httpClient != null) {
			httpClient.close();
		}
	}

}
