package de.fhg.fokus.odp.harvester.persistence.entities;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.persistence.*;

import de.fhg.fokus.odp.harvester.util.TransformationScriptDownloader;
import de.fhg.fokus.odp.harvester.persistence.enums.Frequency;
import de.fhg.fokus.odp.harvester.persistence.enums.HarvesterEnums;
import de.fhg.fokus.odp.harvester.persistence.enums.Visibility;

@Entity
@NamedQueries({
        @NamedQuery(name = "harvesters", query = "select h from Harvester h order by h.name"),
        @NamedQuery(name = "harvesters_of_visibility", query = "select h from Harvester h where h.visibility = :visibility order by h.name"),
        @NamedQuery(name = "harvesters_of_visibility_or_from_owner", query = "select h from Harvester h where h.visibility = :visibility or h.owner = :owner order by h.name"),
        @NamedQuery(name = "harvesters_from_owner", query = "select h from Harvester h where h.owner = :owner order by h.name"),
        @NamedQuery(name = "harvesters_search", query = "select h from Harvester h where lower(h.name) like :searchterm and h.visibility = :visibility order by h.name"),
        @NamedQuery(name = "harvesters_search_from_owner", query = "select distinct h from Harvester h where lower(h.name) like :searchterm and (h.owner = :owner or h.visibility = :visibility) order by h.name"),
        @NamedQuery(name = "harvesters_running", query = "select h from Harvester h join h.runs r where h.owner = :owner and r.runState = de.fhg.fokus.odp.harvester.persistence.enums.RunState.running order by r.startTime desc"),
        @NamedQuery(name = "harvesters_finished_from_owner", query = "select h from Run r join r.harvester h where h.owner = :owner and r.runState <> de.fhg.fokus.odp.harvester.persistence.enums.RunState.running and r.endTime is not null order by r.endTime desc"),
        @NamedQuery(name = "harvesters_scheduled_from_owner", query = "select h from Harvester h where h.owner = :owner and h.frequency <> de.fhg.fokus.odp.harvester.persistence.enums.Frequency.manually order by h.nextHarvestDate asc"),
        @NamedQuery(name = "harvesters_with_errors", query = "SELECT DISTINCT h from Harvester h JOIN h.runs r WHERE r.runState = de.fhg.fokus.odp.harvester.persistence.enums.RunState.error"),
        @NamedQuery(name = "harvesters_with_errors_from_owner", query = "SELECT DISTINCT h from Harvester h JOIN h.runs r WHERE r.runState = de.fhg.fokus.odp.harvester.persistence.enums.RunState.error AND h.owner = :owner order by h.name"),
        @NamedQuery(name = "harvesters_with_errors_of_visibility", query = "SELECT DISTINCT h from Harvester h JOIN h.runs r WHERE r.runState = de.fhg.fokus.odp.harvester.persistence.enums.RunState.error AND h.visibility = :visibility"),
        @NamedQuery(name = "harvesters_with_errors_of_visibility_or_from_owner", query = "SELECT DISTINCT h from Harvester h JOIN h.runs r WHERE r.runState = de.fhg.fokus.odp.harvester.persistence.enums.RunState.error AND (h.visibility = :visibility or h.owner = :owner)"),
        @NamedQuery(name = "harvesters_search_with_errors", query = "SELECT DISTINCT h from Harvester h JOIN h.runs r WHERE r.runState = de.fhg.fokus.odp.harvester.persistence.enums.RunState.error AND lower(h.name) like :searchterm and h.visibility = :visibility"),
        @NamedQuery(name = "harvesters_search_with_errors_from_owner", query = "SELECT DISTINCT h from Harvester h JOIN h.runs r WHERE r.runState = de.fhg.fokus.odp.harvester.persistence.enums.RunState.error AND lower(h.name) like :searchterm and (h.owner = :owner or h.visibility = :visibility)")
})
public class Harvester {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String name;

    @ManyToOne(fetch = FetchType.EAGER)
    private Repository source;

    @ManyToOne(fetch = FetchType.EAGER)
    private Repository target;

    private String sourceApiKey;

    private String targetApiKey;

    private Integer threadCount = 1;

    private String description;

    @OneToMany(mappedBy = "harvester", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
    private List<Activity> activities;

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
    @OrderBy("index asc")
    private Set<Filter> filters;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "harvester", cascade = CascadeType.ALL, orphanRemoval = true)
    @OrderBy("startTime desc")
    private Set<Run> runs;

    private boolean emailNotification = false;

    private String owner;

    @Enumerated(EnumType.STRING)
    private HarvesterEnums.TransformationScriptSource transformationScriptSource;

    private String gitRepoUrl;

    private String gitBranch;

    private String gitFileName;

    private boolean gitRepoRequiresCredentials;

    private boolean gitRepoSshAuthentication;

    /*
    JGit can only handle plain text credentials
     */
    private String gitUserName;

    private String gitPassWord;

    @Lob
    private String mappingScript;

    @Enumerated(EnumType.STRING)
    private Frequency frequency;

    @Enumerated(EnumType.STRING)
    private Visibility visibility;

    @Enumerated(EnumType.STRING)
    private HarvesterEnums.HarvestStrategy harvestStrategy = HarvesterEnums.HarvestStrategy.create;

    @Temporal(TemporalType.TIMESTAMP)
    private Date nextHarvestDate;

    @Temporal(TemporalType.TIMESTAMP)
    private Date created;

    @Temporal(TemporalType.TIMESTAMP)
    private Date updated;

    private boolean forceUpdate;

    public Harvester() {
        filters = new HashSet<>();
        runs = new HashSet<>();
    }

    public List<Activity> getActivities() {
        return this.activities;
    }

    public void setActivities(List<Activity> acts) {
        this.activities = acts;
    }

    @PrePersist
    public void onPersist() {
        created = new Date();
    }

    @PreUpdate
    public void onUpdate() {
        updated = new Date();
    }

    public boolean getEmailNotification() {
        return emailNotification;
    }

    public void setEmailNotification(boolean emailNotification) {
        this.emailNotification = emailNotification;
    }

    public Date getUpdated() {
        return updated;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getNextHarvestDate() {
        return this.nextHarvestDate;
    }

    public void setNextHarvestDate(Date nextHarvestDate) {
        this.nextHarvestDate = nextHarvestDate;
    }

    public String getReadableTimeStamp() {
        return new SimpleDateFormat("EEE, MMM d, HH:mm").format(nextHarvestDate);
    }

    public Frequency getFrequency() {
        return this.frequency;
    }

    public void setFrequency(Frequency frequency) {
        this.frequency = frequency;
    }

    public Visibility getVisibility() {
        return this.visibility;
    }

    public void setVisibility(Visibility visibility) {
        this.visibility = visibility;
    }

    public Integer getThreadCount() {
        return threadCount;
    }

    public void setThreadCount(Integer threadCount) {
        this.threadCount = threadCount;
    }

    public Set<Filter> getFilters() {
        return this.filters;
    }

    //Sorts all filters with the enabled flag set ascending by their index
    public List<Filter> getEnabledAndSortedFilters() {
        List<Filter> result = filters.stream().filter(Filter::isFilterEnabled).collect(Collectors.toList());
        result.sort((f1, f2) -> f1.getIndex() - f2.getIndex());
        return result;
    }

    public void setFilters(Set<Filter> filters) {
        this.filters = filters;
    }

    public Set<Run> getRuns() {
        return this.runs;
    }

    public void setRuns(Set<Run> runs) {
        this.runs = runs;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTargetApiKey() {
        return this.targetApiKey;
    }

    public void setTargetApiKey(String targetApiKey) {
        this.targetApiKey = targetApiKey;
    }

    public String getSourceApiKey() {
        return this.sourceApiKey;
    }

    public void setSourceApiKey(String sourceApiKey) {
        this.sourceApiKey = sourceApiKey;
    }

    public Repository getTarget() {
        return this.target;
    }

    public void setTarget(Repository target) {
        this.target = target;
    }

    public Repository getSource() {
        return this.source;
    }

    public void setSource(Repository source) {
        this.source = source;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSampleTransformationFileName() {
        if ((getSource() != null) && (getTarget() != null)) {
            return getSource().getRepositoryType().name() + "-to-" + getTarget().getRepositoryType().name() + ".script";
        }
        return null;
    }

    //Return the plain script or contents of a file from git
    public String getMappingScript() {
        return mappingScript;
    }

    public void setMappingScript(String mappingScript) {
        this.mappingScript = mappingScript;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public HarvesterEnums.TransformationScriptSource getTransformationScriptSource() {
        return transformationScriptSource;
    }

    public void setTransformationScriptSource(HarvesterEnums.TransformationScriptSource transformationScriptSource) {
        this.transformationScriptSource = transformationScriptSource;
    }

    public String getGitRepoUrl() {
        return gitRepoUrl;
    }

    public void setGitRepoUrl(String gitRepoUrl) {
        this.gitRepoUrl = gitRepoUrl;
    }

    public String getGitBranch() {
        return gitBranch;
    }

    public void setGitBranch(String gitBranch) {
        this.gitBranch = gitBranch;
    }

    public String getGitFileName() {
        return gitFileName;
    }

    public void setGitFileName(String gitFileName) {
        this.gitFileName = gitFileName;
    }

    public boolean isGitRepoRequiresCredentials() {
        return gitRepoRequiresCredentials;
    }

    public void setGitRepoRequiresCredentials(boolean gitRepoRequiresCredentials) {
        this.gitRepoRequiresCredentials = gitRepoRequiresCredentials;
    }

    public boolean isGitRepoSshAuthentication() {
        return gitRepoSshAuthentication;
    }

    public void setGitRepoSshAuthentication(boolean gitRepoSshAuthentication) {
        this.gitRepoSshAuthentication = gitRepoSshAuthentication;
    }

    public String getGitUserName() {
        return gitUserName;
    }

    public void setGitUserName(String gitUserName) {
        this.gitUserName = gitUserName;
    }

    public String getGitPassWord() {
        return gitPassWord;
    }

    public void setGitPassWord(String gitPassWord) {
        this.gitPassWord = gitPassWord;
    }

    public HarvesterEnums.HarvestStrategy getHarvestStrategy() {
        return harvestStrategy;
    }

    public void setHarvestStrategy(HarvesterEnums.HarvestStrategy harvestStrategy) {
        this.harvestStrategy = harvestStrategy;
    }

    public boolean isForceUpdate() {
        return forceUpdate;
    }

    public void setForceUpdate(boolean forceUpdate) {
        this.forceUpdate = forceUpdate;
    }

}
