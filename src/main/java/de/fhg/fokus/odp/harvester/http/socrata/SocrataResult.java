package de.fhg.fokus.odp.harvester.http.socrata;

import com.fasterxml.jackson.databind.JsonNode;
import de.fhg.fokus.odp.harvester.http.HttpResult;

public class SocrataResult extends HttpResult<JsonNode> {

    public SocrataResult(JsonNode result) {
        super(result);
    }

    @Override
    public JsonNode getContent() {
        return result;
    }
}