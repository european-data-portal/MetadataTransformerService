package de.fhg.fokus.odp.harvester.exceptions;


public class InstanceExistsException extends ProtocolException {
    /**
     * Serialization version.
     */
    private static final long serialVersionUID = 1L;

    public InstanceExistsException() {
    }

    public InstanceExistsException(String message) {
        super(message);
    }

    public InstanceExistsException(Throwable cause) {
        super(cause);
    }

    public InstanceExistsException(String message, Throwable cause) {
        super(message, cause);
    }
}
