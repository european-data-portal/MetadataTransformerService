package de.fhg.fokus.odp.harvester.actions;

import de.fhg.fokus.odp.harvester.persistence.LogsManager;
import de.fhg.fokus.odp.harvester.persistence.entities.Harvester;
import de.fhg.fokus.odp.harvester.persistence.entities.Log;
import de.fhg.fokus.odp.harvester.persistence.entities.Run;
import de.fhg.fokus.odp.harvester.persistence.enums.LogEnums;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.transaction.Transactional;
import java.io.Serializable;
import java.util.*;

@Named
@ViewScoped
public class LogController implements Serializable {

	private static final long serialVersionUID = 2292457296119003938L;

    @Inject
    private LogsManager logsManager;

    private Run run;

    private String searchTerm;
    private List<LogEnums.Severity> selectedSeverities;
    private List<LogEnums.Category> selectedCategories;

    private int numberOfLogsToShow;

    public void initController(Run run) {

        this.run = run;

        //ArrayList required by JSF
        selectedSeverities = new ArrayList<>(Arrays.asList(LogEnums.Severity.values()));
        selectedCategories = new ArrayList<>(Arrays.asList(LogEnums.Category.values()));
        searchTerm = "";
        numberOfLogsToShow = 10;
    }

    @Transactional
    public List<Log> getFilteredLogs() {
        return logsManager.getFilteredLogs(run, selectedSeverities, selectedCategories, searchTerm, numberOfLogsToShow);
    }

    public int getNumberOfRows() {
        return numberOfLogsToShow;
    }

    public void incrementNumberOfRows() {
        numberOfLogsToShow += 10;
    }

    public String getSearchTerm() {
        return searchTerm;
    }

    public void setSearchTerm(String searchTerm) {
        this.searchTerm = searchTerm;
    }

    public boolean isInfoLogs() {
    	return selectedSeverities.contains(LogEnums.Severity.info);
    }
    
    public void setInfoLogs(boolean infos) {
    	if (!infos) {
    		selectedSeverities.remove(LogEnums.Severity.info);
    	} else {
    		if (!selectedSeverities.contains(LogEnums.Severity.info)) {
        		selectedSeverities.add(LogEnums.Severity.info);    		    			
    		}
    	}
    }
    
    public boolean isWarningLogs() {
    	return selectedSeverities.contains(LogEnums.Severity.warning);
    }
    
    public void setWarningLogs(boolean warnings) {
    	if (!warnings) {
    		selectedSeverities.remove(LogEnums.Severity.warning);
    	} else {
    		if (!selectedSeverities.contains(LogEnums.Severity.warning)) {
        		selectedSeverities.add(LogEnums.Severity.warning);    		    			
    		}
    	}
    }
    
    public boolean isErrorLogs() {
    	return selectedSeverities.contains(LogEnums.Severity.error);
    }
    
    public void setErrorLogs(boolean errors) {
    	if (!errors) {
    		selectedSeverities.remove(LogEnums.Severity.error);
    	} else {
    		if (!selectedSeverities.contains(LogEnums.Severity.error)) {
        		selectedSeverities.add(LogEnums.Severity.error);    		    			
    		}
    	}
    }
    
    public boolean isSystemLogs() {
    	return selectedCategories.contains(LogEnums.Category.system);
    }
    
    public void setSystemLogs(boolean systems) {
    	if (!systems) {
    		selectedCategories.remove(LogEnums.Category.system);
    	} else {
    		if (!selectedCategories.contains(LogEnums.Category.system)) {
        		selectedCategories.add(LogEnums.Category.system);    		    			
    		}
    	}
    }
    
    public boolean isConnectionLogs() {
    	return selectedCategories.contains(LogEnums.Category.connection);
    }
    
    public void setConnectionLogs(boolean connections) {
    	if (!connections) {
    		selectedCategories.remove(LogEnums.Category.connection);
    	} else {
    		if (!selectedCategories.contains(LogEnums.Category.connection)) {
        		selectedCategories.add(LogEnums.Category.connection);    		    			
    		}
    	}
    }
    
    public boolean isTransformationLogs() {
    	return selectedCategories.contains(LogEnums.Category.transformation);
    }
    
    public void setTransformationLogs(boolean transformations) {
    	if (!transformations) {
    		selectedCategories.remove(LogEnums.Category.transformation);
    	} else {
    		if (!selectedCategories.contains(LogEnums.Category.transformation)) {
        		selectedCategories.add(LogEnums.Category.transformation);    		    			
    		}
    	}
    }
    
    public boolean isDatasetLogs() {
    	return selectedCategories.contains(LogEnums.Category.dataset);
    }
    
    public void setDatasetLogs(boolean datasets) {
    	if (!datasets) {
    		selectedCategories.remove(LogEnums.Category.dataset);
    	} else {
    		if (!selectedCategories.contains(LogEnums.Category.dataset)) {
        		selectedCategories.add(LogEnums.Category.dataset);    		    			
    		}
    	}
    }

    public boolean isUnknownLogs() {
        return selectedCategories.contains(LogEnums.Category.unknown);
    }

    public void setUnknownLogs(boolean unknown) {
        if (!unknown) {
            selectedCategories.remove(LogEnums.Category.unknown);
        } else {
            if (!selectedCategories.contains(LogEnums.Category.unknown)) {
                selectedCategories.add(LogEnums.Category.unknown);
            }
        }
    }
}
