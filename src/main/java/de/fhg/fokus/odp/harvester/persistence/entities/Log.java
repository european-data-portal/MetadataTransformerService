package de.fhg.fokus.odp.harvester.persistence.entities;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Pattern;

import javax.persistence.*;

import de.fhg.fokus.odp.harvester.persistence.enums.LogEnums;
import de.fhg.fokus.odp.harvester.persistence.enums.ServiceStage;

@Entity
@NamedQueries({
		@NamedQuery(name = "all_logs_for_run", query = "select l from Log l where l.run = :run order by l.created desc"),
		@NamedQuery(name = "number_logs_from_run", query = "select count(l) from Log l where l.run = :run"),
		@NamedQuery(name = "number_logs_from_run_with_severity", query = "select count(l) from Log l where l.run = :run and l.severity = :severity"),
		@NamedQuery(name = "logs_with_severity_for_run", query = "select distinct l from Log l where l.run = :run and l.severity = :severity order by l.created desc"),
		@NamedQuery(name = "logs_with_error_type_for_run", query = "select l from Log l where l.run = :run and l.severity = de.fhg.fokus.odp.harvester.persistence.enums.LogEnums$Severity.error and l.category = :category order by l.created desc"),
})
public class Log {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	
	@ManyToOne
	private Run run;
		
	@Enumerated(EnumType.STRING)
	private ServiceStage serviceStage;
	
	@Enumerated(EnumType.STRING)
	private LogEnums.Severity severity;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date created;

	@Enumerated(EnumType.STRING)
	private LogEnums.Category category;

	@Lob
	private String message;

	@Lob
	private String attachment;
	
	@PrePersist
	public void onCreate() {
		created = new Date();
	}
	
	public Date getCreated(){
		return this.created;
	}
	
	public String getMessage(){
		return this.message;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public ServiceStage getServiceStage(){
		return this.serviceStage;
	}

	public void setServiceStage(ServiceStage serviceStage){
		this.serviceStage = serviceStage;
	}

	public LogEnums.Severity getSeverity(){
		return this.severity;
	}

	public void setSeverity(LogEnums.Severity severity){
		this.severity = severity;
	}

	public Run getRun() {
		return run;
	}

	public void setRun(Run run) {
		this.run = run;
	}
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getAttachment() {
		return attachment;
	}

	public void setAttachment(String attachment) {
		this.attachment = attachment;
	}

	public LogEnums.Category getCategory() {
		return category;
	}

	public void setCategory(LogEnums.Category category) {
		this.category = category;
	}

	public LogEnums.Category parseMsgForCategory(String msg) {

		//Default value is unknown
		LogEnums.Category result = LogEnums.Category.unknown;

		//remove all special chars that could mess with regex matching
		msg = msg.replaceAll("[^\\p{L}\\p{Z}]","");

		//TODO add regEx
		final String connectionRegEx = ".*Connect to.*failed.*";
		final String datasetRegEx = ".*HTTP error occured while exporting dataset.*";
		final String transformationRegEx = "";

		if (msg != null) {
			if (Pattern.matches(connectionRegEx, msg)) {
				result = LogEnums.Category.connection;
			} else if (Pattern.matches(datasetRegEx, msg)) {
				result = LogEnums.Category.dataset;
			} else if (Pattern.matches(transformationRegEx, msg)) {
				result = LogEnums.Category.transformation;
			}
		}

		return result;
	}

	public String getShortenedMessage(Integer length) {
		if (message != null) {
			return message.length() <= length ? message : message.substring(0, length) + "...";
		} else {
			return "";
		}
	}

	public String getReadableTimeStamp() {
		return new SimpleDateFormat("EEE, MMM d, HH:mm:ss").format(created);
	}

	@Override
	public String toString() {
		return "Log{" +
				"id=" + id +
				", run=" + run +
				", serviceStage=" + serviceStage +
				", severity=" + severity +
				", created=" + created +
				", category=" + category +
				'}';
	}
}
