package de.fhg.fokus.odp.harvester.rdf;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.ResIterator;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.rdf.model.StmtIterator;
import org.apache.jena.vocabulary.RDF;
import org.apache.jena.vocabulary.SKOS;

@ApplicationScoped
public class CountriesAuthorityTable {
	
	private static final String NAMESPACE = "http://publications.europa.eu/resource/authority/country/";
	private static final String[] EUROPE = {
			// European Union
			NAMESPACE + "BEL",
			NAMESPACE + "BGR",
			NAMESPACE + "DNK",
			NAMESPACE + "DEU",
			NAMESPACE + "EST",
			NAMESPACE + "FIN",
			NAMESPACE + "FRA",
			NAMESPACE + "GRC",
			NAMESPACE + "IRL",
			NAMESPACE + "ITA",
			NAMESPACE + "HRV",
			NAMESPACE + "LVA",
			NAMESPACE + "LTU",
			NAMESPACE + "LUX",
			NAMESPACE + "MLT",
			NAMESPACE + "NLD",
			NAMESPACE + "AUT",
			NAMESPACE + "POL",
			NAMESPACE + "PRT",
			NAMESPACE + "ROU",
			NAMESPACE + "SWE",
			NAMESPACE + "SVK",
			NAMESPACE + "SVN",
			NAMESPACE + "ESP",
			NAMESPACE + "CSK",
			NAMESPACE + "HUN",
			NAMESPACE + "GBR",
			NAMESPACE + "CYP",
			// additional European countries
			NAMESPACE + "ALB",
			NAMESPACE + "AND",
			NAMESPACE + "AZE",
			NAMESPACE + "BIH",
			NAMESPACE + "GEO",
			NAMESPACE + "ISL",
			NAMESPACE + "KAZ",
			NAMESPACE + "LIE",
			NAMESPACE + "MKD",
			NAMESPACE + "MDA",
			NAMESPACE + "MCO",
			NAMESPACE + "MNE",
			NAMESPACE + "NOR",
			NAMESPACE + "RUS",
			NAMESPACE + "SMR",
			NAMESPACE + "CHE",
			NAMESPACE + "SRB",
			NAMESPACE + "TUR",
			NAMESPACE + "UKR",
			NAMESPACE + "VAT",
			NAMESPACE + "BYS"
	};
	
	private static final String[] ALPHA2 = {
			// European Union
			"BE",
			"BG",
			"DK",
			"DE",
			"EE",
			"FI",
			"FR",
			"GR",
			"IE",
			"IT",
			"HR",
			"LV",
			"LT",
			"LU",
			"MT",
			"NL",
			"AT",
			"PL",
			"PT",
			"RO",
			"SE",
			"SK",
			"SI",
			"ES",
			"CZ",
			"HU",
			"GB",
			"CY",
			// additional European countries
			"AL",
			"AD",
			"AZ",
			"BA",
			"GE",
			"IS",
			"KZ",
			"LI",
			"MK",
			"MD",
			"MC",
			"ME",
			"NO",
			"RU",
			"SM",
			"CH",
			"RS",
			"TR",
			"UA",
			"VA",
			"BY"
	};
	
	private Model countries;
	
	private Set<Resource> euCountries;
	
	private Map<String, String> alpha2Codes;

	@PostConstruct
	public void init() {
		countries = ModelFactory.createDefaultModel();
//		countries.read("http://publications.europa.eu/mdr/resource/authority/country/skos/countries-skos.rdf");
		countries.read("http://publications.europa.eu/resource/distribution/country/20181212-0/rdf/skos_core/countries-skos.rdf");

		alpha2Codes = new HashMap<>();
		int i = 0;
		
		euCountries = new HashSet<>();
		for (String country : EUROPE) {
			euCountries.add(countries.getResource(country));
			alpha2Codes.put(country, ALPHA2[i++]);
		}
	}

	public Map<String, String> getAll(String languageCode) {
		Map<String, String> all = new HashMap<>();
		
		ResIterator it = countries.listResourcesWithProperty(RDF.type, SKOS.Concept);
		while (it.hasNext()) {
			Resource country = it.next();
			all.put(country.getProperty(SKOS.prefLabel, languageCode).getString(), country.getURI());
		}
		return all;
	}

	public Map<String, String> getEurope(String languageCode) {
		return euCountries.stream().collect(Collectors.toMap(c -> c.getProperty(SKOS.prefLabel, languageCode).getString(), Resource::getURI));
	}

	public String asAlpha2Code(String countryRef) {
		return alpha2Codes.get(countryRef);
	}

	public String prefLabel(String ref, String language) {
		return countries.getResource(ref).getProperty(SKOS.prefLabel, language).getString();
	}

}
