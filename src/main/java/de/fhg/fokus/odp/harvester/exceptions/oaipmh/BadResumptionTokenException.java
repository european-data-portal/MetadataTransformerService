package de.fhg.fokus.odp.harvester.exceptions.oaipmh;

import de.fhg.fokus.odp.harvester.exceptions.ProtocolException;

public class BadResumptionTokenException extends ProtocolException {
    
    /**
	 * 
	 */
	private static final long serialVersionUID = -851668019759410263L;
	
	private String message = "The value of the resumptionToken argument is invalid or expired.";

    public BadResumptionTokenException() {
        super();    
    }
    
      public BadResumptionTokenException(String message) {
        super(message);
        this.message = message;
    }
    
    public BadResumptionTokenException(Throwable throwable) {
        super(throwable);
        message = throwable.getMessage();
    }

  
    public BadResumptionTokenException(String message, Throwable throwable) {
        super(message, throwable);
        this.message = message;
    }
    
  
    public String getMessage() {
        return message;
    }
    
   
    public void setMessage(String message) {
        this.message = message;
    }

	
}
