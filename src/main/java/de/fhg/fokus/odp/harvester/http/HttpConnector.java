package de.fhg.fokus.odp.harvester.http;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import de.fhg.fokus.odp.harvester.exceptions.GeneralHttpException;
import de.fhg.fokus.odp.harvester.exceptions.ProtocolException;
import de.fhg.fokus.odp.harvester.persistence.enums.RepositoryType;
import org.apache.http.Header;
import org.apache.http.HttpHost;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.conn.routing.HttpRoute;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.message.BasicHeader;
import org.apache.http.util.EntityUtils;
import org.jdom2.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class HttpConnector<T> {

	protected final Logger log = LoggerFactory.getLogger(getClass());

	private final String host;
	private Header[] headers;

	private final CloseableHttpClient httpclient;
	
	public static <T> HttpConnector<T> createConnector(String host, Header[] headers, Class<T> clazz) {
		if (clazz == Document.class)
			return (HttpConnector<T>) new HttpConnectorXmlImpl(host, headers);
		else if (clazz == ObjectNode.class) {
			return (HttpConnector<T>) new HttpConnectorJsonImpl(host, headers);
		} else {
			throw new IllegalArgumentException("class binding not supported");
		}
	}
	
	protected HttpConnector(String host, Header[] headers) {
		PoolingHttpClientConnectionManager cm = new PoolingHttpClientConnectionManager();
		cm.setMaxTotal(200);
		cm.setDefaultMaxPerRoute(20);
		HttpHost localhost = new HttpHost(host);
		cm.setMaxPerRoute(new HttpRoute(localhost), 50);

		httpclient = HttpClients.custom().setConnectionManager(cm).setSSLHostnameVerifier(NoopHostnameVerifier.INSTANCE).build();

		this.host = host;
		this.headers = headers;
	}

	protected abstract T readContent(InputStream content, Charset charset);

	public T get(String path) throws IOException, ProtocolException {
		return get(path, null);
	}

	public T get(String path, RequestConfig params) throws IOException, ProtocolException {
		try {
			HttpGet get = new HttpGet(host + path);
			if (params != null) {
				get.setConfig(params);
			}
			return execute(get);
		} catch (IllegalArgumentException e) {
			throw new ProtocolException("URL invalid: " + host + path, e);
		}
	}
	
	public T post(String path, String content) throws IOException, ProtocolException {
		HttpPost request;
		try {
			request = new HttpPost(host + path);
		} catch (IllegalArgumentException e) {
			throw new ProtocolException("URL invalid: " + host + path, e);
		}
		
		// is this required if we already set this in StringEntity?
		request.setHeader("Content-Type", "application/json; charset=utf-8");

		StringEntity input = new StringEntity(content, Charset.forName("utf-8"));
		input.setContentType("application/json");
		input.setContentEncoding("utf-8");

		request.setEntity(input);
		return execute(request);
	}
	
	private T execute(HttpUriRequest request) throws IOException {
		if (headers != null) {
			request.setHeaders(headers);
		}

		return httpclient.execute(request, response -> {
			StatusLine statusLine = response.getStatusLine();
			log.trace("Status Code {} during request to {}.", statusLine.getStatusCode(), request.getRequestLine());

			if (statusLine.getStatusCode() >= 300) {
				throw new GeneralHttpException(statusLine.getStatusCode(), statusLine.getReasonPhrase(), EntityUtils.toString(response.getEntity()));
			}

			if (response.getEntity() == null) {
				throw new ClientProtocolException("Response contains no content");
			}

			return readContent(response.getEntity().getContent(), ContentType.getOrDefault(response.getEntity()).getCharset());
		});
	}
	
	public void close() {
		try {
			httpclient.close();
		} catch (IOException e) {
			log.error("closing http client", e);
		}
	}

}

