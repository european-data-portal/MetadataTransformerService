package de.fhg.fokus.odp.harvester.sections;

import de.fhg.fokus.odp.harvester.persistence.enums.LogEnums;
import de.fhg.fokus.odp.harvester.persistence.enums.ServiceStage;
import de.fhg.fokus.odp.harvester.service.RunHandler;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

public abstract class QueuedJobSection<T> extends JobSection {

    private BlockingQueue<T> queue;

    private final AtomicInteger putCounter = new AtomicInteger();
    private final AtomicInteger takeCounter = new AtomicInteger();

    public void finish() throws InterruptedException {
		queue.put(poisonObject());
    }

    public boolean init(RunHandler run, JobSection attached, int capacity) {
		if (!super.init(run, attached)) {
			return false;
		}

		queue = capacity > 0 ? new LinkedBlockingQueue<>(capacity) : new LinkedBlockingQueue<>(capacity);

		return true;
	}

    public boolean isFinished(T dataset) {
    	return dataset == poisonObject();
    }
    
    protected abstract T poisonObject();
    
	public T take() throws InterruptedException {
		T dataset = queue.take();
		if (dataset != poisonObject()) {
			takeCounter.incrementAndGet();
		}
		return dataset;
	}
	
	public void put(T dataset) throws InterruptedException {
		putCounter.incrementAndGet();
		queue.put(dataset);
	}

	public int takeCounter() {
		return takeCounter.get();
	}
	
	public int putCounter() {
		return putCounter.get();
	}

	protected void failed(String message, Exception ex, ServiceStage stage) {
		try {
			finish();
		} catch (InterruptedException e) {
			log.warn("Thread interrupted", e);
			run.addLogWarning("Section interrupted.", stage, LogEnums.Category.system);
		}
		super.failed(message, ex, stage);
	}

}
