package de.fhg.fokus.odp.harvester.exceptions.oaipmh;

import de.fhg.fokus.odp.harvester.exceptions.ProtocolException;

public class NoMetadataFormatsException extends ProtocolException {
    
    /**
	 * 
	 */
	private static final long serialVersionUID = -1283952754526734238L;
	
	private String message = "There are no metadata formats available for the specified item.";

    public NoMetadataFormatsException() {
        super();    
    }
    
      public NoMetadataFormatsException(String message) {
        super(message);
        this.message = message;
    }
    
    public NoMetadataFormatsException(Throwable throwable) {
        super(throwable);
        message = throwable.getMessage();
    }

  
    public NoMetadataFormatsException(String message, Throwable throwable) {
        super(message, throwable);
        this.message = message;
    }
    
  
    public String getMessage() {
        return message;
    }
    
   
    public void setMessage(String message) {
        this.message = message;
    }

	
}
