package de.fhg.fokus.odp.harvester.util;

import org.apache.commons.lang3.StringUtils;
import org.apache.jena.sparql.vocabulary.FOAF;
import org.apache.jena.vocabulary.DCTerms;

/**
 * Created by sim on 14.06.2017.
 */
public class EDP {
    static String NAMESPACE = "http://europeandataportal.eu/";
    static String NAMESPACE_DATA = NAMESPACE + "set/data/";
    static String NAMESPACE_RECORD = NAMESPACE + "set/record/";
    static String NAMESPACE_DISTRIBUTION = NAMESPACE + "set/distribution/";

    static String createDatasetURIref(String id) {
        return NAMESPACE + "set/data/" + id;
    }

    static String createDistributionURIref(String id) {
        return NAMESPACE + "set/data/" + id;
    }

    static String createCatalogRecordURIref(String id) {
        return NAMESPACE + "set/record/" + id;
    }

    static String extractId(String uriRef) {
        return StringUtils.substringAfterLast(uriRef, "/");
    }

}
