package de.fhg.fokus.odp.harvester.util;

import org.apache.jena.datatypes.xsd.XSDDatatype;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.sparql.vocabulary.FOAF;
import org.apache.jena.vocabulary.DCAT;
import org.apache.jena.vocabulary.DCTerms;
import org.apache.jena.vocabulary.RDF;

import java.util.Date;

/**
 * Created by sim on 14.06.2017.
 */
public class DCATAPUtils {

    public static final Resource deleted;
    public static final Resource modified;
    public static final Resource created;

    static {
        Model m = ModelFactory.createDefaultModel();
        deleted = m.createResource(":deleted");
        modified = m.createResource(":modified");
        created = m.createResource(":created");
    }

    static Resource createDataset(String uriref, Model model) {
        return model.createResource(uriref, DCAT.Dataset);
    }

    static Resource addTitle(Resource resource, String title, String lang) {
        if (isCatalogRecord(resource) || isDataset(resource)) {
            resource.addLiteral(DCTerms.title, resource.getModel().createLiteral(title, lang));
        }
        return resource;
    }

    static Resource addTitle(Resource resource, String title) {
        if (isCatalogRecord(resource) || isDataset(resource)) {
            resource.addLiteral(DCTerms.title, resource.getModel().createLiteral(title));
        }
        return resource;
    }

    static Resource addDescription(Resource resource, String description, String lang) {
        if (isCatalogRecord(resource) || isDataset(resource)) {
            resource.addLiteral(DCTerms.description, resource.getModel().createLiteral(description, lang));
        }
        return resource;
    }

    static Resource addDescription(Resource resource, String description) {
        if (isCatalogRecord(resource) || isDataset(resource)) {
            resource.addLiteral(DCTerms.description, resource.getModel().createLiteral(description));
        }
        return resource;
    }

    static Resource createDistribution(String id) {
        return ModelFactory.createDefaultModel().createResource(EDP.createDistributionURIref(id), DCAT.Distribution);
    }

    static Resource addAccessURL(Resource resource, String accessURL) {
        if (isDistribution(resource)) {
            resource.addProperty(DCAT.accessURL, resource.getModel().createResource(accessURL));
        }
        return resource;
    }

    static Resource createCatalogRecord(Resource resource) {
        if (!isDataset(resource)) {
            return null;
        }
        Model m = resource.getModel();

        String id = EDP.extractId(resource.getURI());
        Resource catalogRecord = m.createResource(EDP.createCatalogRecordURIref(id), DCAT.CatalogRecord);

        catalogRecord.addProperty(FOAF.primaryTopic, resource);
        catalogRecord.addProperty(DCTerms.issued, m.createTypedLiteral(new Date(), XSDDatatype.XSDdateTime));
        catalogRecord.addProperty(DCTerms.modified, m.createTypedLiteral(new Date(), XSDDatatype.XSDdateTime));
        catalogRecord.addProperty(DCTerms.conformsTo, DCAT.NAMESPACE);
        catalogRecord.addProperty(ADMS.status, created);

        return catalogRecord;
    }

    static Resource setModified(Resource resource, Date date) {
        if (isCatalogRecord(resource) || isDataset(resource)) {
            resource.removeAll(DCTerms.modified);
            resource.addProperty(DCTerms.modified, resource.getModel().createTypedLiteral(date, XSDDatatype.XSDdateTime));
        }
        return resource;
    }

    static Resource setStatusDeleted(Resource resource) {
        if (isCatalogRecord(resource)) {
            resource.removeAll(ADMS.status);
            resource.addProperty(ADMS.status, deleted);
        }
        return resource;
    }

    static Resource setStatusModified(Resource resource) {
        if (isCatalogRecord(resource)) {
            resource.removeAll(ADMS.status);
            resource.addProperty(ADMS.status, modified);
        }
        return resource;
    }

    static boolean isCatalogRecord(Resource resource) {
        return isType(resource, DCAT.CatalogRecord);
    }

    static boolean isDataset(Resource resource) {
        return isType(resource, DCAT.Dataset);
    }

    static boolean isDistribution(Resource resource) {
        return isType(resource, DCAT.Distribution);
    }

    static boolean isType(Resource resource, Resource type) {
        return resource.getPropertyResourceValue(RDF.type).equals(type);
    }

}
