package de.fhg.fokus.odp.harvester.persistence;

import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import javax.transaction.Transactional;

import de.fhg.fokus.odp.harvester.persistence.entities.Harvester;
import de.fhg.fokus.odp.harvester.persistence.entities.Run;
import de.fhg.fokus.odp.harvester.persistence.enums.RunState;

@ApplicationScoped
public class RunsManager {

	@PersistenceContext
	private EntityManager em;

	public Run find(Integer id) {
		return em.find(Run.class, id);
	}
	
	@Transactional
	public void persist(Run run) {
		em.persist(run);
	}
	
	@Transactional
	public void remove(Run run) {
		em.remove(em.contains(run) ? run : em.merge(run));
	}
		
	@Transactional
	public Run update(Run run) {
		return em.merge(run);
	}
	
	public void refresh(Run run) {
		if (!em.contains(run)) {
			run = em.merge(run);
		}
		em.refresh(run);
	}

	@Transactional
	public boolean existRuns(Integer harvesterId) {
		return (Boolean) em.createNativeQuery("select exists (select r from run r where r.harvester_id = ?1)")
				.setParameter(1, harvesterId).getSingleResult();
	}

	@Transactional
	public List<Run> listRuns(Integer harvesterId, Integer rows, Integer start) {
		return paging(em.createNamedQuery("runs_by_harvester_id", Run.class).setParameter("id", harvesterId), rows, start).getResultList();
	}

	public int countRuns(Integer harvesterId) {
		return em.createNamedQuery("count_runs_by_harvester_id", Long.class).setParameter("id", harvesterId).getSingleResult().intValue();
	}

	@Transactional
	public List<Run> getMostRecentRuns(Integer harvesterId) {
		return em.createNamedQuery("runs_by_harvester_id", Run.class).setParameter("id", harvesterId).setMaxResults(5).getResultList();
	}

	@Transactional
	public int countRunsByState(Integer id, RunState runState) {
		return em.createNamedQuery("count_runs_with_status", Long.class).setParameter("id", id).setParameter("runstatus", runState).getSingleResult().intValue();
	}

	@Transactional
	public int countCompletedRuns(Integer id) {
		return em.createNamedQuery("count_completed_runs", Long.class).setParameter("id", id).getSingleResult().intValue();
	}

	@Transactional
	public int getProcessingCount(Run run) {
		List<?> numbers = em.createQuery("select r.numberToProcess from Run r where r = :run").setParameter("run", run).getResultList();
		return numbers.isEmpty() ? 0 : ((Number) numbers.get(0)).intValue();
	}

	@Transactional
	public String getMeanRunDurationInMinutes(Integer harvesterId) {
		List<?> result = em.createNativeQuery("select avg(date_part('day', run.endTime - run.startTime) * 24 + date_part('hour', run.endTime - run.startTime) * 60 + date_part('minute', run.endTime - run.startTime))  from run where run.harvester_id = ?1 and run.runState = 'finished' and endTime is not null")
				.setParameter(1, harvesterId).getResultList();

		return result == null || result.isEmpty() || result.size() > 1 || result.get(0) == null ? null : String.valueOf(Math.round((Double) result.get(0)));
	}

	@Transactional
	public List<Run> getRunsByStates(Harvester harvester, List<RunState> statuses, Boolean groupByRunState, Integer rows, Integer start) {

		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Run> cq = cb.createQuery(Run.class);
		Root<Run> run = cq.from(Run.class);

		// Select runs with specific run states only
		List<Predicate> predicates = new ArrayList<>();
		statuses.forEach(status -> predicates.add(cb.equal(run.get("runState"), status)));

		//Select runs of harvester with previously defined run state criteria
		cq.select(run).where(cb.and(cb.equal(run.get("harvester"), harvester), cb.or(predicates.toArray(new Predicate[predicates.size()]))));

		//Show latest runs on top
		if (groupByRunState) {
			cq.orderBy(cb.asc(run.get("runState")), cb.desc(run.get("startTime")));
		} else {
			cq.orderBy(cb.desc(run.get("startTime")));
		}

		return paging(em.createQuery(cq), rows, start).getResultList();
	}

	public int getAddedCount(Run run) {
		List<?> numbers = em.createQuery("select r.numberAdded from Run r where r = :run").setParameter("run", run).getResultList();
		return numbers.isEmpty() ? 0 : ((Number) numbers.get(0)).intValue();
	}

	public int getUpdatedCount(Run run) {
		List<?> numbers = em.createQuery("select r.numberUpdated from Run r where r = :run").setParameter("run", run).getResultList();
		return numbers.isEmpty() ? 0 : ((Number) numbers.get(0)).intValue();
	}

	public int getSkippedCount(Run run) {
		List<?> numbers = em.createQuery("select r.numberSkipped from Run r where r = :run").setParameter("run", run).getResultList();
		return numbers.isEmpty() ? 0 : ((Number) numbers.get(0)).intValue();
	}

	public int getRejectedCount(Run run) {
		List<?> numbers = em.createQuery("select r.numberRejected from Run r where r = :run").setParameter("run", run).getResultList();
		return numbers.isEmpty() ? 0 : ((Number) numbers.get(0)).intValue();
	}

	private <T> TypedQuery<T> paging(TypedQuery<T> query, Integer rows, Integer start) {
		if (rows != null) {
			query.setMaxResults(rows);
		}
		if (start != null) {
			query.setFirstResult(start);
		}
		return query;
	}
}
