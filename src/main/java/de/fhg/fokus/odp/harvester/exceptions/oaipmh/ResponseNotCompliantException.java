package de.fhg.fokus.odp.harvester.exceptions.oaipmh;

import de.fhg.fokus.odp.harvester.exceptions.ProtocolException;

/**
 * Created by jpi on 15.04.2015.
 */
public class ResponseNotCompliantException extends ProtocolException {

    /**
	 * 
	 */
	private static final long serialVersionUID = -2179849306321365087L;
	
	private String message = "The response is not compliant with the OAI-PMH specification.";

    public ResponseNotCompliantException() {
        super();
    }

    public ResponseNotCompliantException(String message) {
        super(message);
        this.message = message;
    }

    public ResponseNotCompliantException(Throwable throwable) {
        super(throwable);
        message = throwable.getMessage();
    }


    public ResponseNotCompliantException(String message, Throwable throwable) {
        super(message, throwable);
        this.message = message;
    }


    public String getMessage() {
        return message;
    }


    public void setMessage(String message) {
        this.message = message;
    }

}
