package de.fhg.fokus.odp.harvester.http.udata;

import com.fasterxml.jackson.databind.JsonNode;

import de.fhg.fokus.odp.harvester.http.HttpResponse;

public class UDataResponse extends HttpResponse<JsonNode> {

	public UDataResponse(JsonNode content) {
		super(content);
	}

	@Override
	public boolean isError() {
		return content == null || content.path("data").isMissingNode();
	}

	@Override
	public boolean isSuccess() {
		return !isError();
	}

	@Override
	public UDataError getError() {
		return isError() ? new UDataError(content.path("error")) : null;
	}

	@Override
	public UDataResult getResult() {
		return isSuccess() ? new UDataResult(content) : null;
	}
	
}
