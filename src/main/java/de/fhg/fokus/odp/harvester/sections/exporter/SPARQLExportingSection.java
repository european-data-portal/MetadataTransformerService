package de.fhg.fokus.odp.harvester.sections.exporter;

import de.fhg.fokus.odp.harvester.persistence.enums.LogEnums;
import de.fhg.fokus.odp.harvester.persistence.enums.ServiceStage;
import de.fhg.fokus.odp.harvester.sections.JobSection;
import de.fhg.fokus.odp.harvester.sections.QueuedJobSection;
import de.fhg.fokus.odp.harvester.service.RunHandler;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ResIterator;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdfconnection.RDFConnection;
import org.apache.jena.rdfconnection.RDFConnectionFactory;
import org.apache.jena.util.ResourceUtils;
import org.apache.jena.vocabulary.DCAT;
import org.apache.jena.vocabulary.RDF;

import javax.enterprise.context.Dependent;
import java.io.IOException;

/**
 * Created by sim on 02.01.2017.
 */
@Dependent
public class SPARQLExportingSection extends JobSection {

    private CloseableHttpClient httpClient;

    private UriRefResolver resolver = new UriRefResolver();

    public boolean init(RunHandler run, JobSection section) {
        if (!super.init(run, section)) {
            return false;
        }

        UsernamePasswordCredentials creds = new UsernamePasswordCredentials("dba", "dba");
        CredentialsProvider credsProvider = new BasicCredentialsProvider();
        credsProvider.setCredentials(AuthScope.ANY, creds);
        httpClient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();

        return true;
    }

    public Void call() {
        log.info("Starting SPARQL exporting section.");

        QueuedJobSection<Model> queuedSection = (QueuedJobSection<Model>) attached;

        try {
            Model model = queuedSection.take();
            while (!queuedSection.isFinished(model)) {

                ResIterator it = model.listResourcesWithProperty(RDF.type, DCAT.Dataset);
                while (it.hasNext()) {
                    Resource res = it.nextResource();
                    if (res.isURIResource()) { // for now only accept datasets with a uri ref

                        String uriref = res.getURI();
                        // May be we could assume that everything after the last '/' is a identifier
                        String identifier = res.getURI().substring(uriref.lastIndexOf("/") + 1);
                        // 1. create new local URIref for dataset
                        // Should we use identical URIref for named graph?
                        res = ResourceUtils.renameResource(res, resolver.getDatasetUriRef(identifier));

                        // Do we need to enrich it with additional values?

                        // 3. create catalog record

                        // 4. create link to catalogue

                        // Dataset dataset = DatasetFactory.create();
                        // dataset.addNamedModel(name, model);

                        try (RDFConnection connection = RDFConnectionFactory.connect(run.getHarvester().getTarget().getUrl())) {
                            connection.put(res.getURI(), model);
                        }

                        // Iterator<Quad> iterator = dataset.asDatasetGraph().find(NodeFactory.createURI(name), Node.ANY, Node.ANY, Node.ANY);
                        // List<Quad> list = new ArrayList<>();
                        // iterator.forEachRemaining(list::add);

                        // UpdateDataInsert update = new UpdateDataInsert(new QuadDataAcc(list));
                        // UpdateProcessRemote processor = (UpdateProcessRemote)UpdateExecutionFactory.createRemote(update, run.getHarvester().getTarget().getUrl(), httpClient);

                        // processor.execute();

                        log.debug("Exported ({}) {}", queuedSection.takeCounter(), res.getURI());
                    }
                }

                run.incrementAdded();

                model = queuedSection.take();
            }

            // queue in again for all other threads working on this queue
            queuedSection.finish();

            log.info("Finished SPARQL exporting section for {}.", run.getHarvester().getName());
            run.addLogInfo("Finished SPARQL exporting section. " + queuedSection.takeCounter() + " datasets processed.", ServiceStage.export_stage, LogEnums.Category.system);

        } catch(InterruptedException e) {
            log.warn("Export thread interrupted", e);
            run.addLogWarning("Export section interrupted.", ServiceStage.export_stage, LogEnums.Category.system);
        }
        return null;
    }

    public void close() {
        if (httpClient != null) {
            try {
                httpClient.close();
            } catch (IOException e) {
                log.error("closing http connection", e);
            }
        }
    }

    private class UriRefResolver {

        String base = "http://europeandataportal.eu";
        String datasetContext = "/set/data/";
        String recordContext = "/set/record/";

        UriRefResolver() {
        }

        UriRefResolver(String base) {
            this.base = base;
        }

        String getDatasetUriRef(String name){
            return base + datasetContext + name;
        }

        String getRecordUriRef(String name){
            return base + recordContext + name;
        }

    }

}
