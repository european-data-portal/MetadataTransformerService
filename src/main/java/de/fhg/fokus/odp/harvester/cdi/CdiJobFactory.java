package de.fhg.fokus.odp.harvester.cdi;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Any;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;

import org.quartz.Job;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.simpl.SimpleJobFactory;
import org.quartz.spi.TriggerFiredBundle;

@ApplicationScoped
public class CdiJobFactory extends SimpleJobFactory {
 
//    @Inject
//    private BeanManager beanManager;
//
//    @Override
//    public Job newJob(TriggerFiredBundle triggerFiredBundle, Scheduler scheduler) throws SchedulerException {
//    	Job job = super.newJob(triggerFiredBundle, scheduler);
//        Class<? extends Job> clazz = job.getClass();
//
//        if (beanManager != null) {
//            CreationalContext<Job> ctx = beanManager.createCreationalContext(null);
//            @SuppressWarnings("unchecked")
//            AnnotatedType<Job> type = (AnnotatedType<Job>) beanManager.createAnnotatedType(clazz);
//            InjectionTarget<Job> it = beanManager.createInjectionTarget(type);
//            it.inject(job, ctx);
//        }
//        
//        return job;
//    }
    
    @Inject
    @Any
    private Instance<Job> jobs;
 
    @Override
    public Job newJob(TriggerFiredBundle triggerFiredBundle, Scheduler scheduler) throws SchedulerException {
        final JobDetail jobDetail = triggerFiredBundle.getJobDetail();
        final Class<? extends Job> jobClass = jobDetail.getJobClass();
 
        for (Job job : jobs) {
            if (job.getClass().isAssignableFrom(jobClass)) {
                return job;
            }
        }
 
        throw new RuntimeException("Cannot create a Job of type " + jobClass);
    }
}