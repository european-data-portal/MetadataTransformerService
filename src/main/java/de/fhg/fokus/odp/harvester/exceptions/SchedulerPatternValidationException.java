package de.fhg.fokus.odp.harvester.exceptions;

public class SchedulerPatternValidationException extends Exception{
	 /**
     * Serialization version.
     */
    private static final long serialVersionUID = 1L;

    public SchedulerPatternValidationException() {
    }

    public SchedulerPatternValidationException(String message) {
        super(message);
    }

    public SchedulerPatternValidationException(Throwable cause) {
        super(cause);
    }

    public SchedulerPatternValidationException(String message, Throwable cause) {
        super(message, cause);
    }

}
