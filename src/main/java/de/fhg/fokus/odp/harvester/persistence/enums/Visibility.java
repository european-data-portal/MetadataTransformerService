package de.fhg.fokus.odp.harvester.persistence.enums;

public enum Visibility {
	private_visibility("Private"), public_visibility("Public");

	private final String label;

	Visibility(String label) {
		this.label = label;
	}

	public String getLabel() {
		return this.label;
	}
}