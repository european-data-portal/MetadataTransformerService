package de.fhg.fokus.odp.harvester.persistence;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.picketlink.idm.model.basic.User;

import de.fhg.fokus.odp.harvester.persistence.entities.Activity;
import de.fhg.fokus.odp.harvester.persistence.entities.Harvester;
import de.fhg.fokus.odp.harvester.persistence.entities.Repository;
import de.fhg.fokus.odp.harvester.persistence.enums.ActivityType;

@ApplicationScoped
public class ActivitiesManager {

	@PersistenceContext
	private EntityManager em;
	
	@Transactional
	public void persist(Activity activity) {
		em.persist(activity);
	}
	
	public List<Activity> list(ActivityType type) {
		return em.createNamedQuery("activities_of_type", Activity.class).setParameter("type", type).getResultList();
	}

	@Transactional
	public void remove(Activity activity) {
		em.remove(em.contains(activity) ? activity : em.merge(activity));
	}
		
	@Transactional
	public Activity update(Activity activity) {
		return em.merge(activity);
	}
	
	public Activity find(Integer id) {
		return em.find(Activity.class, id);
	}

	public List<Activity> listUserActivities(User user) {
		return em.createNamedQuery("activities_from_user", Activity.class).setParameter("owner", user.getId()).getResultList();		
	}
	
	public List<Activity> listHarvesterActivities(Harvester harvester) {
		return em.createNamedQuery("activities_from_harvester", Activity.class).setParameter("harvester", harvester).getResultList();		
	}
	
	public List<Activity> listRepositoriesActivities(Repository repository) {
		return em.createNamedQuery("activities_from_repository", Activity.class).setParameter("respository", repository).getResultList();		
	}
	
	@Transactional
	public void userCreated(User user, String message) {
		Activity activity = new Activity();
		activity.setActivityType(ActivityType.user_create);
		// activity.setEntityId(user.getId());
		activity.setEntityName(user.getLoginName());
		activity.setOwner(user.getId());
		if (message != null && !message.isEmpty()) {
			activity.setMessage(message);			
		} else {
			activity.setMessage("User " + user.getLoginName() + " created and registered.");						
		}

		persist(activity);		
	}
	
	@Transactional
	public void userUpdated(User user, String message) {
		Activity activity = new Activity();
		activity.setActivityType(ActivityType.user_update);
		// activity.setEntityId(user.getId());
		activity.setEntityName(user.getLoginName());
		activity.setOwner(user.getId());
		if (message != null && !message.isEmpty()) {
			activity.setMessage(message);			
		} else {
			activity.setMessage("User " + user.getLoginName() + " created and registered.");						
		}

		persist(activity);		
	}
	
}
