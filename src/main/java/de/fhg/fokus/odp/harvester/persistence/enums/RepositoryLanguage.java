package de.fhg.fokus.odp.harvester.persistence.enums;

import java.util.Locale;

import javax.faces.context.FacesContext;

public enum RepositoryLanguage {

    BULGARIAN("bg"),
    CROATIAN("hr"),
    CZECH("cs"),
    DANISH("da"),
    DUTCH("nl"),
    ENGLISH("en"),
    ESTONIAN("et"),
    FINNISH("fi"),
    FRENCH("fr"),
    GERMAN("de"),
    GREEK("el"),
    HUNGARIAN("hu"),
    ICELANDIC("is"),
    IRISH("ga"),
    ITALIAN("it"),
    LATVIAN("lv"),
    LITHUANIAN("lt"),
    MALTESE("mt"),
    POLISH("pl"),
    PORTUGUESE("pt"),
    ROMANIAN("ro"),
    SLOVAK("sk"),
    SLOVENE("sl"),
    SPANISH("es"),
    SWEDISH("sv"),
    // non eu
    NORWEGIAN("no"),
    SERBIAN("sr"),
    UKRAINIAN("uk")
    ;

    private final String languageKey;
    RepositoryLanguage(String languageKey) {
        this.languageKey = languageKey;
    }

    public String toString() {
        return "language." + languageKey;
    }

    public String getLanguageKey() {
        return languageKey;
    }
    
    public String getDisplayName() {
    	if (languageKey.isEmpty()) {
    		return "Undefined";
    	} else {
    		return new Locale(languageKey).getDisplayLanguage(FacesContext.getCurrentInstance().getViewRoot().getLocale());
    	}
    }

}