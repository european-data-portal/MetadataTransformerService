package de.fhg.fokus.odp.harvester.exceptions;

/**
 * Created by jpi on 15.04.2015.
 */
public class SectionFailedException extends Exception {

	public SectionFailedException() {}

    public SectionFailedException(String message) {
        super(message);
    }

    public SectionFailedException(Throwable throwable) {
        super(throwable);
    }


    public SectionFailedException(String message, Throwable throwable) {
        super(message, throwable);
    }

}
