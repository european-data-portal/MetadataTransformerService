package de.fhg.fokus.odp.harvester.sections;

import de.fhg.fokus.odp.harvester.persistence.enums.RunState;
import de.fhg.fokus.odp.harvester.persistence.enums.ServiceStage;
import de.fhg.fokus.odp.harvester.service.RunHandler;
import de.fhg.fokus.odp.harvester.util.EMailService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.util.concurrent.Callable;

public abstract class JobSection implements Callable<Void> {
	
	protected final Logger log = LoggerFactory.getLogger(getClass());
	
	protected RunHandler run;

	protected JobSection attached;

	@Inject
	private EMailService emailService;

	public boolean init(RunHandler run, JobSection attached) {
		this.run = run;
		this.attached = attached;
		return true;
	}

	public void close() {

	}

	protected void failed(String message, Exception e, ServiceStage stage) {
		run.setState(RunState.error);

		if (run.getHarvester().getEmailNotification()) {
			emailService.sendError(run.getRun(), e, stage);
		}
	}

}
