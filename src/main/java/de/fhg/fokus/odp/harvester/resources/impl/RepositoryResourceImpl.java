package de.fhg.fokus.odp.harvester.resources.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;

import de.fhg.fokus.odp.harvester.application.InspireAdapter;
import de.fhg.fokus.odp.harvester.application.InspireHarvesterInfo;
import de.fhg.fokus.odp.harvester.persistence.RepositoriesManager;
import de.fhg.fokus.odp.harvester.persistence.entities.Harvester;
import de.fhg.fokus.odp.harvester.persistence.entities.Repository;
import de.fhg.fokus.odp.harvester.persistence.enums.RepositoryType;
import de.fhg.fokus.odp.harvester.resources.RepositoryJson;
import de.fhg.fokus.odp.harvester.resources.RepositoryResource;
import de.fhg.fokus.odp.harvester.resources.ResponseJson;

import java.io.Serializable;
import java.util.Optional;
import java.util.stream.Collectors;

@SessionScoped
public class RepositoryResourceImpl implements Serializable, RepositoryResource {

	/**
	 * 
	 */
	private static final long serialVersionUID = 500997718808575150L;

	@Inject
	private RepositoriesManager repositoriesManager;

	@Inject
	private InspireAdapter inspireAdapter;

	@Override
	@Transactional
	public ResponseJson listRepositories(Integer rows, Integer start, String search) {
	    inspireAdapter.fetchHarvesters();
		List<Repository> repositories = search != null ? repositoriesManager.listRepositories(rows, start, search, new ArrayList<>(Arrays.asList(RepositoryType.values()))) : repositoriesManager.list(rows, start);
		List<RepositoryJson> repositoryList = repositories.stream().map(this::convertToJson).collect(Collectors.toList());
		long total = search != null ? repositoriesManager.countRepositories(search, new ArrayList<>(Arrays.asList(RepositoryType.values()))) : repositoriesManager.count();
		return ResponseJson.createSuccess(repositoryList, total, rows, start);
	}

	@Override
	@Transactional
	public ResponseJson getRepository(String id) {
        inspireAdapter.fetchHarvesters();
		Repository repository = repositoriesManager.find(Integer.parseInt(id));
		return ResponseJson.createSuccess(convertToJson(repository), null, null, null);
	}

	private RepositoryJson convertToJson(Repository repository) {
		RepositoryJson repositoryJson = new RepositoryJson();
		repositoryJson.setId(repository.getId());
		repositoryJson.setName(repository.getName());
		repositoryJson.setDescription(repository.getDescription());
		repositoryJson.setUrl(repository.getUrl());
		repositoryJson.setHomepage(repository.getHomepage());
		repositoryJson.setLanguage(repository.getLanguage().getLanguageKey());
		repositoryJson.setType(repository.getRepositoryType());
		if (repositoryJson.getType() == RepositoryType.dcatap && inspireAdapter.getHarvesters() != null && !inspireAdapter.getHarvesters().isEmpty()) {
			Optional<InspireHarvesterInfo> info = inspireAdapter.getHarvesters().stream().filter(i -> i.getEndpoint().equals(repository.getUrl())).findFirst();
			info.ifPresent(i -> repositoryJson.setOriginUrl(i.getUrl()));
		}
		repositoryJson.setVisibility(repository.getVisibility());
		repositoryJson.setIncremental(repository.isIncremental());
		repositoryJson.setPublisher(repository.getPublisher());
		repositoryJson.setPublisherEmail(repository.getPublisherEmail());
		repositoryJson.getSpatial().addAll(repository.getSpatial());
		for (Harvester harvester : repository.getHarvesterSource()) {
			repositoryJson.getSourceHarvester().add(harvester.getId());
		}
		for (Harvester harvester : repository.getHarvesterTarget()) {
			repositoryJson.getTargetHarvester().add(harvester.getId());
		}
		
		return repositoryJson;
	}

}
