package de.fhg.fokus.odp.harvester.resources;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

@Path("/repository")
public interface RepositoryResource {

	@GET
	@Path("/")
	@Produces("application/json")
	ResponseJson listRepositories(@QueryParam("rows") Integer rows, @QueryParam("start") Integer start, @QueryParam("search") String search);

	@GET
	@Path("/{id}")
	@Produces("application/json")
	ResponseJson getRepository(@PathParam("id") String id);

}
