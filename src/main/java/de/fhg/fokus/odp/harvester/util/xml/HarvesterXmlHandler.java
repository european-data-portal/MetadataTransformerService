package de.fhg.fokus.odp.harvester.util.xml;

import de.fhg.fokus.odp.harvester.persistence.entities.Filter;
import de.fhg.fokus.odp.harvester.persistence.entities.Harvester;
import de.fhg.fokus.odp.harvester.persistence.entities.Repository;
import de.fhg.fokus.odp.harvester.persistence.enums.HarvesterEnums;
import de.fhg.fokus.odp.harvester.persistence.enums.RepositoryLanguage;
import de.fhg.fokus.odp.harvester.persistence.enums.RepositoryType;
import de.fhg.fokus.odp.harvester.util.TransformationScriptDownloader;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

import javax.enterprise.context.RequestScoped;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.Set;

@RequestScoped
public class HarvesterXmlHandler {

    /**
     * Converts a given XML String to an Object of type Harvester
     *
     * @param harvesterXml
     *          The XML representation
     * @return
     *          The Harvester object
     */
    public Harvester importHarvester(String harvesterXml) {

        try (InputStream xmlStream = new ByteArrayInputStream(harvesterXml.getBytes(StandardCharsets.UTF_8))) {

            SAXBuilder saxBuilder = new SAXBuilder();
            Document doc = saxBuilder.build(xmlStream);
            Element root = doc.getRootElement();

            Harvester harvester = new Harvester();

            //get meta data
            harvester.setName(root.getChildText("Name"));
            harvester.setDescription(root.getChildText("Description"));
            harvester.setHarvestStrategy(HarvesterEnums.HarvestStrategy.valueOf(root.getChildText("Strategy")));

            //get filters
            Set<Filter> filters = new HashSet<>();
            if (!root.getChildren("FilterList").isEmpty()) {
                for (Element filter : root.getChildren("FilterList")) {
                    Filter f = new Filter();
                    f.setAttribute(filter.getChildText("Attribute"));
                    f.setValue(filter.getChildText("Value"));
                    f.setAndOp(Boolean.valueOf(filter.getChildText("AndOp")));
                    f.setNegation(Boolean.valueOf(filter.getChildText("Negation")));
                    f.setIndex(Integer.valueOf(filter.getChildText("Index")));
                    f.setFilterEnabled(Boolean.valueOf(filter.getAttributeValue("enabled")));
                    filters.add(f);
                }
            }
            //add empty set when no filters exist
            harvester.setFilters(filters);

            //transformation script
            Element scriptSource = root.getChild("ScriptSource");
            if (HarvesterEnums.TransformationScriptSource.valueOf(scriptSource.getAttributeValue("type")).equals(HarvesterEnums.TransformationScriptSource.custom)) {
                harvester.setTransformationScriptSource(HarvesterEnums.TransformationScriptSource.custom);
                harvester.setMappingScript(scriptSource.getChildText("MappingScript"));
            } else {
                Element gitRepository = scriptSource.getChild("GitRepository");
                harvester.setTransformationScriptSource(HarvesterEnums.TransformationScriptSource.git);
                harvester.setGitRepoUrl(gitRepository.getChildText("GitUrl"));
                harvester.setGitBranch(gitRepository.getChildText("Branch"));

                //reconstructing full file path
                Path gitfilePath = Paths.get(System.getProperty("jboss.server.temp.dir") + File.separator + "transformation_scripts" + File.separator)
                        .resolve(harvester.getGitRepoUrl().replaceAll("[^\\w\\s]","")).resolve(harvester.getGitBranch()).resolve(gitRepository.getChildText("FileName"));
                harvester.setGitFileName(gitfilePath.toString());
                harvester.setGitRepoRequiresCredentials(Boolean.valueOf(gitRepository.getAttributeValue("requiresCredentials")));
                harvester.setGitRepoSshAuthentication(Boolean.valueOf(gitRepository.getAttributeValue("sshAuthentication")));

                //add fake credentials to ensure a valid harvester
                if (harvester.isGitRepoRequiresCredentials()) {
                    if (!harvester.isGitRepoSshAuthentication())
                        harvester.setGitUserName("sampleUser");

                    harvester.setGitPassWord("samplePassword");
                }
            }

            //repository
            harvester.setSource(deserializeRepository(root.getChild("SourceRepository")));
            harvester.setTarget(deserializeRepository(root.getChild("TargetRepository")));

            return harvester;
        } catch (JDOMException | IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * Converts a given Harvester object to an equivalent XML String.
     * Please note that not all attributes of the harvester are passed on.
     *
     * @param harvester
     *          The harvester to be exported
     * @return
     *          An XML representation of the given harvester
     */
    public String exportHarvester(Harvester harvester) {

        //prepare XML root
        Element harvesterElement = new Element("Harvester");
        Document doc = new Document(harvesterElement);
        Element root = doc.getRootElement();

        //harvester meta data
        root.addContent(new Element("Name").setText(harvester.getName()));
        root.addContent(new Element("Description").setText(harvester.getDescription()));

        root.addContent(new Element("Strategy").setText(harvester.getHarvestStrategy().name()));

        //filters
        if (harvester.getFilters() != null && !harvester.getFilters().isEmpty()) {
            Element allFilters = new Element("FilterList");
            for (Filter f : harvester.getFilters()) {
                Element filter = new Element("Filter");
                filter.setAttribute("enabled", Boolean.toString(f.isFilterEnabled()));
                filter.addContent(new Element("Attribute").setText(f.getAttribute()));
                filter.addContent(new Element("Value").setText(f.getValue()));
                filter.addContent(new Element("AndOp").setText(Boolean.toString(f.getAndOp())));
                filter.addContent(new Element("Negation").setText(Boolean.toString(f.isNegation())));
                filter.addContent(new Element("Index").setText(String.valueOf(f.getIndex())));
                allFilters.addContent(filter);
            }
            root.addContent(allFilters);
        }

        //mapping script
        Element scriptSource = new Element("ScriptSource");
        scriptSource.setAttribute("type", harvester.getTransformationScriptSource().name());
        if (harvester.getTransformationScriptSource() == HarvesterEnums.TransformationScriptSource.custom) {
            scriptSource.addContent(new Element("MappingScript").setText(harvester.getMappingScript()));
        } else {
            TransformationScriptDownloader.GitRepository gitRepo = new TransformationScriptDownloader.GitRepository(
                    harvester.getGitRepoUrl(),
                    harvester.getGitBranch(),
                    harvester.getGitFileName(),
                    harvester.isGitRepoRequiresCredentials(),
                    harvester.isGitRepoSshAuthentication(),
                    harvester.getGitUserName(),
                    harvester.getGitPassWord()
            );
            //check if repo is valid
            if (gitRepo.isValid() || harvester.getGitFileName() == null || harvester.getGitFileName().isEmpty())
                return null;

            Element gitRepository = new Element("GitRepository");
            gitRepository.setAttribute("requiresCredentials", Boolean.toString(harvester.isGitRepoRequiresCredentials()));
            gitRepository.setAttribute("sshAuthentication", Boolean.toString(harvester.isGitRepoSshAuthentication()));
            gitRepository.addContent(new Element("GitUrl").setText(harvester.getGitRepoUrl()));
            gitRepository.addContent(new Element("Branch").setText(harvester.getGitBranch()));
            String fileName = gitRepo.getRepoDirWithBranch().relativize(Paths.get(harvester.getGitFileName())).toString();
            gitRepository.addContent(new Element("FileName").setText(fileName));

            //credentials are omitted for security reasons
            scriptSource.addContent(gitRepository);
        }
        root.addContent(scriptSource);

        //add repositories
        if (harvester.getSource() != null)
            root.addContent(serializeRepository(harvester.getSource(), false));

        if (harvester.getTarget() != null)
            root.addContent(serializeRepository(harvester.getTarget(), true));

        XMLOutputter xmlOutputter = new XMLOutputter();
        xmlOutputter.setFormat(Format.getRawFormat()); //format is required to not break transformation script

        return xmlOutputter.outputString(doc);
    }

    /**
     * Creates a JDOM Element for a given Repository.
     *
     * @param repository
     *          The repository to be converted
     * @param isTarget
     *          Though not part of the Repository bean, this flag is required for assigning the
     *          repository to its harvester properly, i.e. as source or target repository
     * @return
     *          The Repository Element
     */
    private Element serializeRepository(Repository repository, boolean isTarget) {

        Element repo;

        //required for proper assignment while deserializing
        if (!isTarget) {
            repo = new Element("SourceRepository");
        } else {
            repo = new Element("TargetRepository");
        }

        repo.setAttribute("incremental", Boolean.toString(repository.isIncremental()));
        repo.addContent(new Element("Name").setText(repository.getName()));
        repo.addContent(new Element("Description").setText(repository.getDescription()));
        repo.addContent(new Element("Publisher").setText(repository.getPublisher()));
        repo.addContent(new Element("PublisherEmail").setText(repository.getPublisherEmail()));
        repo.addContent(new Element("Homepage").setText(repository.getHomepage()));
        repo.addContent(new Element("Url").setText(repository.getUrl()));
        repo.addContent(new Element("Type").setText(repository.getRepositoryType().name()));
        repo.addContent(new Element("Language").setText(repository.getLanguage().name()));

        return repo;
    }

    private Repository deserializeRepository(Element repo) {
        Repository repository = null;
        if (repo != null) {
            repository = new Repository();
            repository.setName(repo.getChildText("Name"));
            repository.setDescription(repo.getChildText("Description"));
            repository.setPublisher(repo.getChildText("Publisher"));
            repository.setPublisherEmail(repo.getChildText("PublisherEmail"));
            repository.setHomepage(repo.getChildText("Homepage"));
            repository.setUrl(repo.getChildText("Url"));
            repository.setRepositoryType(RepositoryType.valueOf(repo.getChildText("Type")));
            repository.setLanguage(RepositoryLanguage.valueOf(repo.getChildText("Language")));
        }
        return repository;
    }
}