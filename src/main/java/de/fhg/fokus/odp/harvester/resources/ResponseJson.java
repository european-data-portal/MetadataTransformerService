package de.fhg.fokus.odp.harvester.resources;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_EMPTY)
public class ResponseJson {
	
	private boolean success;
	
	private String error;
	
	private Object result;
	
	private Long total;
	
	private Integer rows;
	
	private Integer start;

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public Object getResult() {
		return result;
	}

	public void setResult(Object result) {
		this.result = result;
	}

	public Long getTotal() {
		return total;
	}

	public void setTotal(Long total) {
		this.total = total;
	}

	public Integer getRows() {
		return rows;
	}

	public void setRows(Integer rows) {
		this.rows = rows;
	}

	public Integer getStart() {
		return start;
	}

	public void setStart(Integer start) {
		this.start = start;
	}

	public static ResponseJson createSuccess(Object result, Long total, Integer rows, Integer start) {
		ResponseJson response = new ResponseJson();
		response.setSuccess(true);
		response.setResult(result);
		response.setRows(rows);
		response.setStart(start);
		response.setTotal(total);
		return response;		
	}

	public static ResponseJson createError(String error) {
		ResponseJson response = new ResponseJson();
		response.setSuccess(false);
		response.setError(error);
		return response;		
	}
	
}
