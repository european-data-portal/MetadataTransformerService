package de.fhg.fokus.odp.harvester.persistence.enums;

public class HarvesterEnums {

	public enum HarvestStrategy {
		create("Create"), update("Update");

		private String label;

		HarvestStrategy(String label) {
			this.label = label;
		}

		public String getLabel() {
			return this.label;
		}
	}

	public enum TransformationScriptSource {

		custom("Custom"), git("Git");

		private String label;

		TransformationScriptSource(String label) {
			this.label = label;
		}

		public String getLabel() {
			return this.label;
		}
	}
}