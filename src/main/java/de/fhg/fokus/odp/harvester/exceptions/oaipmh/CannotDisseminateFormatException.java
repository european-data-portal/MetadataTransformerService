package de.fhg.fokus.odp.harvester.exceptions.oaipmh;

import de.fhg.fokus.odp.harvester.exceptions.ProtocolException;

public class CannotDisseminateFormatException extends ProtocolException {
    
    /**
	 * 
	 */
	private static final long serialVersionUID = 3426930757867262731L;
	
	private String message = "The value of the identifier argument is unknown or illegal in this repository.";

    public CannotDisseminateFormatException() {
        super();    
    }
    
      public CannotDisseminateFormatException(String message) {
        super(message);
        this.message = message;
    }
    
    public CannotDisseminateFormatException(Throwable throwable) {
        super(throwable);
        message = throwable.getMessage();
    }

  
    public CannotDisseminateFormatException(String message, Throwable throwable) {
        super(message, throwable);
        this.message = message;
    }
    
  
    public String getMessage() {
        return message;
    }
    
   
    public void setMessage(String message) {
        this.message = message;
    }

	
}
