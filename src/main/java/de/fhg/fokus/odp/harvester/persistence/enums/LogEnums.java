package de.fhg.fokus.odp.harvester.persistence.enums;

public class LogEnums {

    public enum Severity {
        info("Info"), error("Error"), warning("Warning");

        private final String label;

        Severity(String label) {
            this.label = label;
        }

        public String getLabel() {
            return this.label;
        }
    }

    public enum Category {
        connection("Connection"), transformation("Transformation"), dataset("Dataset"), system("System"), unknown("Unknown");

        private final String label;

        Category(String label) {
            this.label = label;
        }

        public String getLabel() {
            return this.label;
        }
    }
}
