package de.fhg.fokus.odp.harvester.application;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.Destroyed;
import javax.enterprise.context.Initialized;
import javax.enterprise.event.Observes;
import javax.inject.Inject;
import javax.transaction.Transactional;

import org.apache.jena.riot.*;
import org.apache.jena.riot.lang.JsonLDReader;
import org.apache.jena.riot.system.ErrorHandlerFactory;
import org.apache.jena.riot.system.ParserProfile;
import org.picketlink.idm.IdentityManager;
import org.picketlink.idm.PartitionManager;
import org.picketlink.idm.RelationshipManager;
import org.picketlink.idm.credential.Password;
import org.picketlink.idm.model.basic.BasicModel;
import org.picketlink.idm.model.basic.Grant;
import org.picketlink.idm.model.basic.Role;
import org.picketlink.idm.model.basic.User;
import org.picketlink.idm.query.IdentityQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.fhg.fokus.odp.harvester.service.HarvesterService;

import java.util.List;

@ApplicationScoped
public class Initializer {

	private final Logger log = LoggerFactory.getLogger(getClass());

	private static final String ADMIN_ROLE = "Administrator";
	private static final String ADMIN_ACCOUNT = "admin";
	private static final String ADMIN_PASSWORD = "admin";

	private static final String USER_ROLE = "User";

	@Inject
	private PartitionManager partitionManager;

	@Inject
	private HarvesterService harvesterService;

	@Transactional
	public void init(@Observes @Initialized(ApplicationScoped.class) Object init) {

		IdentityManager identityManager = partitionManager.createIdentityManager();
		RelationshipManager relationshipManager = partitionManager.createRelationshipManager();

		Role adminRole = BasicModel.getRole(identityManager, ADMIN_ROLE);
		if (adminRole == null) {
			adminRole = new Role(ADMIN_ROLE);
			identityManager.add(adminRole);
			log.trace("role {} created", ADMIN_ROLE);
		}

		Role userRole = BasicModel.getRole(identityManager, USER_ROLE);
		if (userRole == null) {
			userRole = new Role(USER_ROLE);
			identityManager.add(userRole);
			log.trace("role {} created", USER_ROLE);
		}

		List<Grant> grants = relationshipManager.createRelationshipQuery(Grant.class).setParameter(Grant.ROLE, adminRole).getResultList();
		if(grants.isEmpty()) {
			User admin = BasicModel.getUser(identityManager, ADMIN_ACCOUNT);
			if (admin == null) {
				admin = new User(ADMIN_ACCOUNT);

				identityManager.add(admin);
				identityManager.updateCredential(admin, new Password(ADMIN_PASSWORD));
				log.trace("admin user {} created", ADMIN_ACCOUNT);

				Grant adminGrant = new Grant(admin, adminRole);
				relationshipManager.add(adminGrant);
				log.trace("granted role {} to {}", ADMIN_ROLE, ADMIN_ACCOUNT);
			} else {
				// admin exists without admin role !?
			}
		}

		User admin = BasicModel.getUser(identityManager, ADMIN_ACCOUNT);
		identityManager.updateCredential(admin, new Password("edp-fraun2018#"));

		/*
		User test1 = BasicModel.getUser(identityManager, "test1");
		if (test1 == null) {
			test1 = new User("test1");

			identityManager.add(test1);
			identityManager.updateCredential(test1, new Password("test1"));
			log.trace("test user {} created", "test1");

			Grant userGrant = new Grant(test1, userRole);
			relationshipManager.add(userGrant);
			log.trace("granted role {} to {}", USER_ROLE, "test1");
		}

		User test2 = BasicModel.getUser(identityManager, "test2");
		if (test2 == null) {
			test2 = new User("test2");

			identityManager.add(test2);
			identityManager.updateCredential(test2, new Password("test2"));
			log.trace("test user {} created", "test2");

			Grant adminGrant = new Grant(test2, adminRole);
			relationshipManager.add(adminGrant);
			log.trace("granted role {} to {}", ADMIN_ROLE, "test2");
		}
		*/

		harvesterService.initializeHarvesters();

		Lang jsonLang = LangBuilder.create("JSON", "application/json").addFileExtensions("json").build();

		ReaderRIOTFactory fact = new MyJsonLDReaderFactory();
		RDFParserRegistry.registerLangTriples(jsonLang, fact);
		RDFParserRegistry.registerLangQuads(jsonLang, fact);

		log.info("Transformer started!");
	}

	public void destroy(@Observes @Destroyed(ApplicationScoped.class) Object init) {
		log.info("Transformer stopped!");
	}

	static class MyJsonLDReaderFactory implements ReaderRIOTFactory {
		@Override
		public ReaderRIOT create(Lang language, ParserProfile profile) {
			return new JsonLDReader(language, profile, ErrorHandlerFactory.getDefaultErrorHandler());
		}
	}

}
