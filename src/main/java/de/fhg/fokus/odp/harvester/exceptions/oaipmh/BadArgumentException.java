package de.fhg.fokus.odp.harvester.exceptions.oaipmh;

import de.fhg.fokus.odp.harvester.exceptions.ProtocolException;

public class BadArgumentException extends ProtocolException {
    
    /**
	 * 
	 */
	private static final long serialVersionUID = 6627189953532688922L;
	
	private String message = "The request includes illegal arguments, is missing required arguments, includes a repeated argument, or values for arguments have an illegal syntax.";

    public BadArgumentException() {
        super();    
    }
    
      public BadArgumentException(String message) {
        super(message);
        this.message = message;
    }
    
    public BadArgumentException(Throwable throwable) {
        super(throwable);
        message = throwable.getMessage();
    }

  
    public BadArgumentException(String message, Throwable throwable) {
        super(message, throwable);
        this.message = message;
    }
    
  
    public String getMessage() {
        return message;
    }
    
   
    public void setMessage(String message) {
        this.message = message;
    }

	
}
