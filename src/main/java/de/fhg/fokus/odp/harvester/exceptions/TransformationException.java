package de.fhg.fokus.odp.harvester.exceptions;

/**
 * Created by jpi on 14.04.2015.
 */
public class TransformationException extends ProtocolException {

    /**
	 * 
	 */
	private static final long serialVersionUID = -5894377653398258463L;

	public TransformationException(String message) {
        super(message);
    }

}
