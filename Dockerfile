###
# vert.x docker example using a Java verticle packaged as a fatjar
# To build:
#  docker build -t sample/vertx-java-fat .
# To run:
#   docker run -t -i -p 8080:8080 ose/validator
###

FROM jboss/wildfly:latest

ADD target/MetadataTransformerService.war /opt/jboss/wildfly/standalone/deployments
